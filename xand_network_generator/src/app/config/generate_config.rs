use crate::{
    app::{
        data::ImageSet, error::load_configuration_error::LoadConfigurationError, file_io, Result,
    },
    contracts::data::bank_info::Bank,
};
use std::num::NonZeroU64;
use std::{collections::HashSet, path::Path};

mod defaults;

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct GenerateConfig {
    /// Number of validators to generate and include in the chain spec.
    pub num_validators: usize,

    /// Number of extra validators to generate, who won't be permissioned in the chain spec. Defaults to 5.
    #[serde(default = "defaults::default_extra_validators")]
    pub num_extra_validators: usize,

    /// Number of members to include in the chain spec. By default, `generate` will produce extra members not in the chain spec
    pub num_members: usize,

    /// Number of extra validators to generate, who won't be permissioned in the chain spec. Defaults to 5.
    #[serde(default = "defaults::default_extra_members")]
    pub num_extra_members: usize,

    /// Versions of software (docker images) to use for entity services (Validator/NCN, Member API, Trustee, etc.)
    pub images: ImageSet,

    /// If provided, seeds the PRNG used to generate keys, such that the resultant network is a pure function of configuration
    pub key_generation_seed: Option<u64>,

    /// If provided, selects the set of banks which will be configured in the Trust and bank-mocks. If unspecified, MCB and Provident are enabled by default.
    enabled_banks: Option<HashSet<Bank>>,

    /// If true, enables jwt across all Xand APIs and Member APIs. Defaults to false
    #[serde(default)]
    pub enable_auth: bool,

    /// Configures the validator emission rate in minor units (cents)
    pub minor_units_per_validator_emission: u64,
    pub block_quota: NonZeroU64,
}

impl GenerateConfig {
    /// Load config from file
    pub fn load<P: AsRef<Path>>(path: P) -> Result<Self, LoadConfigurationError> {
        let path = path.as_ref();
        if !path.exists() {
            return Err(LoadConfigurationError::FileNotFound(path.into()));
        }

        let f = file_io::open_file(path)?;
        let cfg = serde_yaml::from_reader(f)?;
        Ok(cfg)
    }

    pub fn get_enabled_banks_or_default(&self) -> HashSet<Bank> {
        self.enabled_banks
            .clone()
            .unwrap_or_else(|| [Bank::Mcb, Bank::Provident].iter().copied().collect())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::data::ImageVersion;
    use insta::assert_display_snapshot;
    use std::{fs::File, io::Write, path::PathBuf};
    use tempfile::tempdir;

    const SAMPLE_YAML_CONFIG: &str = r#"---
num-validators: 10
num-members: 5
images:
  member-api: "gcr.io/xand-dev/someimage:0.1.0"
  validator: "gcr.io/xand-dev/someimage:0.1.0"
  trustee: "gcr.io/xand-dev/someimage:0.1.0"
  trust-api: "gcr.io/xand-dev/someimage:0.1.0"
  bank-mocks: "gcr.io/xand-dev/someimage:0.1.0"
minor-units-per-validator-emission: 1
block-quota: 2"#;

    impl GenerateConfig {
        fn test() -> Self {
            Self {
                num_validators: 10,
                num_extra_validators: 5,
                num_members: 5,
                num_extra_members: 5,
                images: ImageSet {
                    member_api: ImageVersion("gcr.io/xand-dev/someimage:0.1.0".into()),
                    validator: ImageVersion("gcr.io/xand-dev/someimage:0.1.0".into()),
                    trustee: ImageVersion("gcr.io/xand-dev/someimage:0.1.0".into()),
                    bank_mocks: ImageVersion("gcr.io/xand-dev/someimage:0.1.0".into()),
                    trust_api: ImageVersion("gcr.io/xand-dev/someimage:0.1.0".into()),
                },
                key_generation_seed: Some(0),
                enabled_banks: None,
                enable_auth: false,
                minor_units_per_validator_emission: 0,
                block_quota: 2.try_into().unwrap(),
            }
        }
    }

    #[test]
    fn deserialize__enable_jwt_defaults_to_false_if_not_present() {
        // When
        let cfg: GenerateConfig = serde_yaml::from_str(SAMPLE_YAML_CONFIG).unwrap();

        // Then
        assert!(!cfg.enable_auth);
    }

    #[test]
    fn deserialize__can_deserialize_yaml_string() {
        // When
        let cfg: GenerateConfig = serde_yaml::from_str(SAMPLE_YAML_CONFIG).unwrap();

        // Then
        assert_eq!(cfg.num_members, 5);
        assert_eq!(cfg.num_validators, 10);
        assert_eq!(
            cfg.images.validator.to_string(),
            "gcr.io/xand-dev/someimage:0.1.0"
        )
    }

    #[test]
    fn deserialize__fails_with_extra_top_level_field() {
        // Given
        let config_str = String::from(SAMPLE_YAML_CONFIG) + "\nthis_does_not_exist: 7";

        // When
        let error = serde_yaml::from_str::<GenerateConfig>(&config_str).unwrap_err();

        // Then
        assert_display_snapshot!(error, @"unknown field `this_does_not_exist`, expected one of `num-validators`, `num-extra-validators`, `num-members`, `num-extra-members`, `images`, `key-generation-seed`, `enabled-banks`, `enable-auth`, `minor-units-per-validator-emission`, `block-quota` at line 12 column 1");
    }

    #[test]
    fn serde__roundtrip() {
        // Given test cfg
        let cfg = GenerateConfig::test();

        // When serialized and deserialized
        let yaml = serde_yaml::to_string(&cfg).unwrap();
        let deserialized: GenerateConfig = serde_yaml::from_str(&yaml).unwrap();

        // Then
        assert_eq!(deserialized, cfg);
    }

    #[test]
    fn load__valid_yaml_file_succeeds() {
        // Given
        let tmp = tempdir().unwrap();
        let filepath = tmp.path().join("testcfg.yaml");
        // Write sample string to temp file
        File::create(&filepath)
            .map(|mut f| f.write(SAMPLE_YAML_CONFIG.as_bytes()).unwrap())
            .unwrap();

        // When
        GenerateConfig::load(filepath).unwrap();
    }

    #[test]
    fn load__file_not_found_returns_err() {
        // Given
        let f: PathBuf = "./doesntexist.yaml".into();

        // When
        let res = GenerateConfig::load(&f);

        // Then
        assert!(res.is_err());
        assert_matches!(res.err().unwrap(), LoadConfigurationError::FileNotFound(_));
    }

    #[test]
    fn load__invalid_yaml_file_returns_err() {
        // Given
        let tmp = tempdir().unwrap();
        let filepath = tmp.path().join("testcfg.yaml");
        // Write sample string to temp file
        File::create(&filepath)
            .map(|mut f| f.write(b"{\"not\": \"yaml\"}").unwrap())
            .unwrap();

        // When
        let res = GenerateConfig::load(filepath);

        // Then
        assert!(res.is_err());
        assert_matches!(
            res.err().unwrap(),
            LoadConfigurationError::DeserializeError(_)
        );
    }
}
