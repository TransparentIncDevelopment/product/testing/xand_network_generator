use std::convert::TryFrom;

use member_api_config::{
    Address, LocalFileSecretStoreConfiguration, MemberApiConfig, SecretStoreConfig, Url,
    XandApiAuthConfig, XandApiConfig,
};

use crate::app::{
    entities::member::{states::banked_member::DeployableMember, Member},
    port::participant_info::member::MemberPublicInfo,
    xand_services::secret_store_consts::{
        MEMBER_API_LOOKUP_KEY_JWTSECRET, XAND_API_LOOKUP_KEY_JWTTOKEN,
    },
};

/// Utility function to build the correct MemberAPIConfig given member and context
pub fn build_member_api_config_from(
    mem: &Member<DeployableMember>,
    xand_api_url: String,
) -> MemberApiConfig {
    let xand_api_auth = mem.xand_api_auth().as_ref().map(|_| XandApiAuthConfig {
        jwt: Some(XAND_API_LOOKUP_KEY_JWTTOKEN.to_string()),
    });

    let xand_api = XandApiConfig {
        url: Url::parse(&xand_api_url).unwrap(),
        auth: xand_api_auth,
        timeout_seconds: None,
    };
    let address = Address::try_from(mem.get_address().0.to_string()).unwrap();
    let jwt_secret = mem
        .member_api_auth()
        .as_ref()
        .map(|_| MEMBER_API_LOOKUP_KEY_JWTSECRET.to_string());
    MemberApiConfig {
        path: None,
        port: Some(3000),
        workers: None,
        database: "./member-api.db".into(),
        jwt_secret,
        xand_api,
        secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: "/etc/member-api/secrets/secrets.yaml".to_string(),
        }),
        address,
    }
}

#[cfg(test)]
mod tests {
    use crate::app::contexts::docker_compose::entities::docker_compose_member::DockerComposeMember;

    use super::*;

    const DUMMY_XAND_API_URL: &str = "http://dummy-xand-api.com";

    #[test]
    fn build_member_api_config_from__jwt_secret_in_config_matches_key_in_secret_store() {
        // Given
        let member = DockerComposeMember::test();
        let secret_store = member.member_api_secrets_store();

        // When
        let config = build_member_api_config_from(&member.member, DUMMY_XAND_API_URL.into());

        // Then
        let secret_key = config.jwt_secret.unwrap();
        assert!(secret_store.contains_key(&secret_key));
    }

    #[test]
    fn build_member_api_config_from__jwt_token_in_config_matches_key_in_secret_store() {
        // Given
        let member = DockerComposeMember::test();
        let secret_store = member.member_api_secrets_store();

        // When
        let config = build_member_api_config_from(&member.member, DUMMY_XAND_API_URL.into());

        // Then
        let secret_key = config.xand_api.auth.unwrap().jwt.unwrap();
        assert!(secret_store.contains_key(&secret_key));
    }

    #[test]
    fn build_member_api_config_from__when_member_api_auth_is_none_then_jwt_secret_is_none() {
        // Given
        let member = Member::<DeployableMember>::with_auth(None);

        // When
        let config = build_member_api_config_from(&member, DUMMY_XAND_API_URL.into());

        // Then
        assert!(config.jwt_secret.is_none());
    }

    #[test]
    fn build_member_api_config_from__when_xand_api_auth_is_none_then_xand_auth_is_none() {
        // Given
        let member = Member::<DeployableMember>::with_auth(None);

        // When
        let config = build_member_api_config_from(&member, DUMMY_XAND_API_URL.into());

        // Then
        assert!(config.xand_api.auth.is_none());
    }
}
