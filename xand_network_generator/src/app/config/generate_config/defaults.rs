/// Default to 5 extra validators, if not specified in Configuration.
pub const fn default_extra_validators() -> usize {
    5
}

/// Default to 5 extra members, if not specified in Configuration.
pub const fn default_extra_members() -> usize {
    5
}
