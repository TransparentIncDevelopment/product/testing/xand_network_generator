use crate::app::entities::validator::states::deployable_validator::DeployableValidator;
use crate::{
    app::{
        adapters::{
            auth_provider::AuthProvider,
            bank_info_provider::docker_bank_info_provider::DockerBankInfoProvider,
            ip_provider::docker_ip_provider::DockerIpProvider,
        },
        entities::{
            limited_agent::{
                states::deployable_limited_agent::DeployableLimitedAgent, LimitedAgent,
            },
            member::{states::banked_member::DeployableMember, Member},
            trust::Trust,
            validator::Validator,
        },
        key_provider::KeyProvider,
        network::error::XandEntitySetError,
        port::ip_provider::IpProvider,
    },
    contracts::data::bank_info::Bank,
};
pub use generate_members_spec::GenerateMembersSpec;
pub use generate_validators_spec::GenerateValidatorsSpec;
use itertools::sorted;
use std::collections::HashSet;

pub mod error;
mod generate_members_spec;
mod generate_validators_spec;
#[cfg(test)]
mod tests;

#[derive(Clone)]
pub struct XandEntitySet {
    trust: Trust,
    limited_agent: LimitedAgent<DeployableLimitedAgent>,
    permissioned_validators: Vec<Validator<DeployableValidator>>,
    extra_validators: Vec<Validator<DeployableValidator>>,
    permissioned_members: Vec<Member<DeployableMember>>,
    extra_members: Vec<Member<DeployableMember>>,
    banks: HashSet<Bank>,
}

impl XandEntitySet {
    fn new(
        trust: Trust,
        limited_agent: LimitedAgent<DeployableLimitedAgent>,
        validators: Vec<Validator<DeployableValidator>>,
        off_chain_validators: Vec<Validator<DeployableValidator>>,
        members: Vec<Member<DeployableMember>>,
        off_chain_members: Vec<Member<DeployableMember>>,
        banks: HashSet<Bank>,
    ) -> XandEntitySet {
        Self {
            trust,
            limited_agent,
            permissioned_validators: validators,
            extra_validators: off_chain_validators,
            permissioned_members: members,
            extra_members: off_chain_members,
            banks,
        }
    }

    pub fn generate(
        members_spec: GenerateMembersSpec,
        validators_spec: GenerateValidatorsSpec,
        key_generation_seed: Option<u64>,
        enabled_banks: HashSet<Bank>,
        jwt_enabled: bool,
    ) -> Result<XandEntitySet, XandEntitySetError> {
        // Build members
        let mut ip_provider = DockerIpProvider::new("172.28.0.0/16".parse().unwrap());
        let mut bank_provider = DockerBankInfoProvider::new();

        let key_provider = if let Some(seed) = key_generation_seed {
            KeyProvider::seeded(seed)
        } else {
            KeyProvider::unseeded()
        };
        let keys = key_provider.generate(members_spec.total(), validators_spec.total());

        // If enabled, the same JWT Auth is shared across all generated entities
        let auth_provider = AuthProvider::default();
        let member_api_auth: Option<_> = jwt_enabled
            .then(|| auth_provider.member_api_jwt_info())
            .transpose()?;
        let xand_api_auth = jwt_enabled
            .then(|| auth_provider.xand_api_jwt_info())
            .transpose()?;

        let members: Vec<Member<_>> = keys
            .members
            .into_iter()
            .map(|key| {
                Member::new(key)
                    .transition(vec![ip_provider.next()])
                    .transition(
                        sorted(enabled_banks.iter()).map(|b| bank_provider.next(*b)),
                        member_api_auth.clone(),
                        xand_api_auth.clone(),
                    )
            })
            .collect();
        let permissioned_members = members[0..members_spec.permissioned_count].to_vec();
        let non_permissioned_members = members[members_spec.permissioned_count..].to_vec();

        // Build validators
        let validators: Vec<Validator<_>> = keys
            .validators
            .into_iter()
            .map(|key| {
                Validator::new(key)
                    .transition(vec![ip_provider.next()])
                    .transition(
                        sorted(enabled_banks.iter()).map(|b| bank_provider.next(*b)),
                        xand_api_auth.clone(),
                    )
            })
            .collect();

        let permissioned_validators = validators[0..validators_spec.permissioned_count].to_vec();
        let non_permissioned_validators = validators[validators_spec.permissioned_count..].to_vec();

        // Build trust
        let trust = Trust::new(
            &bank_provider,
            &mut ip_provider,
            keys.trust,
            &enabled_banks,
            xand_api_auth.clone(),
        );

        // Build limited agent
        let limited_agent = LimitedAgent::new(keys.limited_agent)
            .transition(vec![ip_provider.next()])
            .transition(xand_api_auth);

        Ok(XandEntitySet::new(
            trust,
            limited_agent,
            permissioned_validators,
            non_permissioned_validators,
            permissioned_members,
            non_permissioned_members,
            enabled_banks,
        ))
    }

    pub fn validators(&self) -> Vec<Validator<DeployableValidator>> {
        self.permissioned_validators.clone()
    }

    pub fn extra_validators(&self) -> Vec<Validator<DeployableValidator>> {
        self.extra_validators.clone()
    }

    pub fn members(&self) -> Vec<Member<DeployableMember>> {
        self.permissioned_members.clone()
    }

    pub fn extra_members(&self) -> Vec<Member<DeployableMember>> {
        self.extra_members.clone()
    }

    pub fn trust(&self) -> Trust {
        self.trust.clone()
    }

    pub fn limited_agent(&self) -> LimitedAgent<DeployableLimitedAgent> {
        self.limited_agent.clone()
    }

    pub fn banks(&self) -> HashSet<Bank> {
        self.banks.clone()
    }
}
