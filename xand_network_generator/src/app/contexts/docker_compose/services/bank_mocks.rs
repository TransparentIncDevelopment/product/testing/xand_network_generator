use crate::{
    app::{
        contexts::docker_compose::{
            docker_compose_network::structured_docker_compose_root::StructuredDockerComposeRoot,
            service_builder::ServiceBuilder,
            service_naming_convention::ServiceNamingConvention,
            services::bank_mocks::bank_mocks_config::{
                BankMocksBankConfig, BankMocksConfig, BankMocksProvider,
            },
        },
        data::ImageVersion,
    },
    contracts::{data::bank_info::Bank, network::metadata::xand_entity_set_summary::BanksMetadata},
};
use compose_yml::v2::Service;
use std::{collections::HashSet, fs::File, io::Write, path::PathBuf};

mod bank_mocks_config;
mod bank_mocks_urls;

#[cfg(test)]
mod tests;

pub use bank_mocks_urls::build_url_for_mock_bank;

use bank_mocks_urls::BANK_MOCKS_CONTAINER_PORT;

use self::bank_mocks_config::BankMocksAuthentication;
use crate::app::contexts::docker_compose::services::bank_mocks::bank_mocks_urls::BANK_MOCKS_HOST_PORT;

#[derive(Clone)]
pub struct BankMocks {
    version: ImageVersion,
    enabled_banks: HashSet<Bank>,
}

impl BankMocks {
    pub fn new(version: ImageVersion, enabled_banks: HashSet<Bank>) -> Self {
        BankMocks {
            version,
            enabled_banks,
        }
    }
}

impl BankMocks {}

impl BankMocks {
    /// Returns metadata to access bank mocks outside docker network (from host machine).
    /// For URLs that member-api can reach within docker network, use `self.get_service_name()` for host value
    pub fn metadata(&self) -> BanksMetadata {
        let urls = self
            .enabled_banks
            .iter()
            .map(|bank| (*bank, build_url_for_mock_bank(*bank, "localhost")))
            .collect();

        BanksMetadata { urls }
    }

    fn bank_config_for_bank(bank: Bank) -> BankMocksBankConfig {
        let provider = match bank {
            Bank::Mcb => BankMocksProvider::Metropolitan,
            Bank::Provident | Bank::TestBankOne | Bank::TestBankTwo => {
                BankMocksProvider::TreasuryPrime
            }
        };

        BankMocksBankConfig {
            provider,
            authentication: Some(BankMocksAuthentication {
                username: format!("my-fake-{}-username", bank.get_url_name()),
                password: format!("my-fake-{}-password", bank.get_url_name()),
            }),
            base_url: Some(bank.get_url_name()),
        }
    }

    pub fn serialize_config_to(&self, dc_root: &StructuredDockerComposeRoot) -> crate::Result<()> {
        // Expected "config.yaml" file to be in application root
        let app_root = dc_root.bank_mocks_dir();
        let config_file = app_root.join("config.yaml");

        let config = BankMocksConfig {
            banks: self
                .enabled_banks
                .iter()
                .map(|b| Self::bank_config_for_bank(*b))
                .collect(),
        };
        let config_contents = config.to_string();
        File::create(config_file)
            .and_then(|mut f| f.write_all(config_contents.as_bytes()))
            .unwrap();
        Ok(())
    }

    pub fn get_service_name(&self) -> String {
        ServiceNamingConvention::bank_mocks()
    }

    pub fn build_service(&self, rel_path_prefix: PathBuf) -> Service {
        let mut s = ServiceBuilder::default();
        let path_to_app_root = rel_path_prefix.join("./");

        s.set_image(&self.version)
            .set_entrypoint("yarn")
            .override_command_args_vec(vec!["start", &BANK_MOCKS_CONTAINER_PORT.to_string()])
            .set_port_mappings(vec![(BANK_MOCKS_HOST_PORT, BANK_MOCKS_CONTAINER_PORT)])
            .override_host_volume_mapping(vec![(&path_to_app_root, "/etc/xand/bank-mocks/")])
            .set_network("xand");
        s.build()
    }
}
