use crate::{
    app::data::ImageVersion, contracts::network::metadata::bootnode_url_vec::BootnodeUrlVec,
};
use std::path::PathBuf;

pub struct NonConsensusNodeSpec {
    pub validator_version: ImageVersion,
    pub node_name: String,
    pub bootnode_url_vec: BootnodeUrlVec,
    pub multi_addr_dns: String,
    pub rel_path_to_service_dir: PathBuf,
    pub xand_api_host_port: usize,
    pub xand_api_jwt_secret_path: Option<PathBuf>,
}

#[cfg(test)]
mod tests {
    use super::*;

    impl NonConsensusNodeSpec {
        pub fn test() -> Self {
            Self {
                validator_version: ImageVersion("some-image:1.1.1".into()),
                node_name: "the-name".to_string(),
                bootnode_url_vec: vec!["blah"].into(),
                multi_addr_dns: "multi-address-dns".to_string(),
                rel_path_to_service_dir: PathBuf::from("./dummy/root/dir/"),
                xand_api_host_port: 1234,
                xand_api_jwt_secret_path: None,
            }
        }
    }
}
