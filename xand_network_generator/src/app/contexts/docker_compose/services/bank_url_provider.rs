use crate::contracts::data::bank_info::Bank;

pub trait BankUrlProvider {
    fn get_bank_url(&self, bank: Bank) -> String;
}

#[cfg(test)]
pub mod tests {
    use crate::{
        app::contexts::docker_compose::services::bank_url_provider::BankUrlProvider,
        contracts::data::bank_info::Bank,
    };

    pub struct DummyBankUrlProvider;

    impl BankUrlProvider for DummyBankUrlProvider {
        fn get_bank_url(&self, bank: Bank) -> String {
            match bank {
                Bank::Mcb => "http://some_mcb.site".into(),
                Bank::Provident => "http://some_provident.site".into(),
                Bank::TestBankOne => "http://some_test_bank_one.site".into(),
                Bank::TestBankTwo => "http://some_test_bank_two.site".into(),
            }
        }
    }
}
