use crate::app::contexts::docker_compose::{
    service_builder::ServiceBuilder,
    services::non_consensus_node::non_consensus_node_spec::NonConsensusNodeSpec,
};
use compose_yml::v2::Service;
use std::convert::TryInto;

pub mod non_consensus_node_spec;

/// All Xand APIs will be available on this port *within* their individual containers and xand-api exposure
pub const XAND_API_CONTAINER_PORT: u16 = 10044;

/// By convention, this port will be decremented for each Validator
pub const STARTING_VALIDATORS_XAND_API_HOST_PORT: usize = 10042;

/// By convention, the Trust is exposed on this port for a local docker-compose network
pub const TRUST_XAND_API_HOST_PORT: usize = 10043;

/// By convention, the Trust API is exposed on this port for a local and remote docker-compose network
pub const TRUST_API_PORT: u16 = 8080;

/// By convention, the Limited Agent is exposed on this port for a local docker-compose network
pub const LIMITED_AGENT_XAND_API_HOST_PORT: usize = 11001;

/// By convention, this port will be incremented for each Member
pub const STARTING_MEMBERS_XAND_API_HOST_PORT: usize = 10044;

pub struct NonConsensusNode {}

impl NonConsensusNode {
    pub fn build(spec: NonConsensusNodeSpec) -> Service {
        let mut s = ServiceBuilder::default_xand_node();

        s.set_consensus(false)
            .set_image(&spec.validator_version)
            .set_public_addr(&spec.multi_addr_dns)
            .set_bootnode_url_vec(&spec.bootnode_url_vec)
            .set_node_name(&spec.node_name)
            .prefix_volumes_with(spec.rel_path_to_service_dir.clone())
            .set_port_mappings(vec![(
                ((spec.xand_api_host_port).try_into().unwrap()),
                XAND_API_CONTAINER_PORT,
            )]);

        // Add jwt flags and mount in jwt-secret.txt if given path to jwt secret file
        if let Some(path) = spec.xand_api_jwt_secret_path {
            s.add_jwt_flag_to_command_line_args();
            s.add_jwt_secret_txt_volume(path);
        }

        s.build()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use compose_yml::v2::{value, CommandLine, VolumeMount};
    use insta::assert_yaml_snapshot;

    #[test]
    fn build__passing_some_jwt_path_enables_auth() {
        // Given
        let jwt_path = "./dummy/root/dir/xand-api/jwt-secret.txt";
        let mut spec = NonConsensusNodeSpec::test();
        spec.xand_api_jwt_secret_path = Some(jwt_path.into());

        // When
        let service = NonConsensusNode::build(spec);

        // Then
        // Assert required args included in service definition
        let expected_flag = value("--jwt-secret-path".to_string());
        let expected_container_mount_path = "/etc/xand/jwt-secret.txt";
        let expected_arg = value(expected_container_mount_path.to_string());
        match service.command {
            Some(CommandLine::Parsed(args)) => {
                assert!(args.contains(&expected_flag), "{:#?}", &args);
                assert!(args.contains(&expected_arg), "{:#?}", &args);
            }
            _ => panic!("Received unexpected type for command field"),
        };

        // Assert required volume
        let expected_volume = value(VolumeMount::host(
            jwt_path.to_string(),
            expected_container_mount_path,
        ));
        assert!(
            service.volumes.contains(&expected_volume),
            "{:#?}",
            &service.volumes
        );
    }

    #[test]
    fn build__passing_none_jwt_path_disables_auth() {
        // Given
        let mut spec = NonConsensusNodeSpec::test();
        spec.xand_api_jwt_secret_path = None;

        // When
        let service = NonConsensusNode::build(spec);

        // Then
        assert_yaml_snapshot!(service);
    }
}
