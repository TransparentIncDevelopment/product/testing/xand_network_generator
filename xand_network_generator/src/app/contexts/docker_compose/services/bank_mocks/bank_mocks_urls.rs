use crate::contracts::data::bank_info::Bank;

pub const BANK_MOCKS_HOST_PORT: u16 = 8888;
pub const BANK_MOCKS_CONTAINER_PORT: u16 = 8888;

pub fn build_url_for_mock_bank(bank: Bank, service_name: &str) -> String {
    let base_url = bank.get_url_name();

    format!(
        "http://{}:{}/{}/",
        service_name, BANK_MOCKS_CONTAINER_PORT, base_url
    )
}

#[cfg(test)]
mod tests {
    use crate::{
        app::contexts::docker_compose::services::bank_mocks::build_url_for_mock_bank,
        contracts::data::bank_info::Bank,
    };

    #[test]
    fn build_url_for_mock_bank__generates_correct_urls() {
        assert_eq!(
            &build_url_for_mock_bank(Bank::Mcb, "some_service"),
            "http://some_service:8888/metropolitan/"
        );
        assert_eq!(
            &build_url_for_mock_bank(Bank::Provident, "some_service"),
            "http://some_service:8888/provident/"
        );
        assert_eq!(
            &build_url_for_mock_bank(Bank::TestBankOne, "some_service"),
            "http://some_service:8888/test_bank_one/"
        );
        assert_eq!(
            &build_url_for_mock_bank(Bank::TestBankTwo, "some_service"),
            "http://some_service:8888/test_bank_two/"
        );
    }
}
