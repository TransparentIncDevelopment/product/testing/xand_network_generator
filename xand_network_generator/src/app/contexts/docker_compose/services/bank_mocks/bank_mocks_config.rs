use std::collections::HashSet;

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "snake_case")]
pub enum BankMocksProvider {
    Silvergate,
    Metropolitan,
    TreasuryPrime,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "kebab-case")]
pub struct BankMocksAuthentication {
    pub username: String,
    pub password: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "kebab-case")]
pub struct BankMocksBankConfig {
    pub provider: BankMocksProvider,
    pub base_url: Option<String>,
    pub authentication: Option<BankMocksAuthentication>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct BankMocksConfig {
    #[serde(serialize_with = "serde_ordered_collections::flat::sorted_serialize")]
    pub banks: HashSet<BankMocksBankConfig>,
}

impl ToString for BankMocksConfig {
    fn to_string(&self) -> String {
        serde_yaml::to_string(&self).unwrap()
    }
}
