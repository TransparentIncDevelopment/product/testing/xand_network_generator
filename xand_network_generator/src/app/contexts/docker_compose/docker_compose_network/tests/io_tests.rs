use crate::{
    app::{
        contexts::docker_compose::docker_compose_network::*,
        network::{GenerateMembersSpec, GenerateValidatorsSpec},
    },
    contracts::data::bank_info::Bank,
};
use tempfile::{tempdir, TempDir};

#[test]
fn serialize_entities__serializes_all_entities_to_disk() {
    // Given
    let members_spec = GenerateMembersSpec::new(1, 1);
    let validators_spec = GenerateValidatorsSpec::new(1, 1);
    let key_generation_seed = Some(0);
    let entity_set = XandEntitySet::generate(
        members_spec,
        validators_spec,
        key_generation_seed,
        [Bank::TestBankOne, Bank::TestBankTwo]
            .iter()
            .copied()
            .collect(),
        true,
    )
    .unwrap();

    let dummy_chain_spec = tempfile::NamedTempFile::new().unwrap();

    let network = DockerComposeNetwork::new(
        entity_set,
        ImageSet::test(),
        ChainSpec::new(dummy_chain_spec.path().into()),
    );

    let dir = TempDir::new().unwrap();
    let root = StructuredDockerComposeRoot::build(dir.path().into()).unwrap();

    // When
    network.serialize_entities(&root).unwrap();

    // Then
    assert!(root.path().join("validators/0").is_dir());
    assert!(root.path().join("validators/1").is_dir());
    assert!(root.path().join("members/0").is_dir());
    assert!(root.path().join("members/1").is_dir());
}

#[test]
fn write_docker_compose_yaml__writes() {
    // Given
    let tmpdir = tempdir().unwrap();
    let path = tmpdir.into_path();
    let dcc = DockerComposeNetwork::test();

    // Then it doesn't panic
    dcc.write_docker_compose_yaml(&path.into()).unwrap();
}
