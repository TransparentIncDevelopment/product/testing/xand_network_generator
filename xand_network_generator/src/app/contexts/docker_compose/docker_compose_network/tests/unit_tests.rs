use crate::app::contexts::docker_compose::DockerComposeNetwork;

#[test]
fn member_banks__returns_permissioned_member_banks() {
    let test = DockerComposeNetwork::test();

    let res = test.member_banks();

    assert!(res.contains_key("member-0"))
}

#[test]
fn member_banks__returns_nonpermissioned_member_banks() {
    let test = DockerComposeNetwork::test();

    let res = test.member_banks();

    assert!(res.contains_key("member-1"))
}
