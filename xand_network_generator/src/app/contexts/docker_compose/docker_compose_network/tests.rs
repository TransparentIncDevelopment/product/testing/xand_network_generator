use crate::app::contexts::docker_compose::{
    docker_compose_network::docker_xand_entity_set::DockerXandEntitySet, DockerComposeNetwork,
};

pub mod io_tests;
pub mod serialization_tests;
pub mod unit_tests;

impl DockerComposeNetwork {
    pub fn test() -> Self {
        Self {
            docker_xand_entity_set: DockerXandEntitySet::test(),
        }
    }
}
