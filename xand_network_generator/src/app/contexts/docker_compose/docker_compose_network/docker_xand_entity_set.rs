use crate::app::contexts::docker_compose::{
    entities::{
        docker_compose_limited_agent::DockerComposeLimitedAgent,
        docker_compose_member::DockerComposeMember, docker_compose_trust::DockerComposeTrust,
        docker_compose_validator::DockerComposeValidator,
    },
    services::bank_mocks::BankMocks,
};

pub struct DockerXandEntitySet {
    pub banks: BankMocks,
    pub trust: DockerComposeTrust,
    pub limited_agent: DockerComposeLimitedAgent,

    pub permissioned_validators: Vec<DockerComposeValidator>,
    pub extra_validators: Vec<DockerComposeValidator>,

    pub permissioned_members: Vec<DockerComposeMember>,
    pub extra_members: Vec<DockerComposeMember>,
}

#[cfg(test)]
mod tests {
    use super::*;

    impl DockerXandEntitySet {
        pub fn test() -> Self {
            let mut val_0 = DockerComposeValidator::test();
            let mut val_1 = DockerComposeValidator::test();
            val_0.index = 0;
            val_1.index = 1;

            let mut mem_0 = DockerComposeMember::test();
            let mut mem_1 = DockerComposeMember::test();
            mem_0.index = 0;
            mem_1.index = 1;

            Self {
                banks: BankMocks::test(),
                trust: DockerComposeTrust::test(),
                limited_agent: DockerComposeLimitedAgent::test(),
                permissioned_validators: vec![val_0],
                extra_validators: vec![val_1],
                permissioned_members: vec![mem_0],
                extra_members: vec![mem_1],
            }
        }
    }
}
