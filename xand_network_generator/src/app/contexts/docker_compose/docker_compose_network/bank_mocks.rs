use crate::{
    app::contexts::docker_compose::{
        service_naming_convention::ServiceNamingConvention,
        services::{bank_mocks::build_url_for_mock_bank, bank_url_provider::BankUrlProvider},
    },
    contracts::data::bank_info::Bank,
};

pub struct DockerComposeBankUrlProvider;

impl DockerComposeBankUrlProvider {
    pub fn new() -> Self {
        Self
    }
}

impl BankUrlProvider for DockerComposeBankUrlProvider {
    fn get_bank_url(&self, bank: Bank) -> String {
        let service = ServiceNamingConvention::bank_mocks();

        build_url_for_mock_bank(bank, &service)
    }
}
