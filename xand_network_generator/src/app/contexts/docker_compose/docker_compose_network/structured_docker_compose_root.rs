use crate::app::{
    file_io,
    xand_services::dir_structure::{
        MemberApiDirectoryBuilder, TrustApiDirectoryBuilder, TrustDirectoryBuilder,
        XandNodeDirBuilder,
    },
};
use std::path::{Path, PathBuf};

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::tempdir;

    use util::*;
    mod util {
        use std::path::Path;

        pub fn exists<P: AsRef<Path>>(p: P) -> bool {
            p.as_ref().exists()
        }
    }

    #[test]
    fn create_entity_dirs__creates_top_level_directories_given_path() -> crate::Result<()> {
        // Given
        let tmpdir = tempdir().unwrap();
        let dir = StructuredDockerComposeRoot(tmpdir.path().into());

        // When
        dir.create_entity_dirs()?;

        // Then
        let paths_exist = [
            "members",
            "trust",
            "validators",
            "bank-mocks",
            "chain-spec",
            "limited-agent",
        ]
        .iter()
        .map(|d| tmpdir.path().join(d))
        .all(exists);

        assert!(paths_exist);
        Ok(())
    }
}

#[derive(Debug)]
pub struct StructuredDockerComposeRoot(PathBuf);

impl StructuredDockerComposeRoot {
    pub fn build(p: PathBuf) -> crate::Result<Self> {
        let res = StructuredDockerComposeRoot(p);
        // Create root of given output dir
        file_io::create_dir(&res.0, true)?;

        // Create entities' dirs within output dir
        res.create_entity_dirs()?;
        Ok(res)
    }

    fn create_entity_dirs(&self) -> crate::Result<()> {
        let c = StructuredDockerComposeConv {};

        [
            self.0.join(c.chain_spec()),
            self.0.join(c.members()),
            self.0.join(c.validators()),
            self.0.join(c.trust()),
            self.0.join(c.bank_mocks()),
            self.0.join(c.limited_agent()),
        ]
        .iter()
        .try_for_each(|dir| file_io::create_dir(dir, true))?;
        Ok(())
    }

    pub fn path(&self) -> PathBuf {
        self.0.clone()
    }
    pub fn validators_dir(&self) -> PathBuf {
        self.0.join("validators")
    }

    pub fn trust_dir(&self) -> PathBuf {
        self.0.join("trust")
    }

    pub fn limited_agent_dir(&self) -> PathBuf {
        self.0.join("limited-agent")
    }

    /// Init indexed validator entity dir
    pub fn init_validator_dir(&self, index: usize) -> crate::Result<XandNodeDirBuilder> {
        let d = self.validators_dir().join(index.to_string());
        file_io::create_dir(&d, true)?;
        let builder = XandNodeDirBuilder::new(d);
        Ok(builder)
    }

    pub fn init_member_dir(
        &self,
        index: usize,
    ) -> crate::Result<(XandNodeDirBuilder, MemberApiDirectoryBuilder)> {
        let c = StructuredDockerComposeConv {};
        let entity_dir = self.0.join(c.member_dir(index));
        file_io::create_dir(&entity_dir, true).expect("Creating indexed member dir failed");

        let api_dir = entity_dir.join("member-api");
        file_io::create_dir(&api_dir, true)?;

        let builder = MemberApiDirectoryBuilder::new(api_dir);

        let nc_node_dir = self.init_nc_node_dir(&entity_dir)?;

        Ok((nc_node_dir, builder))
    }

    fn init_nc_node_dir(&self, parent: &Path) -> crate::Result<XandNodeDirBuilder> {
        let nc_node_dir = parent.join("xand-node");

        file_io::create_dir(&nc_node_dir, true)?;

        let dir_builder = XandNodeDirBuilder::new(nc_node_dir);
        Ok(dir_builder)
    }

    pub fn init_trust_dir(
        &self,
    ) -> crate::Result<(
        XandNodeDirBuilder,
        TrustDirectoryBuilder,
        TrustApiDirectoryBuilder,
    )> {
        let entity_root = self.trust_dir();
        file_io::create_dir(&entity_root, true)?;

        let nc_node_dir = self.init_nc_node_dir(&entity_root)?;

        let trust_software_dir = entity_root.join("trust-software");
        file_io::create_dir(&trust_software_dir, true)?;

        let trust_software_dir_builder = TrustDirectoryBuilder::from(trust_software_dir);

        let trust_api_dir = entity_root.join("trust-api");
        file_io::create_dir(&trust_api_dir, true)?;

        let trust_api_dir_builder = TrustApiDirectoryBuilder::from(trust_api_dir);

        Ok((
            nc_node_dir,
            trust_software_dir_builder,
            trust_api_dir_builder,
        ))
    }

    pub fn init_limited_agent_dir(&self) -> crate::Result<XandNodeDirBuilder> {
        let d = self.limited_agent_dir();

        file_io::create_dir(&d, true)?;

        self.init_nc_node_dir(&d)
    }

    pub fn bank_mocks_dir(&self) -> PathBuf {
        let c = StructuredDockerComposeConv {};
        self.0.join(c.bank_mocks())
    }

    pub fn chain_spec_dir(&self) -> PathBuf {
        let c = StructuredDockerComposeConv {};
        self.0.join(c.chain_spec())
    }

    pub fn entities_summary_filepath(&self) -> PathBuf {
        self.0.join("entities-metadata.yaml")
    }
}

impl From<PathBuf> for StructuredDockerComposeRoot {
    fn from(p: PathBuf) -> Self {
        Self(p)
    }
}
/// This struct is strongly tied to StructuredDockerComposeRoot
/// It is the convention to organize entities within the "." dir
pub struct StructuredDockerComposeConv {}

impl StructuredDockerComposeConv {
    pub fn trust(&self) -> PathBuf {
        PathBuf::from("./trust")
    }
    pub fn validators(&self) -> PathBuf {
        PathBuf::from("./validators")
    }
    pub fn members(&self) -> PathBuf {
        PathBuf::from("./members")
    }
    pub fn limited_agent(&self) -> PathBuf {
        PathBuf::from("./limited-agent")
    }

    pub fn chain_spec(&self) -> PathBuf {
        PathBuf::from("./chain-spec")
    }

    pub fn member_dir(&self, index: usize) -> PathBuf {
        PathBuf::from("./members").join(index.to_string())
    }

    pub fn member_api_dir(&self, index: usize) -> PathBuf {
        self.member_dir(index).join("member-api")
    }

    pub fn member_nc_node_dir(&self, index: usize) -> PathBuf {
        self.member_dir(index).join("xand-node")
    }

    pub fn validator_dir(&self, index: usize) -> PathBuf {
        PathBuf::from("./validators").join(index.to_string())
    }

    pub fn limited_agent_nc_node_dir(&self) -> PathBuf {
        PathBuf::from("./limited-agent/xand-node")
    }

    pub fn bank_mocks(&self) -> PathBuf {
        PathBuf::from("./bank-mocks")
    }

    pub fn trust_software_dir(&self) -> PathBuf {
        PathBuf::from("./trust/trust-software")
    }

    pub fn trust_nc_node_dir(&self) -> PathBuf {
        PathBuf::from("./trust/xand-node")
    }

    pub fn trust_api_dir(&self) -> PathBuf {
        PathBuf::from("./trust/trust-api")
    }
}
