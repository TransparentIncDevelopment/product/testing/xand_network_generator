use crate::app::contexts::docker_compose::services::non_consensus_node::TRUST_API_PORT;
use crate::{
    app::{
        contexts::docker_compose::{
            bank_secrets::{
                password_secret_key_for, username_secret_key_for, MCB_CLIENT_APP_IDENT_KEY,
                MCB_ORGANIZATION_ID_KEY,
            },
            services::bank_url_provider::BankUrlProvider,
        },
        entities::trust::Trust,
        xand_services::secret_store_consts::XAND_API_LOOKUP_KEY_JWTTOKEN,
    },
    contracts::data::bank_info::{Bank, BankInfo},
};
use std::collections::{HashMap, HashSet};
use std::time::Duration;
use trust_api_config::{TrustApiConfig, XandApiConfig};
use trustee_node_config::{
    trust_banks::{McbConfig, TreasuryPrimeConfig, TrustBanksConfig},
    Account as TrustAccount, LocalFileSecretStoreConfiguration, PartnerAccounts, SecretStoreConfig,
    SynchronizationPolicyConfiguration, TrusteeConfig, Url as TrustUrl,
};

pub fn build_config(
    trust: &Trust,
    xand_api_url: String,
    member_accounts: &HashMap<String, Vec<BankInfo>>,
    validator_accounts: &HashMap<String, Vec<BankInfo>>,
    bank_urls: &dyn BankUrlProvider,
) -> TrusteeConfig {
    let partner_accounts = build_partner_accounts_config(member_accounts, validator_accounts);
    let trust_address = trust.get_address().0.to_string();
    let banks = build_banks_config(trust, bank_urls);

    TrusteeConfig {
        xand_api_endpoint: TrustUrl::parse(&xand_api_url).unwrap(),
        xand_api_jwt_secret_key: trust
            .xand_api_auth()
            .as_ref()
            .map(|_| XAND_API_LOOKUP_KEY_JWTTOKEN.to_string()),
        xand_api_timeout_seconds: None,
        trust_address,
        access_token_folder: "./".to_string(),
        polling_delay: 5,
        iterations_for_status_check: 60,
        redeem_retry_delay: 300,
        banks,
        partner_accounts,
        secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: "./secrets/secrets.yaml".into(),
        }),
        transaction_db_path: "./db/transaction_store.sqlite3".into(),
        sync_policy: SynchronizationPolicyConfiguration {
            cooldown_timeout: Duration::from_secs(1),
            periodic_synchronization_interval: Duration::from_secs(3600),
        },
    }
}

pub fn build_trust_api_config(trust: &Trust, xand_api_url: String) -> TrustApiConfig {
    TrustApiConfig {
        port: Some(TRUST_API_PORT),
        xand_api: XandApiConfig {
            url: TrustUrl::parse(&xand_api_url).unwrap().to_string(),
            jwt: trust
                .xand_api_auth()
                .as_ref()
                .map(|a| a.token.0.clone())
                .unwrap_or_else(|| "".to_string()),
        },
    }
}

fn build_banks_config(trust: &Trust, bank_urls: &dyn BankUrlProvider) -> HashSet<TrustBanksConfig> {
    let accounts = trust.accounts();

    let mut bank_configs = HashSet::new();

    let mcb = accounts.get(&Bank::Mcb).map(|acct| McbConfig {
        routing_number: acct.routing_number().to_string(),
        bank_api_url: TrustUrl::parse(&bank_urls.get_bank_url(Bank::Mcb)).unwrap(),
        refresh_if_expiring_in_secs: None,
        secret_key_username: username_secret_key_for(Bank::Mcb),
        secret_key_password: password_secret_key_for(Bank::Mcb),
        secret_key_client_app_ident: MCB_CLIENT_APP_IDENT_KEY.into(),
        secret_key_organization_id: MCB_ORGANIZATION_ID_KEY.into(),
        trust_account: acct.account_number().to_string(),
        display_name: "mcb".to_string(),
    });
    if let Some(mcb_cfg) = mcb {
        bank_configs.insert(TrustBanksConfig::Mcb(mcb_cfg));
    }

    let make_treasury_prime_config_for_account = |bank: Bank| {
        accounts.get(&bank).map(|acct| TreasuryPrimeConfig {
            bank_api_url: TrustUrl::parse(&bank_urls.get_bank_url(bank)).unwrap(),
            routing_number: acct.routing_number().to_string(),
            trust_account: acct.account_number().to_string(),
            secret_key_username: username_secret_key_for(bank),
            secret_key_password: password_secret_key_for(bank),
            display_name: bank.get_url_name(),
        })
    };

    if let Some(provident_cfg) = make_treasury_prime_config_for_account(Bank::Provident) {
        bank_configs.insert(TrustBanksConfig::TreasuryPrime(provident_cfg));
    }

    if let Some(test_one_cfg) = make_treasury_prime_config_for_account(Bank::TestBankOne) {
        bank_configs.insert(TrustBanksConfig::TreasuryPrime(test_one_cfg));
    }

    if let Some(test_two_cfg) = make_treasury_prime_config_for_account(Bank::TestBankTwo) {
        bank_configs.insert(TrustBanksConfig::TreasuryPrime(test_two_cfg));
    }

    bank_configs
}

fn build_partner_accounts_config(
    member_accounts: &HashMap<String, Vec<BankInfo>>,
    validator_accounts: &HashMap<String, Vec<BankInfo>>,
) -> PartnerAccounts {
    let mut member_map: HashMap<std::string::String, Vec<TrustAccount>> = HashMap::new();
    let mut validator_map: HashMap<std::string::String, Vec<TrustAccount>> = HashMap::new();

    member_accounts
        .iter()
        .for_each(|(member_name, bank_infos)| {
            let accts = bank_infos
                .iter()
                .map(|bank_infos| TrustAccount {
                    account_number: bank_infos.account_number().to_string(),
                    routing_number: bank_infos.routing_number().to_string(),
                })
                .collect();

            member_map.insert(member_name.clone(), accts);
        });

    validator_accounts
        .iter()
        .for_each(|(validator_name, bank_infos)| {
            let accts = bank_infos
                .iter()
                .map(|bank_infos| TrustAccount {
                    account_number: bank_infos.account_number().to_string(),
                    routing_number: bank_infos.routing_number().to_string(),
                })
                .collect();

            validator_map.insert(validator_name.clone(), accts);
        });

    PartnerAccounts {
        members_to_accounts: member_map,
        validators_to_accounts: validator_map,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::contracts::data::bank_info::Account;
    use crate::{
        app::{
            adapters::{
                auth_provider::AuthProvider,
                bank_info_provider::docker_bank_info_provider::DockerBankInfoProvider,
                ip_provider::docker_ip_provider::DockerIpProvider,
            },
            contexts::docker_compose::{
                entities::docker_compose_trust::conversion::build_config,
                services::bank_url_provider::tests::DummyBankUrlProvider,
            },
            entities::trust::Trust,
            key_provider::TrustKeySet,
        },
        contracts::{
            data::bank_info::Bank,
            network::auth::{Jwt, JwtInfo, JwtSecret},
        },
    };
    use std::collections::HashSet;
    use strum::IntoEnumIterator;
    use trustee_node_config::{
        trust_banks::{McbConfig, TreasuryPrimeConfig, TrustBanksConfig},
        Url as TrustUrl,
    };

    fn default_mcb_config() -> McbConfig {
        McbConfig {
            bank_api_url: TrustUrl::parse("http://some_mcb.site/").unwrap(),
            routing_number: "121141343".into(),
            trust_account: "1000000".into(),
            refresh_if_expiring_in_secs: None,
            secret_key_username: "metropolitan-username".into(),
            secret_key_password: "metropolitan-password".into(),
            secret_key_client_app_ident: "metropolitan-client-app-ident".into(),
            secret_key_organization_id: "metropolitan-organization-id".into(),
            display_name: "mcb".into(),
        }
    }

    fn default_provident_config() -> TreasuryPrimeConfig {
        TreasuryPrimeConfig {
            bank_api_url: TrustUrl::parse("http://some_provident.site/").unwrap(),
            routing_number: "211374020".into(),
            trust_account: "1000000".into(),
            secret_key_username: "provident-username".into(),
            secret_key_password: "provident-password".into(),
            display_name: "provident".into(),
        }
    }

    fn default_test_two_config() -> TreasuryPrimeConfig {
        TreasuryPrimeConfig {
            bank_api_url: TrustUrl::parse("http://some_test_bank_one.site/").unwrap(),
            routing_number: "990000000".into(),
            trust_account: "1000000".into(),
            secret_key_username: "test_bank_one-username".into(),
            secret_key_password: "test_bank_one-password".into(),
            display_name: "test_bank_one".into(),
        }
    }

    fn default_test_one_config() -> TreasuryPrimeConfig {
        TreasuryPrimeConfig {
            bank_api_url: TrustUrl::parse("http://some_test_bank_two.site/").unwrap(),
            routing_number: String::from("991000000"),
            trust_account: "1000000".into(),
            secret_key_username: "test_bank_two-username".into(),
            secret_key_password: "test_bank_two-password".into(),
            display_name: "test_bank_two".into(),
        }
    }

    #[test]
    fn build_config__when_trust_xand_api_jwt_is_enabled_then_config_jwt_key_is_some() {
        // Given
        let mut trust = Trust::test();
        let jwt_info = JwtInfo {
            secret: JwtSecret("test-secret".into()),
            token: Jwt("test-token".into()),
        };
        trust.state.xand_api_auth = Some(jwt_info);
        let bank_url_provider = DummyBankUrlProvider;

        // When
        let config = build_config(
            &trust,
            "https://some-xand-api.com".to_string(),
            &Default::default(),
            &Default::default(),
            &bank_url_provider,
        );

        // Then
        assert_eq!(
            config.xand_api_jwt_secret_key,
            Some(XAND_API_LOOKUP_KEY_JWTTOKEN.to_string())
        );
    }

    #[test]
    fn build_trust_api_config__when_xand_api_jwt_is_enabled_then_config_jwt_key_is_some() {
        // Given
        let mut trust = Trust::test();
        let jwt_info = JwtInfo {
            secret: JwtSecret("test-secret".into()),
            token: Jwt("test-token".into()),
        };
        trust.state.xand_api_auth = Some(jwt_info);

        // When
        let config = build_trust_api_config(&trust, "https://some-xand-api.com".to_string());

        // Then
        assert_eq!(config.xand_api.jwt, "test-token".to_string());
    }

    #[test]
    fn build_config__builds_members_correctly() {
        // Given
        let mut members = HashMap::new();
        let member1_name = "awesomeco".to_string();
        let member2_name = "coolinc".to_string();
        members.insert(
            member1_name.clone(),
            vec![BankInfo::new(Bank::Mcb, Account::new(123123, 20000001))],
        );
        members.insert(
            member2_name.clone(),
            vec![BankInfo::new(Bank::Mcb, Account::new(123123, 20000002))],
        );

        // When
        let config = build_config(
            &Trust::test(),
            "https://some-xand-api.com".to_string(),
            &members,
            &Default::default(),
            &DummyBankUrlProvider,
        );

        // Then
        assert!(config
            .partner_accounts
            .members_to_accounts
            .contains_key(&member1_name));
        assert!(config
            .partner_accounts
            .members_to_accounts
            .contains_key(&member2_name));
    }

    #[test]
    fn build_config__builds_validators_correctly() {
        // Given
        let mut validators = HashMap::new();
        let validator1_name = "greatvalidator".to_string();
        let validator2_name = "excellentvalidator".to_string();
        validators.insert(
            validator1_name.clone(),
            vec![BankInfo::new(Bank::Mcb, Account::new(1000000, 1000000001))],
        );
        validators.insert(
            validator2_name.clone(),
            vec![BankInfo::new(Bank::Mcb, Account::new(1000000, 1000000002))],
        );

        // When
        let config = build_config(
            &Trust::test(),
            "https://some-xand-api.com".to_string(),
            &Default::default(),
            &validators,
            &DummyBankUrlProvider,
        );

        // Then
        assert!(config
            .partner_accounts
            .validators_to_accounts
            .contains_key(&validator1_name));
        assert!(config
            .partner_accounts
            .validators_to_accounts
            .contains_key(&validator2_name));
        assert_eq!(config.partner_accounts.validators_to_accounts.len(), 2)
    }

    #[test]
    fn build_partner_accounts_config__empty_key_values_writes_single_member_entry() {
        // Given
        let mut members = HashMap::new();
        // This will only write one member because the member name is the unique key
        let member_awesome_name = "".to_string();
        let member_amazing_name = "".to_string();
        debug_assert_eq!(
            member_awesome_name, member_amazing_name,
            "Both member names should be the same"
        );
        debug_assert!(
            member_awesome_name.is_empty(),
            "Names should all be empty strings"
        );
        let member_awesome_account = Account::new(123123, 20000001);
        let member_amazing_account = Account::new(123123, 20000002);

        members.insert(
            member_awesome_name.clone(),
            vec![BankInfo::new(Bank::Mcb, member_awesome_account)],
        );

        members.insert(
            member_amazing_name.clone(),
            vec![BankInfo::new(Bank::Mcb, member_amazing_account)],
        );

        // When
        let partner_accounts = build_partner_accounts_config(&members, &Default::default());

        // Then
        assert_eq!(partner_accounts.members_to_accounts.len(), 1);
        assert!(partner_accounts
            .members_to_accounts
            .contains_key(&member_awesome_name));
        assert!(partner_accounts
            .members_to_accounts
            .contains_key(&member_amazing_name));
    }

    #[test]
    fn build_partner_accounts_config__empty_key_values_writes_single_validator_entry() {
        // Given
        let mut validators = HashMap::new();
        // This will only write one validator because the validator name is the unique key
        let validator_awesome_name = "".to_string();
        let validator_amazing_name = "".to_string();
        debug_assert_eq!(
            validator_awesome_name, validator_amazing_name,
            "Both validator names should be the same"
        );
        debug_assert!(
            validator_awesome_name.is_empty(),
            "Names should all be empty strings"
        );
        let validator_awesome_account = Account::new(123123, 20000001);
        let validator_amazing_account = Account::new(123123, 20000002);

        validators.insert(
            validator_awesome_name.clone(),
            vec![BankInfo::new(Bank::Mcb, validator_awesome_account)],
        );

        validators.insert(
            validator_amazing_name.clone(),
            vec![BankInfo::new(Bank::Mcb, validator_amazing_account)],
        );

        // When
        let partner_accounts = build_partner_accounts_config(&Default::default(), &validators);

        // Then
        assert_eq!(partner_accounts.validators_to_accounts.len(), 1);
        assert!(partner_accounts
            .validators_to_accounts
            .contains_key(&validator_awesome_name));
        assert!(partner_accounts
            .validators_to_accounts
            .contains_key(&validator_amazing_name));
    }

    #[test]
    fn build_partner_accounts_config__updates_duplicate_members_correctly() {
        // Given
        let mut members = HashMap::new();
        // This will only write one member because the member name is the unique key
        let member_awesome_name = "MemberA".to_string();
        let member_amazing_name = "MemberA".to_string();
        debug_assert_eq!(
            member_awesome_name, member_amazing_name,
            "Both member names should be the same"
        );
        let member_awesome_account = Account::new(123123, 20000001);
        let member_amazing_account = Account::new(123123, 20000002);

        members.insert(
            member_awesome_name.clone(),
            vec![BankInfo::new(Bank::Mcb, member_awesome_account)],
        );

        members.insert(
            member_amazing_name.clone(),
            vec![BankInfo::new(Bank::Mcb, member_amazing_account)],
        );

        // When
        let partner_accounts = build_partner_accounts_config(&members, &Default::default());

        // Then
        assert_eq!(partner_accounts.members_to_accounts.len(), 1);
        assert!(partner_accounts
            .members_to_accounts
            .contains_key(&member_awesome_name));
        assert!(partner_accounts
            .members_to_accounts
            .contains_key(&member_amazing_name));
    }

    #[test]
    fn build_partner_accounts_config__updates_duplicate_validators_correctly() {
        // Given
        let mut validators = HashMap::new();
        // This will only write one validator because the validator name is the unique key
        let validator_awesome_name = "ValidatorA".to_string();
        let validator_amazing_name = "ValidatorA".to_string();
        debug_assert_eq!(
            validator_awesome_name, validator_amazing_name,
            "Both validator names should be the same"
        );
        let validator_awesome_account = Account::new(123123, 20000001);
        let validator_amazing_account = Account::new(123123, 20000002);
        validators.insert(
            validator_awesome_name.clone(),
            vec![BankInfo::new(Bank::Mcb, validator_awesome_account)],
        );
        validators.insert(
            validator_amazing_name.clone(),
            vec![BankInfo::new(Bank::Mcb, validator_amazing_account)],
        );

        // When
        let partner_accounts = build_partner_accounts_config(&Default::default(), &validators);

        // Then
        assert_eq!(partner_accounts.validators_to_accounts.len(), 1);
        assert!(partner_accounts
            .validators_to_accounts
            .contains_key(&validator_awesome_name));
        assert!(partner_accounts
            .validators_to_accounts
            .contains_key(&validator_amazing_name));
    }

    #[test]
    fn build_banks_config__returns_all_banks_when_all_banks_enabled() {
        // Given
        let enabled_banks = Bank::iter().collect();
        let auth = Some(AuthProvider::get_test_xand_api_jwt_info());
        let trust = Trust::new(
            &DockerBankInfoProvider::new(),
            &mut DockerIpProvider::test(),
            TrustKeySet::test(),
            &enabled_banks,
            auth,
        );
        let bank_urls = DummyBankUrlProvider;

        // When
        let banks = build_banks_config(&trust, &bank_urls);

        // Then
        assert!(banks.contains(&TrustBanksConfig::Mcb(default_mcb_config())));
        assert!(banks.contains(&TrustBanksConfig::TreasuryPrime(default_provident_config())));
        assert!(banks.contains(&TrustBanksConfig::TreasuryPrime(default_test_one_config())));
        assert!(banks.contains(&TrustBanksConfig::TreasuryPrime(default_test_two_config())));
    }

    #[test]
    fn build_banks_config__returns_no_banks_when_no_banks_enabled() {
        // Given
        let enabled_banks = HashSet::new();
        let auth = Some(AuthProvider::get_test_xand_api_jwt_info());
        let trust = Trust::new(
            &DockerBankInfoProvider::new(),
            &mut DockerIpProvider::test(),
            TrustKeySet::test(),
            &enabled_banks,
            auth,
        );
        let bank_urls = DummyBankUrlProvider;

        // When
        let banks = build_banks_config(&trust, &bank_urls);

        // Then
        assert!(banks.is_empty());
    }

    #[test]
    fn build_banks_config__returns_partial_banks_when_two_banks_enabled() {
        // Given
        let enabled_banks = [Bank::Mcb, Bank::Provident].iter().cloned().collect();
        let auth = Some(AuthProvider::get_test_xand_api_jwt_info());
        let trust = Trust::new(
            &DockerBankInfoProvider::new(),
            &mut DockerIpProvider::test(),
            TrustKeySet::test(),
            &enabled_banks,
            auth,
        );
        let bank_urls = DummyBankUrlProvider;

        // When
        let banks = build_banks_config(&trust, &bank_urls);

        // Then
        assert!(banks.contains(&TrustBanksConfig::Mcb(default_mcb_config())));
        assert!(banks.contains(&TrustBanksConfig::TreasuryPrime(default_provident_config())));
    }
}
