use std::collections::{HashMap, HashSet};

use crate::{
    app::{
        contexts::docker_compose::bank_secrets::build_bank_secrets_map, entities::trust::Trust,
        xand_services::secret_store_consts::XAND_API_LOOKUP_KEY_JWTTOKEN,
    },
    contracts::data::bank_info::Bank,
};

#[derive(Debug, Clone, Serialize)]
pub struct TrustSecretsMap {
    #[serde(
        flatten,
        serialize_with = "serde_ordered_collections::map::sorted_serialize"
    )]
    secrets: HashMap<String, String>,
}

impl TrustSecretsMap {
    pub fn new(trust: &Trust) -> Self {
        let mut secrets = HashMap::new();

        Self::insert_bank_secrets(
            &mut secrets,
            &trust.accounts().keys().into_iter().cloned().collect(),
        );

        Self::insert_auth_secrets(&mut secrets, trust);

        Self { secrets }
    }

    fn insert_bank_secrets(map: &mut HashMap<String, String>, enabled_banks: &HashSet<Bank>) {
        let values = build_bank_secrets_map(enabled_banks);
        map.extend(values)
    }

    fn insert_auth_secrets(map: &mut HashMap<String, String>, trust: &Trust) {
        if let Some(auth) = trust.xand_api_auth() {
            map.insert(
                XAND_API_LOOKUP_KEY_JWTTOKEN.to_string(),
                auth.token().0.clone(),
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::{HashMap, HashSet};

    use trustee_node_config::trust_banks::{McbConfig, TreasuryPrimeConfig, TrustBanksConfig};

    use crate::{
        app::{
            contexts::docker_compose::{
                entities::docker_compose_trust::conversion::build_config,
                services::bank_url_provider::tests::DummyBankUrlProvider,
            },
            entities::trust::Trust,
            xand_services::secret_store_consts::XAND_API_LOOKUP_KEY_JWTTOKEN,
        },
        contracts::network::auth::{Jwt, JwtInfo, JwtSecret},
    };

    use super::*;

    #[test]
    fn build_secrets__secret_store_keys_match_keys_from_config() {
        // Given
        let mut trust = Trust::test();
        let jwt_info = JwtInfo {
            secret: JwtSecret("test-secret".into()),
            token: Jwt("test-token".into()),
        };
        trust.state.xand_api_auth = Some(jwt_info);

        let bank_urls = DummyBankUrlProvider;
        let config = build_config(
            &trust,
            "https://some-xand-api.com/".into(),
            &Default::default(),
            &Default::default(),
            &bank_urls,
        );

        // When
        let secrets = TrustSecretsMap::new(&trust);

        // Then
        let mut expected_secrets = HashSet::new();
        for config in config.banks {
            match config {
                TrustBanksConfig::Mcb(mcb) => {
                    let McbConfig {
                        secret_key_username: mcb_secret_key_username,
                        secret_key_password: mcb_secret_key_password,
                        secret_key_client_app_ident,
                        secret_key_organization_id,
                        ..
                    } = mcb;
                    expected_secrets.insert(mcb_secret_key_username);
                    expected_secrets.insert(mcb_secret_key_password);
                    expected_secrets.insert(secret_key_client_app_ident);
                    expected_secrets.insert(secret_key_organization_id);
                }
                TrustBanksConfig::TreasuryPrime(treasury_prime) => {
                    let TreasuryPrimeConfig {
                        secret_key_username: treasury_prime_secret_key_username,
                        secret_key_password: treasury_prime_secret_key_password,
                        ..
                    } = treasury_prime;
                    expected_secrets.insert(treasury_prime_secret_key_username);
                    expected_secrets.insert(treasury_prime_secret_key_password);
                }
            }
        }
        let expected_xand_api_jwt_key = XAND_API_LOOKUP_KEY_JWTTOKEN.into();
        expected_secrets.insert(expected_xand_api_jwt_key);

        let registered_secret_keys = secrets.secrets.keys().cloned().collect::<HashSet<_>>();
        assert_eq!(registered_secret_keys, expected_secrets);
    }

    #[test]
    fn new__when_trust_xand_api_has_auth_enabled_secrets_store_contains_secret() {
        // Given
        let mut trust = Trust::test();
        let jwt_info = JwtInfo {
            secret: JwtSecret("test-secret".into()),
            token: Jwt("test-token".into()),
        };
        trust.state.xand_api_auth = Some(jwt_info);

        // When
        let secrets = TrustSecretsMap::new(&trust);

        // Then
        assert_eq!(
            secrets.secrets.get(XAND_API_LOOKUP_KEY_JWTTOKEN),
            Some(&"test-token".to_string())
        );
    }

    #[test]
    fn yaml_to_string__produces_correctly_sorted_entries_consistently() {
        for _i in 0..100 {
            // Given
            let data = TrustSecretsMap {
                secrets: {
                    let mut map = HashMap::new();
                    map.insert("d".into(), "jkl".into());
                    map.insert("a".into(), "abc".into());
                    map.insert("e".into(), "mno".into());
                    map.insert("c".into(), "ghi".into());
                    map.insert("b".into(), "def".into());

                    map
                },
            };

            // When
            let string = serde_yaml::to_string(&data).unwrap();

            // Then
            assert_eq!(
                string,
                r###"---
a: abc
b: def
c: ghi
d: jkl
e: mno
"###
            );
        }
    }
}
