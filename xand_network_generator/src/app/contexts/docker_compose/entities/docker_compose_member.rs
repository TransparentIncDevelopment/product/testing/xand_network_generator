use crate::{
    app::{
        contexts::docker_compose::{
            entities::docker_compose_member::secret_store::MemberSecretsMap,
            service_naming_convention::ServiceNamingConvention,
            services::non_consensus_node::{
                non_consensus_node_spec::NonConsensusNodeSpec, NonConsensusNode,
                STARTING_MEMBERS_XAND_API_HOST_PORT,
            },
        },
        data::ImageVersion,
        entities::member::{states::banked_member::DeployableMember, Member},
        external_wrappers::chain_spec_generation::ChainSpec,
        xand_services::dir_structure::{
            member_api::directory_convention::MemberApiDirConvention, XandNodeDirConventionBuilder,
        },
    },
    contracts::network::metadata::bootnode_url_vec::BootnodeUrlVec,
};
use compose_yml::v2::{value, CommandLine, Image, PortMapping, Service, VolumeMount};
use std::{collections::BTreeMap, convert::TryInto};

mod secret_store;
#[cfg(test)]
mod tests;

pub struct DockerComposeMember {
    pub member: Member<DeployableMember>,
    pub index: usize,
    version: ImageVersion,
    validator_version: ImageVersion,
    pub depends_on_services: Vec<String>,
    bootnode_url_vec: BootnodeUrlVec,
    pub chain_spec: ChainSpec,
}

impl DockerComposeMember {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        member: Member<DeployableMember>,
        index: usize,
        version: ImageVersion,
        validator_version: ImageVersion,
        depends_on_services: Vec<String>,
        bootnode_url_vec: BootnodeUrlVec,
        chain_spec: ChainSpec,
    ) -> Self {
        Self {
            member,
            index,
            version,
            validator_version,
            depends_on_services,
            bootnode_url_vec,
            chain_spec,
        }
    }

    pub fn get_member_api_service_name(&self) -> String {
        ServiceNamingConvention::member(self.index)
    }

    pub fn get_nc_node_service_name(&self) -> String {
        ServiceNamingConvention::non_consensus_node("member", self.index)
    }

    pub fn build_nc_node_service(&self, rel_path: XandNodeDirConventionBuilder) -> Service {
        let multi_addr_dns = format!("/dns4/{}", self.get_nc_node_service_name());
        let node_name = format!("MemberNode{}", self.index);
        let xand_api_host_port = self.xand_api_host_port();

        let maybe_jwt_secret_filepath = self
            .member
            .xand_api_auth()
            .as_ref()
            .map(|_auth| rel_path.get_xand_api_jwt_secret_path());

        let ncn_spec = NonConsensusNodeSpec {
            validator_version: self.validator_version.clone(),
            node_name,
            bootnode_url_vec: self.bootnode_url_vec.clone(),
            multi_addr_dns,
            rel_path_to_service_dir: rel_path.0,
            xand_api_host_port,
            xand_api_jwt_secret_path: maybe_jwt_secret_filepath,
        };

        let mut s = NonConsensusNode::build(ncn_spec);

        s.depends_on = self
            .depends_on_services
            .clone()
            .into_iter()
            .map(value)
            .collect();
        s
    }

    pub fn build_member_api_service(
        &self,
        structured_member_api_dir: MemberApiDirConvention,
    ) -> Service {
        let image = Image::new(self.version.to_string()).unwrap();

        let mut s = Service::default();

        s.entrypoint = Some(CommandLine::ShellCode(value("./member-api".into())));
        s.image = Some(value(image));

        s.networks = {
            let mut networks = BTreeMap::new();
            networks.insert("xand".into(), Default::default());
            networks
        };

        let host_port: u16 = self.member_api_host_port();
        s.ports = vec![value(PortMapping::new(host_port, 3000))];

        s.volumes = vec![value(VolumeMount::host(
            structured_member_api_dir.config_dir(),
            "/etc/member-api",
        ))];
        let non_consensus_node_service =
            ServiceNamingConvention::non_consensus_node("member", self.index);

        s.depends_on = self
            .depends_on_services
            .clone()
            .into_iter()
            .chain(vec![non_consensus_node_service])
            .map(value)
            .collect();

        s
    }

    pub fn member_api_secrets_store(&self) -> MemberSecretsMap {
        MemberSecretsMap::new(&self.member)
    }

    pub fn xand_api_host_port(&self) -> usize {
        STARTING_MEMBERS_XAND_API_HOST_PORT + self.index
    }

    pub fn member_api_host_port(&self) -> u16 {
        (3000 + self.index)
            .try_into()
            .expect("Host port selection for member-api overflowed u16")
    }
}
