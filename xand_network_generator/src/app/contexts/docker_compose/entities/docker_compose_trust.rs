pub mod conversion;
mod secret_store;
#[cfg(test)]
mod tests;

use crate::{
    app::{
        contexts::docker_compose::{
            docker_compose_network::structured_docker_compose_root::StructuredDockerComposeConv,
            entities::docker_compose_trust::secret_store::TrustSecretsMap,
            service_naming_convention::ServiceNamingConvention,
            services::non_consensus_node::{
                non_consensus_node_spec::NonConsensusNodeSpec, NonConsensusNode, TRUST_API_PORT,
                TRUST_XAND_API_HOST_PORT,
            },
        },
        data::ImageVersion,
        entities::trust::Trust,
        external_wrappers::chain_spec_generation::ChainSpec,
        xand_services::dir_structure::{XandNodeDirBuilder, XandNodeDirConventionBuilder},
    },
    contracts::network::metadata::{
        bootnode_url_vec::BootnodeUrlVec,
        xand_entity_set_summary::{TrustMetadata, XandApiDetails},
    },
};
use compose_yml::v2::{value, CommandLine, Image, PortMapping, RestartMode, Service, VolumeMount};
use std::collections::BTreeMap;
use tpfs_krypt::KeyPair;
use url::Url;

pub struct DockerComposeTrust {
    pub trust: Trust,
    version: ImageVersion,
    trust_api_version: ImageVersion,
    validator_version: ImageVersion,
    depends_on_services: Vec<String>,
    bootnode_url_vec: BootnodeUrlVec,
    pub chain_spec: ChainSpec,
}

impl DockerComposeTrust {
    pub fn new(
        trust: Trust,
        version: ImageVersion,
        trust_api_version: ImageVersion,
        validator_version: ImageVersion,
        depends_on_services: Vec<String>,
        bootnode_url_vec: BootnodeUrlVec,
        chain_spec: ChainSpec,
    ) -> Self {
        Self {
            trust,
            version,
            trust_api_version,
            validator_version,
            depends_on_services,
            bootnode_url_vec,
            chain_spec,
        }
    }

    pub fn get_trust_service_name(&self) -> String {
        ServiceNamingConvention::trustee()
    }

    pub fn get_trust_api_service_name(&self) -> String {
        ServiceNamingConvention::trust_api()
    }

    pub fn get_nc_node_service_name(&self) -> String {
        ServiceNamingConvention::trust_non_consensus_node()
    }

    pub fn build_nc_node_service(&self, dir_convention: &XandNodeDirConventionBuilder) -> Service {
        let multi_addr_dns = format!("/dns4/{}", self.get_nc_node_service_name());
        let rel_path_to_service_dir = dir_convention.0.clone();
        let node_name = "TrustNode".to_string();

        let maybe_jwt_secret_filepath = self
            .trust
            .xand_api_auth()
            .as_ref()
            .map(|_auth| dir_convention.get_xand_api_jwt_secret_path());

        let ncn_spec = NonConsensusNodeSpec {
            validator_version: self.validator_version.clone(),
            node_name,
            bootnode_url_vec: self.bootnode_url_vec.clone(),
            multi_addr_dns,
            rel_path_to_service_dir,
            xand_api_host_port: TRUST_XAND_API_HOST_PORT,
            xand_api_jwt_secret_path: maybe_jwt_secret_filepath,
        };

        let mut s = NonConsensusNode::build(ncn_spec);

        s.depends_on = self
            .depends_on_services
            .clone()
            .into_iter()
            .map(value)
            .collect();
        s
    }

    pub fn build_trust_service(&self, dir_conv: &StructuredDockerComposeConv) -> Service {
        let image = Image::new(self.version.to_string()).unwrap();

        let mut s = Service::default();

        s.entrypoint = Some(CommandLine::ShellCode(value("./trustee-node".into())));

        s.command = Some(CommandLine::Parsed(vec![
            value("/etc/trustee-node/config".into()),
            value("--allow-create-db".into()),
        ]));

        s.image = Some(value(image));

        s.networks = {
            let mut networks = BTreeMap::new();
            networks.insert("xand".into(), Default::default());
            networks
        };
        s.restart = Some(value(RestartMode::OnFailure(None)));

        s.environment = {
            let mut map: BTreeMap<String, _> = BTreeMap::new();
            vec![("XAND_HUMAN_LOGGING", "true".into())]
                .into_iter()
                .for_each(|(k, v)| {
                    map.insert(k.to_string(), value(v));
                });
            map
        };

        let path_to_trust_software_dir = dir_conv.trust_software_dir();
        s.volumes = vec![value(VolumeMount::host(
            path_to_trust_software_dir.join("config"),
            "/etc/trustee-node/config",
        ))];

        let non_consensus_node_service = ServiceNamingConvention::trust_non_consensus_node();

        s.depends_on = self
            .depends_on_services
            .clone()
            .into_iter()
            .chain(vec![non_consensus_node_service])
            .map(value)
            .collect();

        s
    }

    pub(crate) fn build_trust_api_service(
        &self,
        dir_conv: &StructuredDockerComposeConv,
    ) -> Service {
        let image = Image::new(self.trust_api_version.to_string()).unwrap();

        let mut s = Service::default();

        s.image = Some(value(image));

        s.networks = {
            let mut networks = BTreeMap::new();
            networks.insert("xand".into(), Default::default());
            networks
        };
        s.restart = Some(value(RestartMode::OnFailure(None)));

        s.ports = vec![value(PortMapping::new(TRUST_API_PORT, TRUST_API_PORT))];

        s.environment = {
            let mut map: BTreeMap<String, _> = BTreeMap::new();
            vec![("XAND_HUMAN_LOGGING", "true".into())]
                .into_iter()
                .for_each(|(k, v)| {
                    map.insert(k.to_string(), value(v));
                });
            map
        };

        let path_to_trust_api_dir = dir_conv.trust_api_dir();
        s.volumes = vec![value(VolumeMount::host(
            path_to_trust_api_dir.join("config"),
            "/etc/xand/trust-api/config",
        ))];

        let non_consensus_node_service = ServiceNamingConvention::trust_non_consensus_node();

        s.depends_on = self
            .depends_on_services
            .clone()
            .into_iter()
            .chain(vec![non_consensus_node_service])
            .map(value)
            .collect();

        s
    }

    pub fn secrets_store(&self) -> TrustSecretsMap {
        TrustSecretsMap::new(&self.trust)
    }

    pub fn serialize_xand_node_config_to(&self, dir: XandNodeDirBuilder) -> crate::Result<()> {
        dir.write_trust(self)?;
        Ok(())
    }

    pub fn metadata(&self) -> TrustMetadata {
        let address = self.trust.get_address().0.to_string();
        let xand_api_url =
            Url::parse(&format!("http://localhost:{}", TRUST_XAND_API_HOST_PORT)).unwrap();
        let trust_api_url = Url::parse(&format!("http://localhost:{}", TRUST_API_PORT)).unwrap();

        let accounts = &self.trust.state.acct_set;
        let banks = std::iter::empty()
            .chain(accounts.mcb.iter())
            .chain(accounts.provident.iter())
            .chain(accounts.test_bank_one.iter())
            .chain(accounts.test_bank_two.iter())
            .cloned()
            .collect();

        TrustMetadata {
            address,
            trust_api_url,
            xand_api_details: XandApiDetails {
                xand_api_url,
                auth: self.trust.state.xand_api_auth.clone(),
            },
            banks,
            encryption_public_key: self.trust.encryption_key_pair().identifier().value,
        }
    }
}
