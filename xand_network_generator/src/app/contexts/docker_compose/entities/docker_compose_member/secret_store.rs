use std::collections::HashMap;

use crate::app::{
    contexts::docker_compose::bank_secrets::build_bank_secrets_map,
    entities::member::{states::banked_member::DeployableMember, Member},
    xand_services::secret_store_consts::{
        MEMBER_API_LOOKUP_KEY_JWTSECRET, XAND_API_LOOKUP_KEY_JWTTOKEN,
    },
};

#[derive(Debug, Clone, Serialize)]
pub struct MemberSecretsMap {
    #[serde(
        flatten,
        serialize_with = "serde_ordered_collections::map::sorted_serialize"
    )]
    values: HashMap<String, String>,
}

impl MemberSecretsMap {
    pub fn new(entity: &Member<DeployableMember>) -> MemberSecretsMap {
        let banked_member = entity.state().borrow();

        let mut secrets = HashMap::new();

        Self::insert_bank_secrets(&mut secrets, banked_member);
        Self::insert_auth_secrets(&mut secrets, banked_member);

        MemberSecretsMap { values: secrets }
    }

    fn insert_bank_secrets(map: &mut HashMap<String, String>, member: &DeployableMember) {
        let needed_banks = member.member_bank_info.iter().map(|c| c.bank).collect();
        let bank_secrets = build_bank_secrets_map(&needed_banks);
        map.extend(bank_secrets);
    }

    fn insert_auth_secrets(map: &mut HashMap<String, String>, member: &DeployableMember) {
        if let Some(jwt) = &member.member_api_auth {
            map.insert(
                MEMBER_API_LOOKUP_KEY_JWTSECRET.to_string(),
                jwt.secret().0.to_string(),
            );
        }

        if let Some(jwt) = &member.xand_api_auth {
            map.insert(
                XAND_API_LOOKUP_KEY_JWTTOKEN.to_string(),
                jwt.token().0.to_string(),
            );
        }
    }

    pub fn contains_key<S: AsRef<str>>(&self, k: S) -> bool {
        self.values.contains_key(k.as_ref())
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::MemberSecretsMap;

    #[test]
    fn yaml_to_string__produces_correctly_sorted_entries_consistently() {
        for _i in 0..100 {
            // Given
            let data = MemberSecretsMap {
                values: {
                    let mut map = HashMap::new();
                    map.insert("d".into(), "jkl".into());
                    map.insert("a".into(), "abc".into());
                    map.insert("e".into(), "mno".into());
                    map.insert("c".into(), "ghi".into());
                    map.insert("b".into(), "def".into());

                    map
                },
            };

            // When
            let string = serde_yaml::to_string(&data).unwrap();

            // Then
            assert_eq!(
                string,
                r###"---
a: abc
b: def
c: ghi
d: jkl
e: mno
"###
            );
        }
    }
}
