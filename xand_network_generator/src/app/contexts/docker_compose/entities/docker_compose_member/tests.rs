use super::*;

impl DockerComposeMember {
    #[cfg(test)]
    pub fn test() -> Self {
        Self {
            member: Member::<DeployableMember>::test(),
            index: 0,
            version: ImageVersion("gcr.io/xand-dev/member-api:1.9.0".into()),
            validator_version: ImageVersion("gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac".into()),
            depends_on_services: vec!["validator-0", "trustee", "bank-mocks"]
                .into_iter()
                .map(Into::into)
                .collect(),

            bootnode_url_vec: vec!["/dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck"].into(),
            chain_spec: ChainSpec::test()
        }
    }
}

mod non_consensus_node {
    use crate::app::{
        contexts::docker_compose::entities::docker_compose_member::DockerComposeMember,
        entities::member::{states::banked_member::DeployableMember, Member},
        xand_services::dir_structure::XandNodeDirConventionBuilder,
    };
    use compose_yml::v2::{value, Service};
    use insta::assert_yaml_snapshot;
    use std::path::PathBuf;

    #[test]
    fn get_nc_node_service_name__incorporates_index_in_service_name() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        dc_member.index = 1;

        // When
        let name = dc_member.get_nc_node_service_name();

        // Then
        assert_eq!(name, "member-non-consensus-node-1");
    }

    #[test]
    fn build_nc_node_service__depends_on_given_service() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        let test_service = "testdependentservice";
        dc_member.depends_on_services = vec![test_service.into()];
        let root_path = PathBuf::from("./members/0");

        // When
        let service = dc_member.build_nc_node_service(root_path.into());

        // Then
        assert!(service.depends_on.contains(&value(test_service.into())));
    }

    #[test]
    fn build_nc_node_service__service_matches_expected() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        dc_member.index = 0;
        let test_service = "testdependentservice";
        dc_member.depends_on_services = vec![test_service.into()];
        dc_member.bootnode_url_vec = vec!["/dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck /dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ"].into();
        let root_path = PathBuf::from("./members/0/xand-node");

        // When
        let service = dc_member.build_nc_node_service(root_path.into());

        // Then
        let expected_yaml = r#"---
image: gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac
command: ["--base-path", "/xand/validator", "--no-mdns", "--unsafe-ws-external",
          "--unsafe-rpc-external", "--rpc-cors", "all", "--prometheus-external",
          "--execution", "NativeElseWasm", "--public-addr", "/dns4/member-non-consensus-node-0",
          "--jwt-secret-path", "/etc/xand/jwt-secret.txt"]
volumes:
  - ./members/0/xand-node/xand/chainspec/chainSpecModified.json:/xand/chainspec/chainSpecModified.json
  - ./members/0/xand-node/xand/validator:/xand/validator/
  - ./members/0/xand-node/xand/keypairs:/xand/keypairs
  - ./members/0/xand-node/xand-api/initial-keys:/etc/xand-keys
  - ./members/0/xand-node/xand-api/key-management:/xand/keys
  - ./members/0/xand-node/xand-api/jwt-secret.txt:/etc/xand/jwt-secret.txt
environment:
  CONSENSUS_NODE: 'false'
  BOOTNODE_URL_VEC: /dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck /dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ
  NODE_NAME: MemberNode0
  TELEMETRY_URL: ws://substrate-telemetry:1024 0
  EMIT_METRICS: "true"
  XAND_HUMAN_LOGGING: "true"
ports:
  - "10044:10044"
expose:
  - "8933"
  - "9944"
  - "30333"
  - "10044"
depends_on:
  - testdependentservice
networks:
  xand: {}"#;
        let expected_service: Service = serde_yaml::from_str(expected_yaml).unwrap();
        assert_eq!(service, expected_service);
    }

    #[test]
    fn build_nc_node_service__when_member_auth_is_none_then_serialized_service_excludes_jwt_flags_and_volume(
    ) {
        // Given
        let auth = None;
        let root_dir = XandNodeDirConventionBuilder("./some/root/dir".into());
        let member = Member::<DeployableMember>::with_auth(auth);
        let mut dcm = DockerComposeMember::test();
        dcm.member = member;

        // When
        let service = dcm.build_nc_node_service(root_dir);

        // Then
        assert_yaml_snapshot!(service);
    }
}

mod member_api {
    use crate::app::contexts::docker_compose::{
        entities::docker_compose_member::DockerComposeMember,
        service_naming_convention::ServiceNamingConvention,
    };
    use compose_yml::v2::{value, VolumeMount};
    use std::path::PathBuf;

    #[test]
    fn get_member_api_service_name__incorporates_index_in_service_name() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        dc_member.index = 1;

        // When
        let name = dc_member.get_member_api_service_name();

        // Then
        assert_eq!(name, "member-api-1");
    }

    #[test]
    fn build_member_api_service__volume_path_incorporates_index() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        dc_member.index = 2;

        // When
        let service = dc_member.build_member_api_service("./members/1".into());

        // Then
        let actual_volume = service.volumes.get(0).unwrap();
        let expected_volume = &value(VolumeMount::host(
            PathBuf::from("./members/1").join("config"),
            "/etc/member-api",
        ));
        assert_eq!(actual_volume, expected_volume);
    }

    #[test]
    fn build_member_api_service__depends_on_indexed_non_consensus_node() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        dc_member.index = 0;

        // When
        let service = dc_member.build_member_api_service("./".into());

        // Then
        let expected_non_consensus_node_service =
            ServiceNamingConvention::non_consensus_node("member", 0);
        assert!(service
            .depends_on
            .contains(&value(expected_non_consensus_node_service)));
    }

    #[test]
    fn build_member_api_service__serialized_service_matches_expected() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        dc_member.index = 4;
        let service = dc_member.build_member_api_service("./members/4/member-api".into());

        // When
        let serialized = serde_yaml::to_string(&service).unwrap();

        // Then
        let expected_yaml = r#"---
depends_on:
  - validator-0
  - trustee
  - bank-mocks
  - member-non-consensus-node-4
entrypoint: "./member-api"
image: "gcr.io/xand-dev/member-api:1.9.0"
networks:
  xand: {}
ports:
  - "3004:3000"
volumes:
  - "./members/4/member-api/config:/etc/member-api"
"#;
        assert_eq!(serialized, expected_yaml);
    }
}
