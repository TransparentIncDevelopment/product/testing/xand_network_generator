use super::*;
use crate::app::contexts::docker_compose::docker_compose_network::structured_docker_compose_root::StructuredDockerComposeConv;
use insta::assert_yaml_snapshot;

impl DockerComposeLimitedAgent {
    #[cfg(test)]
    pub fn test() -> Self {
        Self {
            limited_agent: LimitedAgent::<DeployableLimitedAgent>::test(),
            validator_version: ImageVersion("gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac"
                .into()),
            depends_on_services: vec![],
            bootnode_url_vec: vec!["/dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck"].into(),
            chain_spec: ChainSpec::test()
        }
    }
}

#[test]
fn build_nc_node_service__service_matches_expected() {
    // Given
    let mut dc_la = DockerComposeLimitedAgent::test();
    let test_service = "testdependentservice";
    dc_la.depends_on_services = vec![test_service.into()];
    dc_la.bootnode_url_vec = vec!["/dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck /dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ"].into();
    let root_dir = StructuredDockerComposeConv {}.limited_agent_nc_node_dir();
    let dir_builder = XandNodeDirConventionBuilder::from(root_dir);

    // When
    let service = dc_la.build_nc_node_service(&dir_builder);

    // Then
    let expected_yaml = r#"---
image: gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac
command: ["--base-path", "/xand/validator", "--no-mdns", "--unsafe-ws-external",
          "--unsafe-rpc-external", "--rpc-cors", "all", "--prometheus-external",
          "--execution", "NativeElseWasm", "--public-addr", "/dns4/limited-agent-non-consensus-node",
          "--jwt-secret-path", "/etc/xand/jwt-secret.txt"]
volumes:
  - ./limited-agent/xand-node/xand/chainspec/chainSpecModified.json:/xand/chainspec/chainSpecModified.json
  - ./limited-agent/xand-node/xand/validator:/xand/validator/
  - ./limited-agent/xand-node/xand/keypairs:/xand/keypairs
  - ./limited-agent/xand-node/xand-api/initial-keys:/etc/xand-keys
  - ./limited-agent/xand-node/xand-api/key-management:/xand/keys
  - ./limited-agent/xand-node/./xand-api/jwt-secret.txt:/etc/xand/jwt-secret.txt
environment:
  CONSENSUS_NODE: 'false'
  BOOTNODE_URL_VEC: /dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck /dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ
  NODE_NAME: LimitedAgentNode
  TELEMETRY_URL: ws://substrate-telemetry:1024 0
  EMIT_METRICS: "true"
  XAND_HUMAN_LOGGING: "true"
ports:
  - "11001:10044"
expose:
  - "8933"
  - "9944"
  - "30333"
  - "10044"
depends_on:
  - testdependentservice
networks:
  xand: {}"#;
    let expected_service: Service = serde_yaml::from_str(expected_yaml).unwrap();
    assert_eq!(service, expected_service);
}

#[test]
fn build_service__when_limited_agent_auth_is_none_then_serialized_service_excludes_jwt_flags_and_volume(
) {
    // Given
    let auth = None;
    let root_dir = XandNodeDirConventionBuilder("./some/root/dir".into());
    let la = LimitedAgent::with_auth(auth);
    let mut dcla = DockerComposeLimitedAgent::test();
    dcla.limited_agent = la;

    // When
    let service = dcla.build_nc_node_service(&root_dir);

    // Then
    assert_yaml_snapshot!(service);
}
