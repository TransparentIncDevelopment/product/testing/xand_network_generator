use std::collections::{HashMap, HashSet};

use crate::contracts::data::bank_info::Bank;

pub const MCB_CLIENT_APP_IDENT_KEY: &str = "metropolitan-client-app-ident";
pub const MCB_ORGANIZATION_ID_KEY: &str = "metropolitan-organization-id";

pub fn username_secret_key_for(bank: Bank) -> String {
    format!("{}-username", bank.get_url_name())
}

pub fn password_secret_key_for(bank: Bank) -> String {
    format!("{}-password", bank.get_url_name())
}

pub fn build_bank_secrets_map(enabled_banks: &HashSet<Bank>) -> HashMap<String, String> {
    enabled_banks
        .iter()
        .flat_map(|bank| {
            let mut bank_secrets = vec![
                (
                    username_secret_key_for(*bank),
                    format!("my-fake-{}-username", bank.get_url_name()),
                ),
                (
                    password_secret_key_for(*bank),
                    format!("my-fake-{}-password", bank.get_url_name()),
                ),
            ];

            if *bank == Bank::Mcb {
                bank_secrets.push((
                    MCB_CLIENT_APP_IDENT_KEY.to_string(),
                    "client-app-ident".to_string(),
                ));
                bank_secrets.push((
                    MCB_ORGANIZATION_ID_KEY.to_string(),
                    "organization_id".to_string(),
                ));
            }

            bank_secrets.into_iter()
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use std::collections::{HashMap, HashSet};

    use crate::{
        app::contexts::docker_compose::bank_secrets::build_bank_secrets_map,
        contracts::data::bank_info::Bank,
    };

    #[test]
    fn build_secrets_map__returns_empty_when_no_banks_configured() {
        // Given
        let enabled_banks = HashSet::default();

        // When
        let secrets_map = build_bank_secrets_map(&enabled_banks);

        // Then
        assert_eq!(secrets_map, HashMap::default());
    }

    #[test]
    fn build_secrets_map__returns_mcb_specific_keys() {
        // Given
        let enabled_banks = [Bank::Mcb].iter().copied().collect();

        // When
        let secrets_map = build_bank_secrets_map(&enabled_banks);

        // Then
        assert_eq!(
            secrets_map,
            [
                ("metropolitan-username", "my-fake-metropolitan-username"),
                ("metropolitan-password", "my-fake-metropolitan-password"),
                ("metropolitan-organization-id", "organization_id"),
                ("metropolitan-client-app-ident", "client-app-ident"),
            ]
            .iter()
            .cloned()
            .map(|(k, v)| (k.into(), v.into()))
            .collect()
        );
    }

    #[test]
    fn build_secrets_map__returns_generic_username_and_password() {
        // Given
        let enabled_banks = [Bank::TestBankOne].iter().copied().collect();

        // When
        let secrets_map = build_bank_secrets_map(&enabled_banks);

        // Then
        assert_eq!(
            secrets_map,
            [
                ("test_bank_one-username", "my-fake-test_bank_one-username"),
                ("test_bank_one-password", "my-fake-test_bank_one-password"),
            ]
            .iter()
            .cloned()
            .map(|(k, v)| (k.into(), v.into()))
            .collect()
        );
    }

    #[test]
    fn build_secrets_map__returns_secrets_for_multiple_banks_at_once() {
        // Given
        let enabled_banks = [Bank::Mcb, Bank::Provident].iter().copied().collect();

        // When
        let secrets_map = build_bank_secrets_map(&enabled_banks);

        // Then
        assert_eq!(
            secrets_map,
            [
                ("metropolitan-username", "my-fake-metropolitan-username"),
                ("metropolitan-password", "my-fake-metropolitan-password"),
                ("metropolitan-organization-id", "organization_id"),
                ("metropolitan-client-app-ident", "client-app-ident"),
                ("provident-username", "my-fake-provident-username"),
                ("provident-password", "my-fake-provident-password"),
            ]
            .iter()
            .cloned()
            .map(|(k, v)| (k.into(), v.into()))
            .collect()
        );
    }

    #[test]
    fn build_secrets_map__returns_secrets_for_all_banks_at_once() {
        // Given
        let enabled_banks = [
            Bank::Mcb,
            Bank::Provident,
            Bank::TestBankOne,
            Bank::TestBankTwo,
        ]
        .iter()
        .copied()
        .collect();

        // When
        let secrets_map = build_bank_secrets_map(&enabled_banks);

        // Then
        assert_eq!(
            secrets_map,
            [
                // mcb
                ("metropolitan-username", "my-fake-metropolitan-username"),
                ("metropolitan-password", "my-fake-metropolitan-password"),
                ("metropolitan-organization-id", "organization_id"),
                ("metropolitan-client-app-ident", "client-app-ident"),
                // provident
                ("provident-username", "my-fake-provident-username"),
                ("provident-password", "my-fake-provident-password"),
                // test_bank_one
                ("test_bank_one-username", "my-fake-test_bank_one-username"),
                ("test_bank_one-password", "my-fake-test_bank_one-password"),
                // test_bank_two
                ("test_bank_two-username", "my-fake-test_bank_two-username"),
                ("test_bank_two-password", "my-fake-test_bank_two-password"),
            ]
            .iter()
            .cloned()
            .map(|(k, v)| (k.into(), v.into()))
            .collect()
        );
    }
}
