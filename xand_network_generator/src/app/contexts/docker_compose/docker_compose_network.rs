use self::bank_mocks::DockerComposeBankUrlProvider;
use crate::app::contexts::docker_compose::docker_compose_network::docker_xand_entity_set_builder::DockerXandEntitySetBuilder;
use crate::contracts::data::NetworkParticipant;
use crate::contracts::network::metadata::xand_entity_set_summary::XandEntitySetSummary;
use crate::{
    app::{
        contexts::docker_compose::{
            docker_compose_network::{
                docker_xand_entity_set::DockerXandEntitySet,
                structured_docker_compose_root::{
                    StructuredDockerComposeConv, StructuredDockerComposeRoot,
                },
            },
            service_naming_convention::ServiceNamingConvention,
        },
        data::ImageSet,
        external_wrappers::chain_spec_generation::ChainSpec,
        file_io,
        network::XandEntitySet,
        xand_services::dir_structure::{
            member_api::directory_convention::MemberApiDirConvention, XandNodeDirConventionBuilder,
        },
    },
    contracts::{data::bank_info::BankInfo, network::metadata::bootnode_url_vec::BootnodeUrlVec},
};
use compose_yml::v2::{value, File, Network, Service};
use std::collections::{BTreeMap, HashMap};

#[cfg(test)]
mod tests;

mod bank_mocks;
pub(crate) mod docker_xand_entity_set;
mod docker_xand_entity_set_builder;
pub(crate) mod structured_docker_compose_root;

pub struct DockerComposeNetwork {
    docker_xand_entity_set: DockerXandEntitySet,
}

impl DockerComposeNetwork {
    pub fn new(
        entity_set: XandEntitySet,
        image_version_set: ImageSet,
        chain_spec: ChainSpec,
    ) -> Self {
        let bootnode_url_vec = Self::bootnode_url_vec(&entity_set);
        let builder = DockerXandEntitySetBuilder::new(
            entity_set,
            image_version_set,
            bootnode_url_vec,
            chain_spec,
        );
        let docker_xand_entity_set = builder.build();
        Self {
            docker_xand_entity_set,
        }
    }

    pub fn build_docker_compose_yaml(&self) -> File {
        let des = &self.docker_xand_entity_set;

        let convention = StructuredDockerComposeConv {};

        let mut services: BTreeMap<String, Service> = BTreeMap::new();

        // Add bank mocks
        let bank_mocks = self.docker_xand_entity_set.banks.clone();
        services.insert(
            bank_mocks.get_service_name(),
            bank_mocks.build_service(convention.bank_mocks()),
        );

        // Add validators
        for (idx, v) in des
            .permissioned_validators
            .iter()
            .chain(des.extra_validators.iter())
            .enumerate()
        {
            let rel_path = convention.validator_dir(idx);

            let xand_node_dir = XandNodeDirConventionBuilder::from(rel_path);
            services.insert(
                v.get_service_name(),
                v.build_validator_service(xand_node_dir),
            );
        }

        // Add members and their nc_nodes
        for (idx, m) in des
            .permissioned_members
            .iter()
            .chain(des.extra_members.iter())
            .enumerate()
        {
            let rel_path_api = convention.member_api_dir(idx);
            let member_api_convention = MemberApiDirConvention::from(rel_path_api);
            services.insert(
                m.get_member_api_service_name(),
                m.build_member_api_service(member_api_convention),
            );

            let rel_path_nc_node = convention.member_nc_node_dir(idx);
            let xand_node_dir = XandNodeDirConventionBuilder::from(rel_path_nc_node);
            services.insert(
                m.get_nc_node_service_name(),
                m.build_nc_node_service(xand_node_dir),
            );
        }

        // Add trust
        let trust_svc = des.trust.build_trust_service(&convention);
        services.insert(des.trust.get_trust_service_name(), trust_svc);

        // Add trust api service
        let trust_api_svc = des.trust.build_trust_api_service(&convention);
        services.insert(des.trust.get_trust_api_service_name(), trust_api_svc);

        let trust_ncn_dir = &convention.trust_nc_node_dir();
        let trust_ncn_dir_convention = XandNodeDirConventionBuilder::from(trust_ncn_dir);
        let trust_nc_node_service = des.trust.build_nc_node_service(&trust_ncn_dir_convention);
        services.insert(des.trust.get_nc_node_service_name(), trust_nc_node_service);

        // Add limited agent
        let la_ncn_dir_convention =
            XandNodeDirConventionBuilder::from(&convention.limited_agent_nc_node_dir());
        let la_ncn_service = des
            .limited_agent
            .build_nc_node_service(&la_ncn_dir_convention);

        services.insert(des.limited_agent.get_nc_node_service_name(), la_ncn_service);

        // Add "xand" docker networking
        let networks = {
            let mut m = BTreeMap::new();
            m.insert(
                "xand".to_string(),
                Network {
                    driver: Some(value("bridge".into())),
                    driver_opts: Default::default(),
                    external: None,
                    internal: false,
                    enable_ipv6: false,
                    labels: Default::default(),
                    _hidden: (),
                },
            );
            m
        };
        File {
            version: "2.4".to_string(),
            services,
            volumes: Default::default(),
            networks,
            _hidden: (),
        }
    }

    /// Writes out a "entities-metadata.yaml" file for the entity_set
    pub fn summarize_entities(&self, dir: &StructuredDockerComposeRoot) -> crate::Result<()> {
        let entities = &self.docker_xand_entity_set;
        let summary: XandEntitySetSummary = entities.into();

        let filepath = dir.entities_summary_filepath();
        let yaml_str = serde_yaml::to_string(&summary)?;
        file_io::write_file(filepath, yaml_str.as_bytes())?;
        Ok(())
    }

    pub fn write_docker_compose_yaml(
        &self,
        dir: &StructuredDockerComposeRoot,
    ) -> crate::Result<()> {
        let file = self.build_docker_compose_yaml();
        let dc_yaml_filepath = dir.path().join("docker-compose.yaml");
        file.write_to_path(dc_yaml_filepath).unwrap();

        Ok(())
    }

    /// Initializes directories given
    pub fn serialize_entities(&self, dir: &StructuredDockerComposeRoot) -> crate::Result<()> {
        let docker_entities = &self.docker_xand_entity_set;

        // Serialize bank_mock dirs
        let dc_bm = self.docker_xand_entity_set.banks.clone();
        dc_bm.serialize_config_to(dir)?;

        // Init validator dirs
        for (idx, dcv) in docker_entities
            .permissioned_validators
            .iter()
            .chain(docker_entities.extra_validators.iter())
            .enumerate()
        {
            let val_dir = dir.init_validator_dir(idx)?;
            val_dir.write_validator(dcv)?;
        }

        // Init member dirs
        for (idx, dcm) in docker_entities
            .permissioned_members
            .iter()
            .chain(docker_entities.extra_members.iter())
            .enumerate()
        {
            let (nc_node_dir, mem_dir) = dir.init_member_dir(idx)?;
            mem_dir.write_member(dcm)?;
            nc_node_dir.write_member(dcm)?;
        }

        // Init trust dir
        let (nc_node_dir, trust_dir_builder, trust_api_builder) = dir.init_trust_dir()?;
        let dct = &docker_entities.trust;
        nc_node_dir.write_trust(dct)?;
        trust_dir_builder.write_trust(
            dct,
            &self.member_banks(),
            &self.validator_banks(),
            &self.bank_urls(),
        )?;
        trust_api_builder.write(dct)?;

        // Init limited agent dir
        let nc_node_dir = dir.init_limited_agent_dir()?;
        let dcla = &docker_entities.limited_agent;
        nc_node_dir.write_limited_agent(dcla)?;

        Ok(())
    }

    // Return first 3 validators' multiaddress value for use as BOOTNODE_URL_VEC
    fn bootnode_url_vec(xand_entity_set: &XandEntitySet) -> BootnodeUrlVec {
        let urls: Vec<String> = xand_entity_set
            .validators()
            .iter()
            .take(3)
            .enumerate()
            .map(|(idx, v)| {
                format!(
                    "/dns4/{}/tcp/30333/p2p/{}",
                    ServiceNamingConvention::validator_node(idx),
                    v.libp2p_kp().get_address()
                )
            })
            .collect();

        BootnodeUrlVec { urls }
    }

    fn bank_urls(&self) -> DockerComposeBankUrlProvider {
        DockerComposeBankUrlProvider::new()
    }

    fn member_banks(&self) -> HashMap<String, Vec<BankInfo>> {
        self.banks_from_permissioned_participants(NetworkParticipant::Member)
    }

    fn validator_banks(&self) -> HashMap<String, Vec<BankInfo>> {
        self.banks_from_permissioned_participants(NetworkParticipant::Validator)
    }

    fn banks_from_permissioned_participants(
        &self,
        participant: NetworkParticipant,
    ) -> HashMap<String, Vec<BankInfo>> {
        let mut map = HashMap::new();
        match participant {
            NetworkParticipant::Member => {
                self.docker_xand_entity_set
                    .permissioned_members
                    .iter()
                    .chain(self.docker_xand_entity_set.extra_members.iter())
                    .enumerate()
                    .for_each(|(idx, mem)| {
                        let key = format!("member-{}", idx);
                        let bank_infos = mem.member.state().borrow().member_bank_info.clone();
                        map.insert(key, bank_infos);
                    });
            }
            NetworkParticipant::Validator => {
                self.docker_xand_entity_set
                    .permissioned_validators
                    .iter()
                    .chain(self.docker_xand_entity_set.extra_validators.iter())
                    .enumerate()
                    .for_each(|(idx, val)| {
                        let key = format!("validator-{}", idx);
                        let bank_infos = val.validator.bank_info();
                        map.insert(key, bank_infos.clone());
                    });
            }
            _ => (),
        }

        map
    }
}
