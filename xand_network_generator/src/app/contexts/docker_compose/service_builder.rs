use compose_yml::v2::{value, CommandLine, Image, PortMapping, Service, VolumeMount};
use std::{collections::BTreeMap, path::PathBuf};

use crate::{
    app::data::ImageVersion, contracts::network::metadata::bootnode_url_vec::BootnodeUrlVec,
};

#[derive(Default)]
pub struct ServiceBuilder {
    service: Service,
    command_line_args: Vec<String>,
    volume_mappings: Vec<(PathBuf, String)>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use compose_yml::v2::RawOr;

    #[test]
    fn default_xand_node__assert_in_xand_network() {
        // When
        let service = ServiceBuilder::default_xand_node().build();

        // Then
        assert!(service.networks.contains_key("xand"));
    }

    #[test]
    fn default_xand_node__assert_default_exposed_ports() {
        // When
        let service = ServiceBuilder::default_xand_node().build();

        // Then
        let exposed_ports: Vec<usize> = service
            .expose
            .iter()
            .map(ToString::to_string)
            .map(|s| s.parse().unwrap())
            .collect();
        assert_eq!(exposed_ports, [8933, 9944, 30333, 10044]);
    }

    #[test]
    fn default_xand_node__incorporates_default_xand_node_command_line_args() {
        // When
        let builder = ServiceBuilder::default_xand_node();
        let service = builder.build();

        // Then
        let default_args = ServiceBuilder::default_xand_node_command_line_args();
        let args = default_args.into_iter().map(value).collect();
        let expected = Some(CommandLine::Parsed(args));
        assert_eq!(service.command, expected);
    }

    #[test]
    fn default_xand_node__incorporates_default_xand_node_volumes() {
        // When
        let builder = ServiceBuilder::default_xand_node();
        let service = builder.build();

        // Then
        let default_volumes = ServiceBuilder::default_volumes_vec();
        let expected_volumes: Vec<RawOr<VolumeMount>> = default_volumes
            .into_iter()
            .map(|(host, container)| VolumeMount::host(host, container))
            .map(value)
            .collect();
        assert_eq!(service.volumes, expected_volumes);
    }

    #[test]
    fn add_jwt_flag_to_command_line_args__incorporates_expected_command_line_args_into_service() {
        // Given
        let mut builder = ServiceBuilder::default_xand_node();

        // When
        builder.add_jwt_flag_to_command_line_args();
        let service = builder.build();

        // Then
        let expected_arg_1 = value("--jwt-secret-path".into());
        let expected_arg_2 = value("/etc/xand/jwt-secret.txt".into());

        match service.command.unwrap() {
            CommandLine::ShellCode(_) => {
                panic!("Expected CommandLine::Parsed, found CommandLine::ShellCode")
            }
            CommandLine::Parsed(args) => {
                assert!(args.contains(&expected_arg_1));
                assert!(args.contains(&expected_arg_2));
            }
        }
    }

    #[test]

    fn override_command_args_vec__incorporates_expected_command_line_args_into_service() {
        // Given
        let mut builder = ServiceBuilder::default_xand_node();
        let override_args_with = vec!["--some-flag", "some-val"];

        // When
        builder.override_command_args_vec(override_args_with.clone());
        let service = builder.build();

        // Then
        let expected_args = override_args_with
            .into_iter()
            .map(ToString::to_string)
            .map(value)
            .collect();
        let expected_command_line = CommandLine::Parsed(expected_args);
        assert_eq!(service.command, Some(expected_command_line));
    }

    #[test]
    fn add_jwt_secret_txt_volume__volume_is_found_in_service() {
        // Given
        let mut builder = ServiceBuilder::default_xand_node();
        let host_path: PathBuf = "./some/path/to/jwt-secret.txt".into();
        let container_path: String = "/etc/xand/jwt-secret.txt".into();

        // When
        builder.add_jwt_secret_txt_volume(host_path.clone());
        let service = builder.build();

        // Then
        let expected_volume = VolumeMount::host(host_path, container_path);
        let expected_volume = value(expected_volume);
        assert!(
            service.volumes.contains(&expected_volume),
            "Did not find:\n{:#?}\nVec:\n{:#?}",
            &expected_volume,
            &service.volumes
        );
    }

    #[test]
    fn override_host_volume_mapping__volumes_vec_equals_override_arg() {
        // Given
        let mut builder = ServiceBuilder::default_xand_node();
        let host_path: PathBuf = "./some/host/path".into();
        let container_path = "/some/container/path";
        let override_with = vec![(host_path.clone(), container_path)];

        // When
        builder.override_host_volume_mapping(override_with);
        let service = builder.build();

        // Then
        let expected_volume = VolumeMount::host(host_path, container_path);
        let expected_volume = value(expected_volume);
        assert_eq!(service.volumes, vec![expected_volume]);
    }
}

impl ServiceBuilder {
    fn default_xand_node_command_line_args() -> Vec<String> {
        let default_cmd_line_args: Vec<String> = vec![
            "--base-path",
            "/xand/validator",
            "--no-mdns",
            "--unsafe-ws-external",
            "--unsafe-rpc-external",
            "--rpc-cors",
            "all",
            "--prometheus-external",
            "--execution",
            "NativeElseWasm",
        ]
        .into_iter()
        .map(ToString::to_string)
        .collect();
        default_cmd_line_args
    }

    pub fn build(mut self) -> Service {
        // Convert accumulated command line args to types compatible with Service
        let args = self.command_line_args.into_iter().map(value).collect();
        self.service.command = Some(CommandLine::Parsed(args));

        self.service.volumes = self
            .volume_mappings
            .iter()
            .map(|(host, container)| VolumeMount::host(host, container))
            .map(value)
            .collect();

        self.service
    }

    pub fn set_image(&mut self, image_version: &ImageVersion) -> &mut Self {
        let image = Image::new(image_version.to_string()).unwrap();
        self.service.image = Some(value(image));
        self
    }

    pub fn set_entrypoint(&mut self, entrypoint: &str) -> &mut Self {
        let cmd = CommandLine::ShellCode(value(entrypoint.to_string()));
        self.service.entrypoint = Some(cmd);
        self
    }

    pub fn override_command_args_vec(&mut self, args: Vec<&str>) -> &mut Self {
        self.command_line_args = args.into_iter().map(ToString::to_string).collect();
        self
    }

    pub fn set_public_addr<S: AsRef<str>>(&mut self, multi_address: S) -> &mut Self {
        self.command_line_args.push("--public-addr".into());
        self.command_line_args
            .push(multi_address.as_ref().to_string());
        self
    }

    pub fn add_env_file(&mut self, path: PathBuf) -> &mut Self {
        self.service.env_files.push(value(path));
        self
    }

    pub fn set_node_key(&mut self, node_key: String) -> &mut Self {
        self.service
            .environment
            .insert("NODE_KEY".into(), value(node_key));
        self
    }
    pub fn set_consensus(&mut self, is_consensus: bool) -> &mut Self {
        self.service
            .environment
            .insert("CONSENSUS_NODE".into(), value(is_consensus.to_string()));
        self
    }

    pub fn set_bootnode_url_vec(&mut self, bootnode_url_vec: &BootnodeUrlVec) -> &mut Self {
        self.service.environment.insert(
            "BOOTNODE_URL_VEC".into(),
            value(bootnode_url_vec.to_formatted_arg_string()),
        );
        self
    }

    pub fn set_node_name(&mut self, node_name: &str) -> &mut Self {
        self.service
            .environment
            .insert("NODE_NAME".into(), value(node_name.to_string()));
        self
    }

    pub fn override_host_volume_mapping<P: Into<PathBuf>>(
        &mut self,
        volumes: Vec<(P, &str)>,
    ) -> &mut Self {
        self.volume_mappings = volumes
            .into_iter()
            .map(|(p, s)| (p.into(), s.into()))
            .collect();

        self
    }
    pub fn prefix_volumes_with(&mut self, path_prefix: PathBuf) -> &mut Self {
        self.volume_mappings = self
            .volume_mappings
            .clone()
            .into_iter()
            .map(|(host, container)| (path_prefix.join(host), container))
            .collect();
        self
    }

    pub fn set_port_mappings(&mut self, mappings: Vec<(u16, u16)>) -> &mut Self {
        self.service.ports = mappings
            .into_iter()
            .map(|(host, container)| PortMapping::new(host, container))
            .map(value)
            .collect();
        self
    }

    pub fn set_network(&mut self, network: &str) -> &mut Self {
        self.service.networks = {
            let mut networks = BTreeMap::new();
            networks.insert(network.into(), Default::default());
            networks
        };
        self
    }

    pub fn add_jwt_flag_to_command_line_args(&mut self) -> &mut Self {
        self.command_line_args.push("--jwt-secret-path".into());
        self.command_line_args
            .push("/etc/xand/jwt-secret.txt".into());
        self
    }

    pub fn add_jwt_secret_txt_volume(&mut self, jwt_filepath: PathBuf) -> &mut Self {
        self.volume_mappings
            .push((jwt_filepath, "/etc/xand/jwt-secret.txt".into()));
        self
    }

    /// Returns base service for non-consensus and consensus xand node
    pub fn default_xand_node() -> Self {
        let mut service = Service::default();

        service.networks = {
            let mut networks = BTreeMap::new();
            networks.insert("xand".into(), Default::default());
            networks
        };

        service.expose = vec![8933, 9944, 30333, 10044]
            .iter()
            .map(|x| x.to_string())
            .map(value)
            .collect();

        service.environment = {
            let mut map: BTreeMap<String, _> = BTreeMap::new();
            vec![
                ("TELEMETRY_URL", "ws://substrate-telemetry:1024 0".into()),
                ("EMIT_METRICS", "true".into()),
                ("XAND_HUMAN_LOGGING", "true".into()),
            ]
            .into_iter()
            .for_each(|(k, v)| {
                map.insert(k.to_string(), value(v));
            });
            // .collect::<()>();
            map
        };

        service.volumes = Self::default_volumes_vec()
            .into_iter()
            .map(|(host, container)| VolumeMount::host(host, container))
            .map(value)
            .collect();
        Self {
            service,
            command_line_args: Self::default_xand_node_command_line_args(),
            volume_mappings: Self::default_volumes_vec(),
        }
    }

    fn default_volumes_vec() -> Vec<(PathBuf, String)> {
        vec![
            (
                "xand/chainspec/chainSpecModified.json",
                "/xand/chainspec/chainSpecModified.json",
            ),
            ("xand/validator", "/xand/validator/"),
            ("xand/keypairs", "/xand/keypairs"),
            ("xand-api/initial-keys", "/etc/xand-keys"),
            ("xand-api/key-management", "/xand/keys"),
        ]
        .into_iter()
        .map(|(h, c)| (h.into(), c.into()))
        .collect()
    }
}
