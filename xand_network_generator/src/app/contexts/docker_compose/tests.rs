use std::{collections::HashMap, fs};

use super::{
    docker_compose_network::structured_docker_compose_root::StructuredDockerComposeRoot,
    DockerComposeNetwork,
};
use crate::{
    app::{
        contexts::docker_compose::{
            docker_compose_network::structured_docker_compose_root::StructuredDockerComposeConv,
            entities::{
                docker_compose_limited_agent::DockerComposeLimitedAgent,
                docker_compose_member::DockerComposeMember,
                docker_compose_trust::DockerComposeTrust,
                docker_compose_validator::DockerComposeValidator,
            },
        },
        data::ImageSet,
        external_wrappers::chain_spec_generation::ChainSpec,
        network::{GenerateMembersSpec, GenerateValidatorsSpec, XandEntitySet},
        xand_services::dir_structure::XandNodeDirConventionBuilder,
    },
    contracts::data::bank_info::Bank,
};
use std::path::PathBuf;
use strum::IntoEnumIterator;
use tempfile::TempDir;
use walkdir::WalkDir;

#[test]
fn can_serialize_member_api() {
    // Given
    let dc_member = DockerComposeMember::test();
    let service = dc_member.build_member_api_service("./members/0".into());

    // When
    let serialized = serde_yaml::to_string(&service);

    // Then
    serialized.unwrap();
}

#[test]
fn can_serialize_validator() {
    // Given
    let dc_validator = DockerComposeValidator::test();
    let service = dc_validator.build_validator_service("./".into());

    // When
    let serialized = serde_yaml::to_string(&service);

    // Then
    serialized.unwrap();
}

#[test]
fn can_serialize_member_non_consensus_node() {
    // Given
    let dc_member = DockerComposeMember::test();
    let service = dc_member.build_nc_node_service("./members/prefix/".into());

    // When
    let serialized = serde_yaml::to_string(&service);

    // Then
    serialized.unwrap();
}

#[test]
fn can_serialize_trustee() {
    // Given
    let trust = DockerComposeTrust::test();
    let conv = StructuredDockerComposeConv {};
    let service = trust.build_trust_service(&conv);

    // When
    let serialized = serde_yaml::to_string(&service);

    // Then
    serialized.unwrap();
}

#[test]
fn can_serialize_trustee_non_consensus_node() {
    // Given
    let trust = DockerComposeTrust::test();
    let dummy_root_dir: PathBuf = "/some/path".into();
    let dir_convention = XandNodeDirConventionBuilder::from(dummy_root_dir);
    let service = trust.build_nc_node_service(&dir_convention);

    // When
    let serialized = serde_yaml::to_string(&service);

    // Then
    serialized.unwrap();
}

#[test]
fn can_serialize_limited_agent_non_consensus_node() {
    // Given
    let la = DockerComposeLimitedAgent::test();
    let dummy_root_path: PathBuf = "/some/path".into();
    let dir_builder = XandNodeDirConventionBuilder::from(dummy_root_path);
    let service = la.build_nc_node_service(&dir_builder);

    // When
    let serialized = serde_yaml::to_string(&service);

    // Then
    serialized.unwrap();
}

#[test]
fn serialize_entities__serialized_directory_is_deterministic() {
    let num_members = 2;
    let num_validators = 2;
    let key_generation_seed = Some(0);
    let enable_jwt = true;

    for _ in 0..10 {
        // Given
        let member_spec = GenerateMembersSpec::new(num_members, 0);
        let validator_spec = GenerateValidatorsSpec::new(num_validators, 0);
        let entity_set_1 = XandEntitySet::generate(
            member_spec.clone(),
            validator_spec.clone(),
            key_generation_seed,
            Bank::iter().collect(),
            enable_jwt,
        )
        .unwrap();
        let entity_set_2 = XandEntitySet::generate(
            member_spec,
            validator_spec,
            key_generation_seed,
            Bank::iter().collect(),
            enable_jwt,
        )
        .unwrap();

        let dummy_chain_spec = tempfile::NamedTempFile::new().unwrap();

        let network_1 = DockerComposeNetwork::new(
            entity_set_1,
            ImageSet::test(),
            ChainSpec::new(dummy_chain_spec.path().into()),
        );
        let network_2 = DockerComposeNetwork::new(
            entity_set_2,
            ImageSet::test(),
            ChainSpec::new(dummy_chain_spec.path().into()),
        );

        let dir_1 = TempDir::new().unwrap();
        let dir_2 = TempDir::new().unwrap();

        let root_1 = StructuredDockerComposeRoot::build(dir_1.path().into()).unwrap();
        let root_2 = StructuredDockerComposeRoot::build(dir_2.path().into()).unwrap();

        // When
        network_1.serialize_entities(&root_1).unwrap();
        network_2.serialize_entities(&root_2).unwrap();

        // Then
        let diff_result = std::process::Command::new("diff")
            .arg("--recursive")
            .arg("-p")
            .arg(dir_1.path())
            .arg(dir_2.path())
            .output()
            .unwrap();

        let stdout = String::from_utf8_lossy(&diff_result.stdout);
        assert_eq!(
            diff_result.status.code(),
            Some(0),
            "Expected two generations to return same results; found differences:\n{}",
            stdout
        );
    }
}

#[test]
fn serialize_entities__serialized_directory_contains_no_partner_names_when_only_test_banks_enabled()
{
    // Given
    let key_generation_seed = Some(0);
    let entity_set = XandEntitySet::generate(
        GenerateMembersSpec::test(),
        GenerateValidatorsSpec::test(),
        key_generation_seed,
        [Bank::TestBankOne, Bank::TestBankTwo]
            .iter()
            .copied()
            .collect(),
        true,
    )
    .unwrap();

    let dummy_chain_spec = tempfile::NamedTempFile::new().unwrap();

    let network = DockerComposeNetwork::new(
        entity_set,
        ImageSet::test(),
        ChainSpec::new(dummy_chain_spec.path().into()),
    );

    let dir = TempDir::new().unwrap();

    let root = StructuredDockerComposeRoot::build(dir.path().into()).unwrap();

    // When
    network.serialize_entities(&root).unwrap();

    // Then
    let disallowed_terms = ["mcb", "metropolitan", "provident", "bankprov"];
    let mut violating_files = HashMap::new();
    for entry_result in WalkDir::new(&dir) {
        let entry = entry_result.unwrap();

        if !entry.file_type().is_file() {
            continue;
        }

        let path = entry.path();
        let contents = fs::read_to_string(path).unwrap().to_lowercase();

        let violations: Vec<_> = disallowed_terms
            .iter()
            .filter(|&&term| contents.contains(term))
            .collect();

        if !violations.is_empty() {
            violating_files.insert(path.to_string_lossy().to_string(), violations);
        }
    }

    if !violating_files.is_empty() {
        println!(
            "With only test banks enabled, the following files leaked mentions of real partners:"
        );
        for (path, terms) in &violating_files {
            println!("  - {}: {:?}", path, terms)
        }
    }

    assert!(
        violating_files.is_empty(),
        "Found file(s) which leak partner names"
    );
}
