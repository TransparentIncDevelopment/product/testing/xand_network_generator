pub struct ServiceNamingConvention {}

impl ServiceNamingConvention {
    pub fn member(idx: usize) -> String {
        format!("member-api-{}", idx)
    }

    pub fn validator_node(idx: usize) -> String {
        format!("validator-{}", idx)
    }

    pub fn non_consensus_node<S: AsRef<str>>(owner: S, idx: usize) -> String {
        format!("{}-non-consensus-node-{}", owner.as_ref(), idx)
    }

    pub fn trust_non_consensus_node() -> String {
        "trust-non-consensus-node".into()
    }

    pub fn limited_agent_non_consensus_node() -> String {
        "limited-agent-non-consensus-node".into()
    }

    pub fn trustee() -> String {
        "trustee".into()
    }

    pub fn trust_api() -> String {
        "trust-api".into()
    }

    pub fn bank_mocks() -> String {
        "bank-mocks".into()
    }
}
