use crate::contracts::network::auth::{Jwt, JwtSecret};
use jwtcli::jwt::{
    generate_jwt,
    models::{Claim, ClaimValue},
};

pub mod error;

/// Member API and Xand API expect this claim within JWT
pub const ISSUER: &str = "XAND_NETWORK_GENERATOR";

/// Member API and Xand API expect this claim within JWT
pub const SUBJECT: &str = "DEVELOPER";

/// Member API and Xand API expect this claim within JWT
pub const COMPANY: &str = "AWESOMECO";

/// Member API and Xand API expect this claim within JWT.
/// Constant value representing "2099-12-31T00:00:00-00:00", such that JWT will expire
/// at the end of the century for development purposes, and the JWTs generated through re-runs
/// of network generation remain deterministic
pub const EXPIRY_TIMESTAMP: u64 = 4102358400;

pub struct JwtProvider;

impl JwtProvider {
    pub fn try_create(secret: &JwtSecret) -> error::Result<Jwt> {
        let claims = Self::default_claims();
        let secret = jwtcli::jwt::models::JwtSecret::new(secret.0.clone());

        let token_str = generate_jwt(claims, Some(secret), None)?.value();
        Ok(Jwt(token_str))
    }

    fn default_claims() -> Vec<Claim> {
        vec![
            Claim::new("iss".into(), ClaimValue::String(ISSUER.into())),
            Claim::new("sub".into(), ClaimValue::String(SUBJECT.into())),
            Claim::new("company".into(), ClaimValue::String(COMPANY.into())),
            Claim::new("exp".into(), ClaimValue::Integer(EXPIRY_TIMESTAMP)),
        ]
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::contracts::network::auth::JwtSecret;
    use chrono::{DateTime, TimeZone, Utc};

    fn unpack_claims(claims: Vec<Claim>) -> Vec<(String, String)> {
        let claims_unpacked: Vec<(String, String)> = claims
            .iter()
            .map(|c| {
                let val_str = match c.value() {
                    ClaimValue::Integer(i) => i.to_string(),
                    ClaimValue::String(s) => s,
                };
                (c.key(), val_str)
            })
            .collect();
        claims_unpacked
    }

    #[test]
    fn try_create__returns_non_empty_token() {
        // Given
        let secret = JwtSecret("myarbitrarysecret".into());

        // When
        let token = JwtProvider::try_create(&secret).unwrap();

        // Then
        assert!(!token.value().is_empty());
    }

    #[test]
    fn default_claims__includes_iss_claim() {
        // When
        let claims = JwtProvider::default_claims();

        // Then
        // Unpack Claims to get (key,value)
        let claims_unpacked: Vec<(String, String)> = unpack_claims(claims);
        assert!(claims_unpacked.contains(&("iss".into(), ISSUER.into())));
    }

    #[test]
    fn default_claims__includes_sub_claim() {
        // When
        let claims = JwtProvider::default_claims();

        // Then
        let claims_unpacked: Vec<(String, String)> = unpack_claims(claims);
        assert!(claims_unpacked.contains(&("sub".into(), SUBJECT.into())));
    }

    #[test]
    fn default_claims__includes_company_claim() {
        // When
        let claims = JwtProvider::default_claims();

        // Then
        let claims_unpacked: Vec<(String, String)> = unpack_claims(claims);
        assert!(claims_unpacked.contains(&("company".into(), COMPANY.into())))
    }

    #[test]
    fn default_claims__includes_exp_claim() {
        // When
        let claims = JwtProvider::default_claims();

        // Then
        let claims_unpacked: Vec<(String, String)> = unpack_claims(claims);
        assert!(claims_unpacked.contains(&("exp".into(), EXPIRY_TIMESTAMP.to_string())))
    }

    #[test]
    fn expiry_time_maps_to_expected_date() {
        // When
        let datetime: DateTime<Utc> = Utc.timestamp_opt(EXPIRY_TIMESTAMP as i64, 0).unwrap();

        // Then
        let expected: DateTime<Utc> = DateTime::parse_from_rfc3339("2099-12-31T00:00:00-00:00")
            .unwrap()
            .into();
        assert_eq!(datetime, expected);
    }
}
