use crate::app::adapters::auth_provider::jwt_provider::error::JwtProviderError;
use thiserror::Error;

#[derive(Clone, Debug, Error, PartialEq, Eq)]
pub enum AuthProviderError {
    #[error("{0}")]
    JwtProvider(#[from] JwtProviderError),
}
