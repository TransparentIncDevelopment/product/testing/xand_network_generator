use crate::contracts::data::bank_info::{Bank, BankInfo};

pub trait BankProvider {
    fn get_trust(&self, bank: Bank) -> BankInfo;
}
