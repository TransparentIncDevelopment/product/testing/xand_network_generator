pub mod member {
    use crate::contracts::data::MemberAddress;
    use std::net::Ipv4Addr;

    pub trait MemberPublicInfo {
        fn get_address(&self) -> MemberAddress;
        fn ips(&self) -> &Vec<Ipv4Addr>;
    }
}

pub mod trust {
    use crate::contracts::data::TrustAddress;
    use std::net::Ipv4Addr;

    pub trait TrustPublicInfo {
        fn get_address(&self) -> TrustAddress;
        fn get_ip_addresses(&self) -> Vec<Ipv4Addr>;
    }
}
