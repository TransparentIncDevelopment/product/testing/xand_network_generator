use crate::app::{external_wrappers::DomainXandKeyPair, key_provider::KryptRng};

#[derive(Clone, Debug)]
pub struct LimitedAgentKeySet {
    pub key: DomainXandKeyPair,
}

impl LimitedAgentKeySet {
    pub(crate) fn generate_with<R: KryptRng>(rng: &mut R) -> Self {
        Self {
            key: DomainXandKeyPair::generate_limited_agent_key(rng),
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;

    use super::LimitedAgentKeySet;

    #[test]
    fn generate_with__generates_unique_keys_for_different_random_inputs() {
        let mut rng1 = rand::rngs::StdRng::seed_from_u64(0);
        let mut rng2 = rand::rngs::StdRng::seed_from_u64(1);

        let keys1 = LimitedAgentKeySet::generate_with(&mut rng1);
        let keys2 = LimitedAgentKeySet::generate_with(&mut rng2);

        assert_ne!(keys1.key.get_address(), keys2.key.get_address());
    }

    #[test]
    fn generate_with__generates_reproducible_keys_for_same_seed() {
        let mut rng1 = rand::rngs::StdRng::seed_from_u64(0);
        let mut rng2 = rand::rngs::StdRng::seed_from_u64(0);

        let keys1 = LimitedAgentKeySet::generate_with(&mut rng1);
        let keys2 = LimitedAgentKeySet::generate_with(&mut rng2);

        assert_eq!(keys1, keys2);
    }

    impl PartialEq for LimitedAgentKeySet {
        fn eq(&self, other: &Self) -> bool {
            self.key.get_address() == other.key.get_address()
        }
    }
}
