use crate::app::{external_wrappers::DomainXandKeyPair, key_provider::KryptRng};
use tpfs_krypt::KeyPairInstance;

#[derive(Clone, Debug)]
pub struct ValidatorKeySet {
    pub authority_key: DomainXandKeyPair,
    pub libp2p_key: DomainXandKeyPair,
    pub produce_blocks_kp: DomainXandKeyPair,
    pub finalize_blocks_kp: DomainXandKeyPair,
    pub encryption_key: KeyPairInstance,
}

impl ValidatorKeySet {
    pub(crate) fn generate_with<R: KryptRng>(rng: &mut R) -> Self {
        Self {
            authority_key: DomainXandKeyPair::generate_authority_key(rng),
            libp2p_key: DomainXandKeyPair::generate_libp2p(rng),
            produce_blocks_kp: DomainXandKeyPair::generate_produce_blocks_kp(rng),
            finalize_blocks_kp: DomainXandKeyPair::generate_finalize_blocks_kp(rng),
            encryption_key: KeyPairInstance::generate_with(
                tpfs_krypt::KeyType::SharedEncryptionX25519,
                rng,
            )
            .expect("Key pair generation should not fail"),
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use tpfs_krypt::KeyPair;

    use super::ValidatorKeySet;

    #[test]
    fn generate_with__generates_unique_keys_for_different_random_inputs() {
        let mut rng1 = rand::rngs::StdRng::seed_from_u64(0);
        let mut rng2 = rand::rngs::StdRng::seed_from_u64(1);

        let keys1 = ValidatorKeySet::generate_with(&mut rng1);
        let keys2 = ValidatorKeySet::generate_with(&mut rng2);

        assert_ne!(
            keys1.authority_key.get_address(),
            keys2.authority_key.get_address()
        );
        assert_ne!(
            keys1.libp2p_key.get_address(),
            keys2.libp2p_key.get_address()
        );
        assert_ne!(
            keys1.produce_blocks_kp.get_address(),
            keys2.produce_blocks_kp.get_address()
        );
        assert_ne!(
            keys1.finalize_blocks_kp.get_address(),
            keys2.finalize_blocks_kp.get_address()
        );
    }

    #[test]
    fn generate_with__generates_reproducible_keys_for_same_seed() {
        let mut rng1 = rand::rngs::StdRng::seed_from_u64(0);
        let mut rng2 = rand::rngs::StdRng::seed_from_u64(0);

        let keys1 = ValidatorKeySet::generate_with(&mut rng1);
        let keys2 = ValidatorKeySet::generate_with(&mut rng2);

        assert_eq!(keys1, keys2);
    }

    impl PartialEq for ValidatorKeySet {
        fn eq(&self, other: &Self) -> bool {
            self.libp2p_key.get_address() == other.libp2p_key.get_address()
                && self.authority_key.get_address() == other.authority_key.get_address()
                && self.finalize_blocks_kp.get_address() == other.finalize_blocks_kp.get_address()
                && self.produce_blocks_kp.get_address() == other.produce_blocks_kp.get_address()
                && self.encryption_key.identifier().value == other.encryption_key.identifier().value
        }
    }
}
