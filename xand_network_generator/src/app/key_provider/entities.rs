mod limited_agent;
mod member;
mod trust;
mod validator;

pub use limited_agent::LimitedAgentKeySet;
pub use member::MemberKeySet;
pub use trust::TrustKeySet;
pub use validator::ValidatorKeySet;

#[cfg(test)]
mod tests {
    use rand::{prelude::StdRng, SeedableRng};

    use super::{LimitedAgentKeySet, MemberKeySet, TrustKeySet, ValidatorKeySet};

    const TRUST_KEY_INDEX: u64 = 0;
    const VALIDATOR_KEY_INDEX: u64 = 1;
    const MEMBER_KEY_INDEX: u64 = 2;
    const LIMITED_AGENT_KEY_INDEX: u64 = 3;

    /// Returns a seeded RNG derived from the given index.
    fn dummy_rng(index: u64) -> StdRng {
        StdRng::seed_from_u64(index)
    }

    impl TrustKeySet {
        pub fn test() -> Self {
            Self::generate_with(&mut dummy_rng(TRUST_KEY_INDEX))
        }
    }

    impl ValidatorKeySet {
        pub fn test() -> Self {
            Self::generate_with(&mut dummy_rng(VALIDATOR_KEY_INDEX))
        }
    }

    impl MemberKeySet {
        pub fn test() -> Self {
            Self::generate_with(&mut dummy_rng(MEMBER_KEY_INDEX))
        }
    }

    impl LimitedAgentKeySet {
        pub fn test() -> Self {
            Self::generate_with(&mut dummy_rng(LIMITED_AGENT_KEY_INDEX))
        }
    }
}
