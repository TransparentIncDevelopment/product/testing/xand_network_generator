#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct ImageSet {
    pub member_api: ImageVersion,
    pub validator: ImageVersion,
    pub trustee: ImageVersion,
    pub bank_mocks: ImageVersion,
    pub trust_api: ImageVersion,
}

/// Represents a docker image with version. E.g. gcr.io/xand-dev/member-api:1.9.0
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ImageVersion(pub String);

impl ToString for ImageVersion {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

#[cfg(test)]
mod tests {
    use insta::assert_display_snapshot;

    use super::*;

    const BANK_MOCKS_VERSION: &str = "3.0.0";
    const MEMBER_API_VERSION: &str = "1.12.0";
    const VALIDATOR_VERSION: &str = "1.0.23";
    const TRUSTEE_VERSION: &str = "1.1.1";
    const TRUST_API_VERSION: &str = "1.0.0";

    impl ImageSet {
        pub fn test() -> Self {
            Self {
                member_api: ImageVersion(format!(
                    "gcr.io/xand-dev/member-api:{}",
                    MEMBER_API_VERSION
                )),
                validator: ImageVersion(format!("gcr.io/xand-dev/validator:{}", VALIDATOR_VERSION)),
                trustee: ImageVersion(format!("gcr.io/xand-dev/trust:{}", TRUSTEE_VERSION)),
                bank_mocks: ImageVersion(format!(
                    "gcr.io/xand-dev/xand-bank-mocks:{}",
                    BANK_MOCKS_VERSION
                )),
                trust_api: ImageVersion(format!("gcr.io/xand-dev/trust-api:{}", TRUST_API_VERSION)),
            }
        }
    }

    #[test]
    fn deserialize__fails_with_extra_image_field() {
        // Given
        let config_str = r#"
        member-api: "gcr.io/xand-dev/someimage:0.1.0"
        validator: "gcr.io/xand-dev/someimage:0.1.0"
        trustee: "gcr.io/xand-dev/someimage:0.1.0"
        bank-mocks: "gcr.io/xand-dev/someimage:0.1.0"
        this_does_not_exist: foo
        "#;

        // When
        let error = serde_yaml::from_str::<ImageSet>(config_str).unwrap_err();

        // Then
        assert_display_snapshot!(error, @"unknown field `this_does_not_exist`, expected one of `member-api`, `validator`, `trustee`, `bank-mocks`, `trust-api` at line 6 column 9");
    }
}
