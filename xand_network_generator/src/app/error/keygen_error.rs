use crate::app::error::Error;
use tpfs_krypt::errors::KeyManagementError;

impl From<KeyManagementError> for Error {
    fn from(e: KeyManagementError) -> Self {
        Error::Keygen(e.to_string())
    }
}
