use crate::app::error::file_io_error::FileIoError;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum LoadConfigurationError {
    #[error("File not found {:?}", 0)]
    FileNotFound(PathBuf),
    #[error(transparent)]
    IoError(#[from] FileIoError),
    #[error(transparent)]
    DeserializeError(#[from] serde_yaml::Error),
}
