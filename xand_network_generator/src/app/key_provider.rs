mod entities;

pub use entities::{LimitedAgentKeySet, MemberKeySet, TrustKeySet, ValidatorKeySet};
use rand::{prelude::StdRng, RngCore, SeedableRng};

// Alias for trait bound "rand::RngCore + rand::CryptoRng", used by tpfs-krypt
pub trait KryptRng: rand::RngCore + rand::CryptoRng {}
impl<T: rand::RngCore + rand::CryptoRng> KryptRng for T {}

pub struct NetworkKeys {
    pub limited_agent: LimitedAgentKeySet,
    pub trust: TrustKeySet,
    pub members: Vec<MemberKeySet>,
    pub validators: Vec<ValidatorKeySet>,
}

const TRUST_KEY_INDEX: u64 = 0;
const VALIDATOR_KEY_INDEX: u64 = 1;
const MEMBER_KEY_INDEX: u64 = 2;
const LIMITED_AGENT_KEY_INDEX: u64 = 3;

/// Optionally-seeded pseudorandom key provider which uses NON-CRYPTOGRAPHICALLY-SECURE seed bit
/// widths.
pub struct KeyProvider {
    seed: Option<u64>,
}
impl KeyProvider {
    pub fn seeded(seed: u64) -> Self {
        KeyProvider { seed: Some(seed) }
    }

    pub fn unseeded() -> Self {
        KeyProvider { seed: None }
    }

    fn create_root_rng(self) -> StdRng {
        if let Some(seed) = self.seed {
            StdRng::seed_from_u64(seed)
        } else {
            let mut seed = [0u8; 32];
            rand::thread_rng().fill_bytes(&mut seed);
            StdRng::from_seed(seed)
        }
    }

    /// Given a seed, returns a seeded PRNG with state derived from both the root seed and the
    /// index. The resultant PRNGs will be reproducibly seeded according to their index.
    ///
    /// Note that this function is NOT cryptographically secure, as there are only 64 bits of
    /// entropy used to initialize each PRNG.
    fn create_seeded_rng(root_seed: u64, derived_index: u64) -> impl KryptRng {
        let derived_seed = root_seed + derived_index;
        StdRng::seed_from_u64(derived_seed)
    }

    pub fn generate(self, num_members: usize, num_validators: usize) -> NetworkKeys {
        let mut root_prng = self.create_root_rng();
        let root_seed = root_prng.next_u64();

        let mut limited_agent_rng = Self::create_seeded_rng(root_seed, LIMITED_AGENT_KEY_INDEX);
        let mut trust_rng = Self::create_seeded_rng(root_seed, TRUST_KEY_INDEX);
        let mut members_rng = Self::create_seeded_rng(root_seed, MEMBER_KEY_INDEX);
        let mut validators_rng = Self::create_seeded_rng(root_seed, VALIDATOR_KEY_INDEX);

        NetworkKeys {
            limited_agent: LimitedAgentKeySet::generate_with(&mut limited_agent_rng),
            trust: TrustKeySet::generate_with(&mut trust_rng),
            members: (0..num_members)
                .map(|_| MemberKeySet::generate_with(&mut members_rng))
                .collect(),
            validators: (0..num_validators)
                .map(|_| ValidatorKeySet::generate_with(&mut validators_rng))
                .collect(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{KeyProvider, LimitedAgentKeySet, MemberKeySet, TrustKeySet, ValidatorKeySet};

    use rand::RngCore;
    use tpfs_krypt::KeyPair;

    use std::{collections::HashSet, hash::Hash};

    fn extract_raw_member_key_addresses(keys: &MemberKeySet) -> impl IntoIterator<Item = String> {
        vec![
            keys.key.get_address(),
            keys.encryption_key.identifier().value,
        ]
    }

    fn extract_raw_validator_key_addresses(
        keys: &ValidatorKeySet,
    ) -> impl IntoIterator<Item = String> {
        vec![
            keys.authority_key.get_address(),
            keys.libp2p_key.get_address(),
            keys.produce_blocks_kp.get_address(),
            keys.finalize_blocks_kp.get_address(),
        ]
    }

    fn extract_raw_trust_key_addresses(keys: &TrustKeySet) -> impl IntoIterator<Item = String> {
        vec![
            keys.key.get_address(),
            keys.encryption_key.identifier().value,
        ]
    }

    fn extract_raw_limited_agent_key_addresses(
        keys: &LimitedAgentKeySet,
    ) -> impl IntoIterator<Item = String> {
        vec![keys.key.get_address()]
    }

    fn all_unique<T: Hash + Eq, I: IntoIterator<Item = T>>(iter: I) -> bool {
        let mut set = HashSet::new();
        iter.into_iter().all(move |v| set.insert(v))
    }

    #[test]
    fn generate__returns_requested_number_of_members() {
        let keys = KeyProvider::seeded(0).generate(2, 0);
        assert_eq!(keys.members.len(), 2);
    }

    #[test]
    fn generate__returns_requested_number_of_validators() {
        let keys = KeyProvider::seeded(0).generate(0, 2);
        assert_eq!(keys.validators.len(), 2);
    }

    #[test]
    fn unseeded_generate__produces_different_keys_per_run() {
        // When
        let keys_1 = KeyProvider::unseeded().generate(2, 2);
        let keys_2 = KeyProvider::unseeded().generate(2, 2);

        // Then
        assert_ne!(keys_1.limited_agent, keys_2.limited_agent);
        assert_ne!(keys_1.trust, keys_2.trust);
        assert_ne!(keys_1.members[0], keys_2.members[0]);
        assert_ne!(keys_1.members[1], keys_2.members[1]);
        assert_ne!(keys_1.validators[0], keys_2.validators[0]);
        assert_ne!(keys_1.validators[1], keys_2.validators[1]);
    }

    #[test]
    fn seeded_generate__returns_unique_keys_per_role() {
        // Given
        let provider = KeyProvider::seeded(0);

        // When
        let keys = provider.generate(5, 5);

        // Then
        let all_raw_keys: Vec<String> = std::iter::empty()
            .chain(
                keys.members
                    .iter()
                    .flat_map(extract_raw_member_key_addresses),
            )
            .chain(
                keys.validators
                    .iter()
                    .flat_map(extract_raw_validator_key_addresses),
            )
            .chain(extract_raw_limited_agent_key_addresses(&keys.limited_agent))
            .chain(extract_raw_trust_key_addresses(&keys.trust))
            .collect();

        assert!(
            all_unique(&all_raw_keys),
            "expected all keys to be unique, got: {:?}",
            all_raw_keys
        )
    }

    #[test]
    fn seeded_generate__generates_reproducible_keys_for_same_seed() {
        // Given
        let seed = 0;

        // When
        let keys_1 = KeyProvider::seeded(seed).generate(2, 2);
        let keys_2 = KeyProvider::seeded(seed).generate(2, 2);

        // Then
        assert_eq!(keys_1.limited_agent, keys_2.limited_agent);
        assert_eq!(keys_1.trust, keys_2.trust);

        for (i, (member_1, member_2)) in keys_1.members.into_iter().zip(keys_2.members).enumerate()
        {
            assert_eq!(member_1, member_2, "mismatched member keys at index {}", i);
        }

        for (i, (validator_1, validator_2)) in keys_1
            .validators
            .into_iter()
            .zip(keys_2.validators)
            .enumerate()
        {
            assert_eq!(
                validator_1, validator_2,
                "mismatched validator keys at index {}",
                i
            );
        }
    }

    #[test]
    fn seeded_generate__generates_reproducible_keys_when_adding_new_nodes() {
        // Given
        let seed = 0;

        // When
        let keys_1 = KeyProvider::seeded(seed).generate(1, 1);
        let keys_2 = KeyProvider::seeded(seed).generate(2, 2);

        // Then
        assert_eq!(keys_1.members.len(), 1);
        assert_eq!(keys_2.members.len(), 2);

        assert_eq!(keys_1.limited_agent, keys_2.limited_agent);
        assert_eq!(keys_1.trust, keys_2.trust);
        assert_eq!(keys_1.members[0], keys_2.members[0]);
        assert_eq!(keys_1.validators[0], keys_2.validators[0]);

        assert_ne!(
            keys_2.validators[1].authority_key.get_address(),
            keys_2.members[1].key.get_address()
        );
    }

    #[test]
    fn create_seeded_rng__returns_distinct_prngs() {
        // Given
        let root_seed = 0;

        // When
        let first_u64_of_first_five: Vec<_> = (0..5)
            .map(|i| {
                let mut rng = KeyProvider::create_seeded_rng(root_seed, i);
                rng.next_u64()
            })
            .collect();

        // Then
        assert!(
            all_unique(&first_u64_of_first_five),
            "expected each derived PRNG to return a unique first value, got values: {:?}",
            first_u64_of_first_five
        )
    }

    #[test]
    fn create_seeded_rng__generates_reproducible_state_for_same_seed() {
        // Given
        let root_seed = 0;

        // When
        let first_u64_of_first_five_1: Vec<_> = (0..5)
            .map(|i| {
                let mut rng = KeyProvider::create_seeded_rng(root_seed, i);
                rng.next_u64()
            })
            .collect();

        let first_u64_of_first_five_2: Vec<_> = (0..5)
            .map(|i| {
                let mut rng = KeyProvider::create_seeded_rng(root_seed, i);
                rng.next_u64()
            })
            .collect();

        // Then
        assert_eq!(first_u64_of_first_five_1, first_u64_of_first_five_2);
    }
}
