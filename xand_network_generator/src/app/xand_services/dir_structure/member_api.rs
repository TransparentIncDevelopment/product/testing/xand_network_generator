use crate::app::{
    config::external::member_api::build_member_api_config_from,
    contexts::docker_compose::{
        entities::docker_compose_member::DockerComposeMember,
        services::non_consensus_node::XAND_API_CONTAINER_PORT,
    },
    file_io,
};
use directory_convention::MemberApiDirConvention;
use std::path::PathBuf;

pub mod directory_convention;
#[cfg(test)]
mod tests;

pub struct MemberApiDirectoryBuilder {
    convention_builder: MemberApiDirConvention,
}

impl MemberApiDirectoryBuilder {
    pub fn new(path: PathBuf) -> Self {
        let convention_builder = MemberApiDirConvention(path);
        Self { convention_builder }
    }

    pub fn write_member(self, dcm: &DockerComposeMember) -> crate::Result<MemberApiDirConvention> {
        let config_dir = self.convention_builder.config_dir();
        file_io::create_dir(&config_dir, true)?;

        let secrets_dir = self.convention_builder.secrets_dir();
        file_io::create_dir(&secrets_dir, true)?;

        // Write secrets file to location by convention
        let secrets_filepath = self.convention_builder.secrets_file();
        let secrets = dcm.member_api_secrets_store();
        let secrets_store_str = serde_yaml::to_string(&secrets)?;
        file_io::write_file(secrets_filepath, secrets_store_str.as_bytes())?;

        // Write config file
        let config_file = self.convention_builder.config_file();
        let xand_api_url = format!(
            "http://{}:{}",
            dcm.get_nc_node_service_name(),
            XAND_API_CONTAINER_PORT
        );
        let member_api_config = build_member_api_config_from(&dcm.member, xand_api_url);

        let config_yaml_str = serde_yaml::to_string(&member_api_config)?;
        file_io::write_file(config_file, config_yaml_str)?;

        Ok(self.convention_builder)
    }
}
