use std::path::{Path, PathBuf};

pub struct MemberApiDirConvention(pub(in crate::app) PathBuf);

impl MemberApiDirConvention {
    pub fn config_dir(&self) -> PathBuf {
        self.0.join("config")
    }
    pub fn secrets_dir(&self) -> PathBuf {
        self.config_dir().join("secrets")
    }

    pub fn config_file(&self) -> PathBuf {
        self.config_dir().join("config.yaml")
    }
    pub fn secrets_file(&self) -> PathBuf {
        self.secrets_dir().join("secrets.yaml")
    }
}

impl<P: AsRef<Path>> From<P> for MemberApiDirConvention {
    fn from(path: P) -> Self {
        Self(PathBuf::from(path.as_ref()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_dir__located_in_root_dir() {
        // Given
        let root: PathBuf = "/root/of/member-api".into();
        let c = MemberApiDirConvention(root);

        // When
        let res = c.config_dir();

        // Then
        let expected: PathBuf = "/root/of/member-api/config".into();
        assert_eq!(res, expected);
    }

    #[test]
    fn secrets_dir__located_in_config_dir() {
        // Given
        let root: PathBuf = "/root/of/member-api".into();
        let c = MemberApiDirConvention(root);

        // When
        let res = c.secrets_dir();

        // Then
        let expected: PathBuf = "/root/of/member-api/config/secrets".into();
        assert_eq!(res, expected);
    }

    #[test]
    fn config_file__named_config_dot_yaml_in_config_dir() {
        // Given
        let root: PathBuf = "/root/of/member-api".into();
        let c = MemberApiDirConvention(root);

        // When
        let res = c.config_file();

        // Then
        let expected: PathBuf = "/root/of/member-api/config/config.yaml".into();
        assert_eq!(res, expected);
    }

    #[test]
    fn secrets_file__named_secrets_dot_yaml_in_secrets_dir() {
        // Given
        let root: PathBuf = "/root/of/member-api".into();
        let c = MemberApiDirConvention(root);

        // When
        let res = c.secrets_file();

        // Then
        let expected: PathBuf = "/root/of/member-api/config/secrets/secrets.yaml".into();
        assert_eq!(res, expected);
    }
}
