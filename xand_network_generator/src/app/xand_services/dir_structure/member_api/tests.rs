use super::*;
use tempfile::tempdir;

#[test]
fn write_member__directory_structure_matches_expected() {
    // Given
    let dcm = DockerComposeMember::test();
    let tempdir = tempdir().unwrap();
    let builder = MemberApiDirectoryBuilder::new(tempdir.path().to_path_buf());

    // When
    builder.write_member(&dcm).unwrap();

    // Then
    let config_dir = tempdir.path().join("config");
    let secrets_dir = config_dir.join("secrets");

    assert!(config_dir.is_dir());
    assert!(secrets_dir.is_dir());
    assert!(config_dir.join("config.yaml").is_file());
    assert!(secrets_dir.join("secrets.yaml").is_file());
}
