use std::path::PathBuf;

pub struct DirConvention(pub(super) PathBuf);

impl DirConvention {
    pub fn config_dir(&self) -> PathBuf {
        self.0.join("config")
    }
    pub fn config_file(&self) -> PathBuf {
        self.config_dir().join("config.yaml")
    }
}
