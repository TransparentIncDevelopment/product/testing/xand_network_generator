use super::*;
use crate::app::file_io::open_file;
use tempfile::tempdir;
use trust_api_config::TrustApiConfig;

#[test]
fn write_trust__directory_structure_matches_expected() {
    // Given
    let dcm = DockerComposeTrust::test();
    let tempdir = tempdir().unwrap();
    let builder = TrustApiDirectoryBuilder::from(tempdir.path().to_path_buf());

    // When
    builder.write(&dcm).unwrap();

    // Then
    let config_dir = tempdir.path().join("config");
    assert!(config_dir.is_dir());
    assert!(config_dir.join("config.yaml").is_file());
}

#[test]
fn write_trust__writes_parseable_config_yaml_file() {
    // Given
    let dcm = DockerComposeTrust::test();
    let tempdir = tempdir().unwrap();
    let builder = TrustApiDirectoryBuilder::from(tempdir.path().to_path_buf());

    // When
    let written_dir_convention = builder.write(&dcm).unwrap();

    // Then
    let config_file = open_file(written_dir_convention.config_file()).unwrap();
    serde_yaml::from_reader::<_, TrustApiConfig>(config_file).unwrap();
}
