use crate::app::{
    contexts::docker_compose::entities::{
        docker_compose_limited_agent::DockerComposeLimitedAgent,
        docker_compose_member::DockerComposeMember, docker_compose_trust::DockerComposeTrust,
        docker_compose_validator::DockerComposeValidator,
    },
    file_io,
    xand_services::dir_structure::xand_node::xand_node_dir_builder::convention_builder::XandNodeDirConventionBuilder,
};
use std::path::PathBuf;

pub mod convention_builder;
#[cfg(test)]
mod tests;

pub struct XandNodeDirBuilder {
    convention: XandNodeDirConventionBuilder,
}

impl XandNodeDirBuilder {
    pub fn new(path: PathBuf) -> Self {
        let convention = XandNodeDirConventionBuilder(path);

        XandNodeDirBuilder { convention }
    }

    pub(crate) fn write_limited_agent(
        &self,
        dc_limited_agent: &DockerComposeLimitedAgent,
    ) -> crate::Result<()> {
        self.convention.init_dirs()?;

        let chain_spec_dir = self.convention.get_chain_spec_dir();
        dc_limited_agent.chain_spec.copy_to_dir(chain_spec_dir)?;

        // Write xand-api key to xand-api/initial-keys
        let xand_api_keys_dir = self.convention.get_xand_api_keys_path();
        dc_limited_agent
            .limited_agent
            .kp()
            .write_to(&xand_api_keys_dir)?;

        dc_limited_agent
            .limited_agent
            .xand_api_auth()
            .as_ref()
            .map_or(Ok(()), |auth| {
                let path = self.convention.get_xand_api_jwt_secret_path();
                file_io::write_file(path, &auth.secret().0)
            })?;

        Ok(())
    }

    pub(crate) fn write_member(&self, dc_member: &DockerComposeMember) -> crate::Result<()> {
        self.convention.init_dirs()?;

        let chain_spec_dir = self.convention.get_chain_spec_dir();
        dc_member.chain_spec.copy_to_dir(chain_spec_dir)?;

        // Write xand-api key to target location
        let xand_api_keys_path = self.convention.get_xand_api_keys_path();
        dc_member.member.kp().write_to(&xand_api_keys_path)?;
        dc_member
            .member
            .encryption_key_pair()
            .write_to_directory(&xand_api_keys_path)
            .map_err(|e| crate::Error::Keygen(e.to_string()))?;

        dc_member
            .member
            .xand_api_auth()
            .as_ref()
            .map_or(Ok(()), |auth| {
                let path = self.convention.get_xand_api_jwt_secret_path();
                file_io::write_file(path, &auth.secret().0)
            })?;

        Ok(())
    }

    pub(crate) fn write_trust(&self, dc_trust: &DockerComposeTrust) -> crate::Result<()> {
        self.convention.init_dirs()?;

        let chain_spec_dir = self.convention.get_chain_spec_dir();
        dc_trust.chain_spec.copy_to_dir(chain_spec_dir)?;

        // Write xand-api key to xand-api/initial-keys
        let xand_api_keys_path = self.convention.get_xand_api_keys_path();
        dc_trust.trust.kp().write_to(&xand_api_keys_path)?;
        dc_trust
            .trust
            .encryption_key_pair()
            .write_to_directory(&xand_api_keys_path)
            .map_err(|e| crate::Error::Keygen(e.to_string()))?;

        dc_trust
            .trust
            .xand_api_auth()
            .as_ref()
            .map_or(Ok(()), |auth| {
                let path = self.convention.get_xand_api_jwt_secret_path();
                file_io::write_file(path, &auth.secret().0)
            })?;

        Ok(())
    }

    pub(crate) fn write_validator(&self, dc_val: &DockerComposeValidator) -> crate::Result<()> {
        self.convention.init_dirs()?;

        let val_state = dc_val.validator.state();

        // Write chain spec to expected dir
        dc_val
            .chain_spec
            .copy_to_dir(self.convention.get_chain_spec_dir())?;

        // Write validator grandpa/session keys to ./xand/keystore
        let consensus_keys_path = self.convention.get_consensus_keys_path();
        val_state
            .borrow()
            .validator_block_production_kp()
            .write_to(&consensus_keys_path)?;

        val_state
            .borrow()
            .validator_block_finalization_kp()
            .write_to(&consensus_keys_path)?;

        // Write validator libp2p keys to ./xand/validator-lp2p-key.env
        let libp2p_key_path = self.convention.get_libp2p_key_dir();
        val_state
            .borrow()
            .validator_libp2p_kp()
            .write_to(&libp2p_key_path)?;

        // Write xand-api key to xand-api/initial-keys
        let xand_api_keys_path = self.convention.get_xand_api_keys_path();

        val_state
            .borrow()
            .validator_encryption_kp()
            .write_to_directory(&xand_api_keys_path)?;

        val_state
            .borrow()
            .validator_authority_kp()
            .write_to(&xand_api_keys_path)?;

        val_state
            .borrow()
            .xand_api_auth()
            .as_ref()
            .map_or(Ok(()), |auth| {
                let path = self.convention.get_xand_api_jwt_secret_path();
                file_io::write_file(path, &auth.secret().0)
            })?;

        Ok(())
    }
}
