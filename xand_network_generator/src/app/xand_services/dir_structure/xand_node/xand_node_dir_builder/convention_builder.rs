use fs_extra::dir::create_all;
use std::path::{PathBuf, Path};
use crate::app::xand_services::dir_structure::xand_node::xand_node_dir_builder::convention_builder::convention::XandNodeDirConvention;

mod convention;

pub struct XandNodeDirConventionBuilder(pub PathBuf);

impl XandNodeDirConventionBuilder {
    pub fn init_dirs(&self) -> crate::Result<()> {
        let path = &self.0;
        let c = XandNodeDirConvention {};

        // Create substrate specific dir
        create_all(&path.join(c.substrate_dir()), true).unwrap();
        create_all(&path.join(c.chain_spec_dir()), true).unwrap();
        create_all(&path.join(c.keypairs_dir()), true).unwrap();

        // Create xand-api specific dirs
        create_all(&path.join(c.xand_api_dir()), true).unwrap();
        create_all(&path.join(c.xand_api_keys_dir()), true).unwrap();
        create_all(&path.join(c.key_management_dir()), true).unwrap();

        Ok(())
    }

    pub fn root_path(&self) -> &PathBuf {
        &self.0
    }
    pub fn get_consensus_keys_path(&self) -> PathBuf {
        let c = XandNodeDirConvention {};
        self.0.join(c.keypairs_dir())
    }

    pub fn get_libp2p_key_dir(&self) -> PathBuf {
        let c = XandNodeDirConvention {};
        self.0.join(c.substrate_dir())
    }

    pub fn get_libp2p_key_filepath(&self) -> PathBuf {
        let c = XandNodeDirConvention {};
        self.get_libp2p_key_dir().join(c.libp2p_filename())
    }

    pub fn get_xand_api_keys_path(&self) -> PathBuf {
        let c = XandNodeDirConvention {};
        self.0.join(c.xand_api_keys_dir())
    }

    pub fn get_xand_api_jwt_secret_path(&self) -> PathBuf {
        let c = XandNodeDirConvention {};
        self.0.join(c.jwt_secret_filepath())
    }

    pub fn get_chain_spec_dir(&self) -> PathBuf {
        let c = XandNodeDirConvention {};
        self.0.join(c.chain_spec_dir())
    }
}

impl<P: AsRef<Path>> From<P> for XandNodeDirConventionBuilder {
    fn from(path: P) -> Self {
        Self(PathBuf::from(path.as_ref()))
    }
}
