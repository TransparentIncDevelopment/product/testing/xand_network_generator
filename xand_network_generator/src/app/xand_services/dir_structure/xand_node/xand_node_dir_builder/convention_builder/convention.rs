use std::path::PathBuf;

pub struct XandNodeDirConvention {}

impl XandNodeDirConvention {
    pub fn substrate_dir(&self) -> PathBuf {
        "./xand".into()
    }

    pub fn chain_spec_dir(&self) -> PathBuf {
        self.substrate_dir().join("chainspec")
    }

    pub fn keypairs_dir(&self) -> PathBuf {
        self.substrate_dir().join("keypairs")
    }

    pub fn key_management_dir(&self) -> PathBuf {
        self.substrate_dir().join("key-management")
    }

    pub fn xand_api_dir(&self) -> PathBuf {
        "./xand-api".into()
    }

    pub fn xand_api_keys_dir(&self) -> PathBuf {
        self.xand_api_dir().join("initial-keys")
    }

    pub fn jwt_secret_filepath(&self) -> PathBuf {
        self.xand_api_dir().join("jwt-secret.txt")
    }

    pub fn libp2p_filename(&self) -> PathBuf {
        "validator-lp2p-key.env".into()
    }
}
