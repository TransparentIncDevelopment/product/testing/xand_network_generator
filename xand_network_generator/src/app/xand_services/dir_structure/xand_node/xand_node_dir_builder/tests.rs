use super::*;
use crate::app::{
    adapters::ip_provider::docker_ip_provider::DockerIpProvider,
    entities::{
        limited_agent::LimitedAgent,
        member::{states::banked_member::DeployableMember, Member},
        trust::Trust,
        validator::Validator,
    },
    external_wrappers::chain_spec_generation::ChainSpec,
    file_io,
    key_provider::ValidatorKeySet,
};
use std::path::Path;
use tempfile::tempdir;

#[test]
fn write_member__dir_structure_matches_expected() {
    // Given
    let mut entity = DockerComposeMember::test();
    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");
    entity.chain_spec = dummy_chain_spec_file(chainspec_filepath);
    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root.clone());

    // When
    builder.write_member(&entity).unwrap();

    // Then
    assert_common_xand_node_structure_exists(root);
}

#[test]
fn write_member__if_entity_auth_is_none_does_not_write_jwt_secret_to_file() {
    // Given
    let auth = None;
    let entity = Member::<DeployableMember>::with_auth(auth);

    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");

    let mut dcm = DockerComposeMember::test();
    dcm.member = entity;
    dcm.chain_spec = dummy_chain_spec_file(chainspec_filepath);

    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root);

    // When
    builder.write_member(&dcm).unwrap();

    // Then
    assert!(!builder.convention.get_xand_api_jwt_secret_path().exists())
}

#[test]
fn write_limited_agent__dir_structure_matches_expected() {
    // Given
    let mut entity = DockerComposeLimitedAgent::test();
    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");
    entity.chain_spec = dummy_chain_spec_file(chainspec_filepath);
    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root.clone());

    // When
    builder.write_limited_agent(&entity).unwrap();

    // Then
    assert_common_xand_node_structure_exists(root);
}

#[test]
fn write_limited_agent__if_entity_auth_is_none_does_not_write_jwt_secret_to_file() {
    // Given
    let auth = None;
    let la = LimitedAgent::with_auth(auth);

    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");

    let mut dcla = DockerComposeLimitedAgent::test();
    dcla.limited_agent = la;
    dcla.chain_spec = dummy_chain_spec_file(chainspec_filepath);

    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root);

    // When
    builder.write_limited_agent(&dcla).unwrap();

    // Then
    assert!(!builder.convention.get_xand_api_jwt_secret_path().exists())
}

#[test]
fn write_trust__dir_structure_matches_expected() {
    // Given
    let mut entity = DockerComposeTrust::test();
    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");
    entity.chain_spec = dummy_chain_spec_file(chainspec_filepath);
    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root.clone());

    // When
    builder.write_trust(&entity).unwrap();

    // Then
    assert_common_xand_node_structure_exists(root);
}

#[test]
fn write_trust__if_entity_auth_is_none_does_not_write_jwt_secret_to_file() {
    // Given
    let mut trust = Trust::test();
    trust.state.xand_api_auth = None;

    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");

    let mut dct = DockerComposeTrust::test();
    dct.trust = trust;
    dct.chain_spec = dummy_chain_spec_file(chainspec_filepath);

    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root);

    // When
    builder.write_trust(&dct).unwrap();

    // Then
    assert!(!builder.convention.get_xand_api_jwt_secret_path().exists())
}

#[test]
fn write_validator__dir_structure_matches_expected() {
    // Given
    let mut entity = DockerComposeValidator::test();
    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");
    entity.chain_spec = dummy_chain_spec_file(chainspec_filepath);
    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root.clone());

    // When
    builder.write_validator(&entity).unwrap();

    // Then
    assert_common_xand_node_structure_exists(&root);
    assert!(root.join("xand/validator-lp2p-key.env").is_file());
}

#[test]
fn write_validator__if_entity_auth_is_none_does_not_write_jwt_secret_to_file() {
    // Given
    let auth = None;
    let validator = Validator::new(ValidatorKeySet::test())
        .transition(vec![DockerIpProvider::test().get_subnet().addr()])
        .transition(auth, None);

    let temp_dir = tempdir().unwrap();
    let chainspec_filepath = temp_dir.path().join("dummychainspec.json");

    let mut dcv = DockerComposeValidator::test();
    dcv.validator = validator;
    dcv.chain_spec = dummy_chain_spec_file(chainspec_filepath);

    let xand_node_dir = tempdir().unwrap();
    let root = xand_node_dir.path().to_path_buf();
    let builder = XandNodeDirBuilder::new(root);

    // When
    builder.write_validator(&dcv).unwrap();

    // Then
    assert!(!builder.convention.get_xand_api_jwt_secret_path().exists())
}

fn dummy_chain_spec_file(chainspec_filepath: PathBuf) -> ChainSpec {
    file_io::write_file(chainspec_filepath.clone(), "{\"fake\": \"chainspec\"}").unwrap();
    ChainSpec::new(chainspec_filepath)
}

fn assert_common_xand_node_structure_exists<P: AsRef<Path>>(root: P) {
    let root = root.as_ref();
    assert!(root.join("xand").is_dir());
    assert!(root.join("xand/chainspec").is_dir());
    assert!(root.join("xand/keypairs").is_dir());
    assert!(root.join("xand/key-management").is_dir());
    assert!(root.join("xand-api").is_dir());
    assert!(root.join("xand-api/initial-keys").is_dir());
    assert!(root.join("xand-api/jwt-secret.txt").is_file());
}
