use crate::app::{
    contexts::docker_compose::{
        entities::docker_compose_trust::DockerComposeTrust,
        services::non_consensus_node::XAND_API_CONTAINER_PORT,
    },
    file_io::{create_dir, write_file},
    xand_services::dir_structure::trust_api::convention::DirConvention,
};
use std::path::PathBuf;

mod convention;
#[cfg(test)]
mod tests;

pub struct TrustApiDirectoryBuilder {
    convention: DirConvention,
}

impl TrustApiDirectoryBuilder {
    pub fn write(self, dct: &DockerComposeTrust) -> crate::Result<DirConvention> {
        self.init_dirs()?;

        // Write config file to location by convention
        let xand_api_url = format!(
            "http://{}:{}",
            dct.get_nc_node_service_name(),
            XAND_API_CONTAINER_PORT
        );

        let config = dct.trust.build_trust_api_config(xand_api_url);
        let config_file_str = serde_yaml::to_string(&config)?;
        write_file(self.convention.config_file(), config_file_str)?;

        Ok(self.convention)
    }

    /// Initialize for an isolated trust directory
    fn init_dirs(&self) -> crate::Result<()> {
        let config_dir = self.convention.config_dir();
        create_dir(&config_dir, true)?;

        Ok(())
    }
}

impl From<PathBuf> for TrustApiDirectoryBuilder {
    fn from(p: PathBuf) -> Self {
        Self {
            convention: DirConvention(p),
        }
    }
}
