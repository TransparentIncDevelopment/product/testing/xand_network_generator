use crate::{
    app::{
        entities::member::{
            member_state::MemberState,
            states::{
                banked_member::DeployableMember, networked_member::NetworkedMember,
                walleted_member::WalletedMember,
            },
        },
        external_wrappers::DomainXandKeyPair,
        key_provider::MemberKeySet,
        port::participant_info::member::MemberPublicInfo,
    },
    contracts::{
        data::{bank_info::BankInfo, MemberAddress},
        network::auth::JwtInfo,
    },
};
use std::{convert::TryInto, net::Ipv4Addr};
use tpfs_krypt::{KeyPair, KeyPairInstance, PublicKey};

pub mod member_state;
pub mod states;
#[cfg(test)]
mod tests;

#[derive(Clone)]
pub struct Member<S> {
    state: MemberState<S>,
}

impl<S> Member<S> {
    pub fn state(&self) -> &MemberState<S> {
        &self.state
    }
}

impl Member<WalletedMember> {
    pub fn new(keys: MemberKeySet) -> Self {
        let id = WalletedMember::new(keys);
        Member {
            state: MemberState::Walleted(id),
        }
    }

    /// State transition function consuming `Member<WalletedMember>` and producing `Member<NetworkedMember>`
    pub fn transition(self, ip_addrs: Vec<Ipv4Addr>) -> Member<NetworkedMember> {
        let networked_member = NetworkedMember::new(self.state.take(), ip_addrs);
        Member {
            state: MemberState::Networked(networked_member),
        }
    }
}

impl Member<NetworkedMember> {
    // State transition function
    pub fn transition<T: IntoIterator<Item = BankInfo>>(
        self,
        bank_info: T,
        member_api_auth: Option<JwtInfo>,
        xand_api_auth: Option<JwtInfo>,
    ) -> Member<DeployableMember> {
        let banked_member = DeployableMember {
            networked_member: self.state.take(),
            member_bank_info: bank_info.into_iter().collect(),
            member_api_auth,
            xand_api_auth,
        };
        Member {
            state: MemberState::DeployableMember(banked_member),
        }
    }
}

impl Member<DeployableMember> {
    pub fn kp(&self) -> &DomainXandKeyPair {
        self.state.borrow().networked_member.identity.kp()
    }

    pub fn encryption_key(&self) -> PublicKey {
        // TODO: tpfs_krypt types should be revisited to avoid exposing `Vec` and arrays forcing
        // the use of fallible conversions when such conversions cannot fail.
        self.encryption_key_pair()
            .public()
            .try_into()
            .expect("Public part of key pair must fit in PublicKey")
    }

    pub fn encryption_key_pair(&self) -> &KeyPairInstance {
        self.state
            .borrow()
            .networked_member
            .identity
            .encryption_key_pair()
    }

    pub fn member_api_auth(&self) -> &Option<JwtInfo> {
        &self.state.borrow().member_api_auth
    }

    pub fn xand_api_auth(&self) -> &Option<JwtInfo> {
        &self.state.borrow().xand_api_auth
    }
}
impl MemberPublicInfo for Member<DeployableMember> {
    fn get_address(&self) -> MemberAddress {
        let walleted_member = &self.state.borrow().networked_member.identity;
        walleted_member.get_address()
    }

    fn ips(&self) -> &Vec<Ipv4Addr> {
        &self.state.borrow().networked_member.ip_addrs
    }
}

impl From<NetworkedMember> for Member<NetworkedMember> {
    fn from(nm: NetworkedMember) -> Self {
        Member {
            state: MemberState::Networked(nm),
        }
    }
}
