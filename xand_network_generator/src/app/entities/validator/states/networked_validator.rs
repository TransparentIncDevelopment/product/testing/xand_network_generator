use crate::app::entities::validator::states::walleted_validator::WalletedValidator;
use std::net::Ipv4Addr;

#[derive(Clone, Debug)]
pub struct NetworkedValidator {
    identity: WalletedValidator,
    ips: Vec<Ipv4Addr>,
}

impl NetworkedValidator {
    pub fn new(identity: WalletedValidator, ips: Vec<Ipv4Addr>) -> Self {
        Self { identity, ips }
    }

    pub(crate) fn identity(&self) -> &WalletedValidator {
        &self.identity
    }
    pub(crate) fn ips(&self) -> &Vec<Ipv4Addr> {
        &self.ips
    }
}
