use crate::app::{external_wrappers::DomainXandKeyPair, key_provider::ValidatorKeySet};
use tpfs_krypt::KeyPairInstance;

#[derive(Clone, Debug)]
pub struct WalletedValidator {
    authority_kp: DomainXandKeyPair,
    libp2p_kp: DomainXandKeyPair,
    produce_blocks_kp: DomainXandKeyPair,
    finalize_blocks_kp: DomainXandKeyPair,
    encryption_kp: KeyPairInstance,
}

impl WalletedValidator {
    pub fn new(keys: ValidatorKeySet) -> Self {
        WalletedValidator {
            authority_kp: keys.authority_key,
            libp2p_kp: keys.libp2p_key,
            produce_blocks_kp: keys.produce_blocks_kp,
            finalize_blocks_kp: keys.finalize_blocks_kp,
            encryption_kp: keys.encryption_key,
        }
    }

    pub(crate) fn authority_kp(&self) -> &DomainXandKeyPair {
        &self.authority_kp
    }
    pub(crate) fn libp2p_kp(&self) -> &DomainXandKeyPair {
        &self.libp2p_kp
    }
    pub(crate) fn produce_blocks_kp(&self) -> &DomainXandKeyPair {
        &self.produce_blocks_kp
    }
    pub(crate) fn finalize_blocks_kp(&self) -> &DomainXandKeyPair {
        &self.finalize_blocks_kp
    }
    pub(crate) fn node_key(&self) -> String {
        self.libp2p_kp.secret_key_str()
    }
    pub(crate) fn encryption_kp(&self) -> &KeyPairInstance {
        &self.encryption_kp
    }
}
