use super::*;

use crate::app::{
    adapters::{auth_provider::AuthProvider, ip_provider::docker_ip_provider::DockerIpProvider},
    entities::validator::states::walleted_validator::WalletedValidator,
};

use crate::contracts::data::bank_info::{Account, Bank};
use std::{net::Ipv4Addr, str::FromStr};

/// Test helper for testing Validator in the entire network
impl Validator<DeployableValidator> {
    pub fn test() -> Self {
        let xand_api_auth = AuthProvider::get_test_xand_api_jwt_info();
        let bank_info = vec![BankInfo {
            bank: Bank::TestBankOne,
            account: Account::new(1231231231, 222222),
        }];
        Validator::new(ValidatorKeySet::test())
            .transition(vec![DockerIpProvider::test().get_subnet().addr()])
            .transition(bank_info, Some(xand_api_auth))
    }
}

#[test]
fn transition__walleted_validator_goes_to_networked_validator() {
    // Given
    let ip_addrs = vec![Ipv4Addr::from_str("172.28.0.1").unwrap()];
    let walleted_validator = Validator::<WalletedValidator>::new(ValidatorKeySet::test());

    // When
    let networked_validator = walleted_validator.transition(ip_addrs);

    // Then
    assert_matches!(networked_validator.state, ValidatorState::Networked(_));
}

#[test]
fn transition__networked_validator_goes_to_deployable_validator() {
    // Given
    let ip_addrs = vec![Ipv4Addr::from_str("172.28.0.1").unwrap()];
    let walleted_validator = Validator::<WalletedValidator>::new(ValidatorKeySet::test());
    let networked_validator = walleted_validator.transition(ip_addrs);
    let bank_info = vec![BankInfo {
        bank: Bank::TestBankOne,
        account: Account::new(1231231231, 222222),
    }];
    let xand_api_auth = AuthProvider::default().xand_api_jwt_info().unwrap();

    // When
    let deployable_validator = networked_validator.transition(bank_info, Some(xand_api_auth));

    // Then
    assert_matches!(deployable_validator.state, ValidatorState::Deployable(_));
}

#[test]
fn transition__deployable_validator_contains_correct_banks() {
    // Given
    let ip_addrs = vec![Ipv4Addr::from_str("172.28.0.1").unwrap()];
    let walleted_validator = Validator::<WalletedValidator>::new(ValidatorKeySet::test());
    let networked_validator = walleted_validator.transition(ip_addrs);
    let bank_info = vec![BankInfo {
        bank: Bank::TestBankOne,
        account: Account::new(1231231231, 222222),
    }];
    let xand_api_auth = AuthProvider::default().xand_api_jwt_info().unwrap();

    // When
    let deployable_validator =
        networked_validator.transition(bank_info.clone(), Some(xand_api_auth));

    // Then
    assert_eq!(
        deployable_validator.state().borrow().validator_bank_info(),
        &bank_info
    );
    assert!(deployable_validator
        .state()
        .borrow()
        .validator_bank_info()
        .iter()
        .next()
        .is_some());
}
