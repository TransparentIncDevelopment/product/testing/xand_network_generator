use crate::{
    app::entities::member::states::networked_member::NetworkedMember,
    contracts::{data::bank_info::BankInfo, network::auth::JwtInfo},
};

#[derive(Clone, Debug)]
pub struct DeployableMember {
    pub networked_member: NetworkedMember,
    pub member_bank_info: Vec<BankInfo>,
    pub member_api_auth: Option<JwtInfo>,
    pub xand_api_auth: Option<JwtInfo>,
}
