use super::*;

use crate::app::entities::member::states::{
    networked_member::NetworkedMember, walleted_member::WalletedMember,
};

use crate::app::adapters::auth_provider::AuthProvider;
use std::{net::Ipv4Addr, str::FromStr};

/// Member
/// - starts with ID
/// - gets an IP from an IPAssigner
/// - goes through the chain spec generator
/// - type with references to everything needed for docker-compose
///     - xand-api bundle
///         - path to chain spec
///         - path to the keyfile
///         - xandstrate_version
///         -
///     - member-api bundle
///         - member_api_version
///         -

#[test]
fn transition__walleted_member_goes_to_networked_member() {
    // Given
    let ip_addrs = vec![Ipv4Addr::from_str("172.28.0.1").unwrap()];
    let walleted_member = Member::<WalletedMember>::new(MemberKeySet::test());

    // When
    let networked_member = walleted_member.transition(ip_addrs);

    // Then
    assert_matches!(networked_member.state, MemberState::Networked(_));
}

#[test]
fn transition__networked_member_goes_to_banked_member() {
    // Given
    let ip_addrs = vec![Ipv4Addr::from_str("172.28.0.1").unwrap()];
    let member = WalletedMember::new(MemberKeySet::test());
    let networked_member: Member<_> = NetworkedMember::new(member, ip_addrs).into();
    let bank_info = vec![];
    let member_api_auth = None;
    let xand_api_auth = None;

    // When
    let banked_member = networked_member.transition(bank_info, member_api_auth, xand_api_auth);

    // Then
    assert_matches!(banked_member.state, MemberState::DeployableMember(_));
}

impl Member<DeployableMember> {
    pub fn test() -> Self {
        let ap = AuthProvider::default();
        let member_api_auth = ap.member_api_jwt_info().unwrap();
        let xand_api_auth = ap.xand_api_jwt_info().unwrap();
        Member::new(MemberKeySet::test())
            .transition(vec!["172.28.0.1".parse().unwrap()])
            .transition(vec![], Some(member_api_auth), Some(xand_api_auth))
    }

    pub fn with_auth(jwt_info: Option<JwtInfo>) -> Self {
        Member::new(MemberKeySet::test())
            .transition(vec!["172.28.0.1".parse().unwrap()])
            .transition(vec![], jwt_info.clone(), jwt_info)
    }
}
