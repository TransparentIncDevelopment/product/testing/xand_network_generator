#[derive(Clone)]
pub enum MemberState<S> {
    Walleted(S),
    Networked(S),
    DeployableMember(S),
}
impl<S: Clone> MemberState<S> {
    pub fn take(self) -> S {
        match self {
            MemberState::Walleted(s) => s,
            MemberState::Networked(s) => s,
            MemberState::DeployableMember(s) => s,
        }
    }

    pub fn borrow(&self) -> &S {
        match self {
            MemberState::Walleted(s) => s,
            MemberState::Networked(s) => s,
            MemberState::DeployableMember(s) => s,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::{Debug, Formatter};

    impl<S: Clone> Debug for MemberState<S> {
        fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
            let state_str = match self {
                MemberState::Walleted(_) => "Walleted",
                MemberState::Networked(_) => "Networked",
                MemberState::DeployableMember(_) => "Banked",
                // TODO update Banked -> Deployable
            };
            write!(f, "{}", state_str)
        }
    }
}
