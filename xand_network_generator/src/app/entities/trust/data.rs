use crate::contracts::data::bank_info::BankInfo;

#[derive(Clone, Debug)]
pub struct TrustBankAccountSet {
    pub mcb: Option<BankInfo>,
    pub provident: Option<BankInfo>,

    pub test_bank_one: Option<BankInfo>,
    pub test_bank_two: Option<BankInfo>,
}
