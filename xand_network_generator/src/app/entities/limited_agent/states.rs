pub mod keyed_limited_agent {
    use crate::app::{external_wrappers::DomainXandKeyPair, key_provider::LimitedAgentKeySet};

    #[derive(Clone, Debug)]
    pub struct KeyedLimitedAgent {
        kp: DomainXandKeyPair,
    }

    impl KeyedLimitedAgent {
        pub fn new(keys: LimitedAgentKeySet) -> Self {
            KeyedLimitedAgent { kp: keys.key }
        }

        pub fn kp(&self) -> &DomainXandKeyPair {
            &self.kp
        }
    }
}
pub mod networked_limited_agent {
    use super::keyed_limited_agent::KeyedLimitedAgent;
    use std::net::Ipv4Addr;

    #[derive(Clone, Debug)]
    pub struct NetworkedLimitedAgent {
        pub identity: KeyedLimitedAgent,
        pub ip_addrs: Vec<Ipv4Addr>,
    }

    impl NetworkedLimitedAgent {
        pub fn new(identity: KeyedLimitedAgent, ip_addrs: Vec<Ipv4Addr>) -> Self {
            Self { identity, ip_addrs }
        }
    }
}

pub mod deployable_limited_agent {
    use crate::{
        app::entities::limited_agent::states::networked_limited_agent::NetworkedLimitedAgent,
        contracts::network::auth::JwtInfo,
    };

    #[derive(Clone, Debug)]
    pub struct DeployableLimitedAgent {
        pub networked_la: NetworkedLimitedAgent,
        pub xand_api_auth: Option<JwtInfo>,
    }
}
