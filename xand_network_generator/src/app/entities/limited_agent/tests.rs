use super::*;

use crate::app::adapters::auth_provider::AuthProvider;
use std::{net::Ipv4Addr, str::FromStr};

#[test]
fn transition__limited_agent_maintains_kp_after_transition_through_states() {
    // Given
    let ip_addrs = vec![Ipv4Addr::from_str("172.28.0.1").unwrap()];
    let keyed_la = LimitedAgent::new(LimitedAgentKeySet::test());
    let address_copy = keyed_la.state.kp().get_address();
    let auth = AuthProvider::get_test_xand_api_jwt_info();

    // When
    let networked_la = keyed_la.transition(ip_addrs).transition(Some(auth));

    // Then
    assert_eq!(networked_la.kp().get_address(), address_copy);
}

impl LimitedAgent<DeployableLimitedAgent> {
    pub(crate) fn test() -> Self {
        let auth = AuthProvider::get_test_xand_api_jwt_info();
        LimitedAgent::with_auth(Some(auth))
    }

    pub(crate) fn with_auth(auth: Option<JwtInfo>) -> Self {
        LimitedAgent::new(LimitedAgentKeySet::test())
            .transition(vec![Ipv4Addr::from_str("172.28.0.1").unwrap()])
            .transition(auth)
    }
}
