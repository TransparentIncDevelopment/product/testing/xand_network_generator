use crate::app::contexts::docker_compose::entities::docker_compose_trust::conversion::build_trust_api_config;
use crate::{
    app::{
        contexts::docker_compose::{
            entities::docker_compose_trust::conversion::build_config,
            services::bank_url_provider::BankUrlProvider,
        },
        entities::trust::{data::TrustBankAccountSet, states::banked_trust::DeployableTrust},
        external_wrappers::DomainXandKeyPair,
        key_provider::TrustKeySet,
        port::{bank_provider::BankProvider, ip_provider::IpProvider},
    },
    contracts::{
        data::{
            bank_info::{Account, Bank, BankInfo},
            TrustAddress,
        },
        network::auth::JwtInfo,
    },
};
use std::{
    collections::{HashMap, HashSet},
    convert::TryInto,
    net::Ipv4Addr,
};
use tpfs_krypt::{KeyPair, KeyPairInstance, PublicKey};
use trust_api_config::TrustApiConfig;
use trustee_node_config::TrusteeConfig;

pub mod data;
pub mod states;

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::adapters::{
        auth_provider::AuthProvider,
        bank_info_provider::docker_bank_info_provider::DockerBankInfoProvider,
        ip_provider::docker_ip_provider::DockerIpProvider,
    };
    use strum::IntoEnumIterator;

    impl Trust {
        pub fn test() -> Self {
            let bp = DockerBankInfoProvider::new();
            let mut ip = DockerIpProvider::test();
            let enabled_banks = Bank::iter().collect();
            let auth = AuthProvider::get_test_xand_api_jwt_info();
            Trust::new(
                &bp,
                &mut ip,
                TrustKeySet::test(),
                &enabled_banks,
                Some(auth),
            )
        }
    }
    #[test]
    fn generate__has_consistent_bank_accounts() {
        // Given
        let bp = DockerBankInfoProvider::new();
        let mut ip = DockerIpProvider::test();
        let enabled_banks = Bank::iter().collect();
        let auth = Some(AuthProvider::get_test_xand_api_jwt_info());
        // When
        let t_1 = Trust::new(
            &bp,
            &mut ip,
            TrustKeySet::test(),
            &enabled_banks,
            auth.clone(),
        );
        let t_2 = Trust::new(&bp, &mut ip, TrustKeySet::test(), &enabled_banks, auth);

        // Then
        assert_eq!(t_1.accounts(), t_2.accounts())
    }

    #[test]
    fn generate__has_1_ip_address() {
        // When
        let t = Trust::test();

        // Then
        assert_eq!(t.ips().len(), 1)
    }

    #[test]
    fn generate__has_keypair_matches_get_address() {
        // When
        let t = Trust::test();

        // Then
        assert_eq!(t.kp().get_address(), t.get_address().0.to_string());
    }
}
#[derive(Clone)]
pub struct Trust {
    pub(crate) state: DeployableTrust,
}

impl Trust {
    pub(crate) fn build_config(
        &self,
        xand_api_url: String,
        member_accounts: &HashMap<String, Vec<BankInfo>>,
        validator_accounts: &HashMap<String, Vec<BankInfo>>,
        bank_urls: &dyn BankUrlProvider,
    ) -> TrusteeConfig {
        build_config(
            self,
            xand_api_url,
            member_accounts,
            validator_accounts,
            bank_urls,
        )
    }

    pub(crate) fn build_trust_api_config(&self, xand_api_url: String) -> TrustApiConfig {
        build_trust_api_config(self, xand_api_url)
    }
}

impl Trust {
    pub fn new<B: BankProvider, IP: IpProvider>(
        bank_provider: &B,
        ip_provider: &mut IP,
        keys: TrustKeySet,
        enabled_banks: &HashSet<Bank>,
        xand_api_auth: Option<JwtInfo>,
    ) -> Self {
        let get_trust_if_enabled = |b| enabled_banks.get(&b).map(|_| bank_provider.get_trust(b));
        let acct_set = TrustBankAccountSet {
            mcb: get_trust_if_enabled(Bank::Mcb),
            provident: get_trust_if_enabled(Bank::Provident),
            test_bank_one: get_trust_if_enabled(Bank::TestBankOne),
            test_bank_two: get_trust_if_enabled(Bank::TestBankTwo),
        };
        Self {
            state: DeployableTrust::new(
                keys.key,
                vec![ip_provider.next()],
                acct_set,
                keys.encryption_key,
                xand_api_auth,
            ),
        }
    }

    pub fn accounts(&self) -> HashMap<Bank, Account> {
        let mut map = HashMap::new();

        if let Some(bank) = &self.state.acct_set.mcb {
            map.insert(Bank::Mcb, bank.account.clone());
        }

        if let Some(bank) = &self.state.acct_set.provident {
            map.insert(Bank::Provident, bank.account.clone());
        }

        if let Some(bank) = &self.state.acct_set.test_bank_one {
            map.insert(Bank::TestBankOne, bank.account.clone());
        }

        if let Some(bank) = &self.state.acct_set.test_bank_two {
            map.insert(Bank::TestBankTwo, bank.account.clone());
        }

        map
    }

    pub fn get_address(&self) -> TrustAddress {
        TrustAddress(self.state.kp.get_address().try_into().unwrap())
    }

    pub fn kp(&self) -> &DomainXandKeyPair {
        &self.state.kp
    }

    pub fn ips(&self) -> &Vec<Ipv4Addr> {
        &self.state.ips
    }

    pub fn encryption_key(&self) -> PublicKey {
        // TODO: tpfs_krypt types should be revisited to avoid exposing `Vec` and arrays forcing
        // the use of fallible conversions when such conversions cannot fail.
        self.state
            .encryption_key_pair
            .public()
            .try_into()
            .expect("Public part of key pair must fit in PublicKey")
    }

    pub fn encryption_key_pair(&self) -> &KeyPairInstance {
        &self.state.encryption_key_pair
    }

    pub fn xand_api_auth(&self) -> &Option<JwtInfo> {
        &self.state.xand_api_auth
    }
}
