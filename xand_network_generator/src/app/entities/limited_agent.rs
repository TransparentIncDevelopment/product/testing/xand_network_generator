use crate::{
    app::{
        entities::limited_agent::states::deployable_limited_agent::DeployableLimitedAgent,
        external_wrappers::DomainXandKeyPair, key_provider::LimitedAgentKeySet,
    },
    contracts::network::auth::JwtInfo,
};
use states::{
    keyed_limited_agent::KeyedLimitedAgent, networked_limited_agent::NetworkedLimitedAgent,
};
use std::net::Ipv4Addr;

pub mod states;
#[cfg(test)]
mod tests;

#[derive(Clone)]
pub struct LimitedAgent<S> {
    state: S,
}

impl LimitedAgent<KeyedLimitedAgent> {
    /// Generates a key for the LimitedAgent, putting it in its starting state.
    pub fn new(keys: LimitedAgentKeySet) -> Self {
        Self {
            state: KeyedLimitedAgent::new(keys),
        }
    }

    /// Given IP addresses, transitions the LimitedAgent's state from the KeyedLimitedAgent to NetworkedLimitedAgent
    pub fn transition(self, ip_addrs: Vec<Ipv4Addr>) -> LimitedAgent<NetworkedLimitedAgent> {
        let networked_la = NetworkedLimitedAgent::new(self.state, ip_addrs);
        LimitedAgent {
            state: networked_la,
        }
    }
}

impl LimitedAgent<NetworkedLimitedAgent> {
    /// Given IP addresses, transitions the LimitedAgent's state from the KeyedLimitedAgent to NetworkedLimitedAgent
    pub fn transition(
        self,
        xand_api_auth: Option<JwtInfo>,
    ) -> LimitedAgent<DeployableLimitedAgent> {
        LimitedAgent {
            state: DeployableLimitedAgent {
                networked_la: self.state,
                xand_api_auth,
            },
        }
    }
}

impl LimitedAgent<DeployableLimitedAgent> {
    pub fn ips(&self) -> &Vec<Ipv4Addr> {
        &self.state.networked_la.ip_addrs
    }

    pub fn kp(&self) -> &DomainXandKeyPair {
        self.state.networked_la.identity.kp()
    }

    pub fn xand_api_auth(&self) -> &Option<JwtInfo> {
        &self.state.xand_api_auth
    }
}
