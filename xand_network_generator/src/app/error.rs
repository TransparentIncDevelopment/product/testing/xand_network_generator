use crate::app::{
    error::{file_io_error::FileIoError, load_configuration_error::LoadConfigurationError},
    network::error::XandEntitySetError,
};
use thiserror::Error;

pub mod file_io_error;
pub mod keygen_error;
pub mod load_configuration_error;
pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    FileIoError(#[from] FileIoError),
    #[error("{:?}", 0)]
    Keygen(String),
    #[error("{:?}", 0)]
    DeserializeError(#[from] serde_yaml::Error),
    #[error("{:?}", 0)]
    DeserializeJsonError(#[from] serde_json::Error),
    #[error("{:?}", 0)]
    ConfigError(#[from] LoadConfigurationError),
    #[error("{:?}", 0)]
    SerdeYamlError(String),
    #[error("{:?}", 0)]
    GenerateXandEntitySet(#[from] XandEntitySetError),
}
