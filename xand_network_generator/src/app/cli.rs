use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub enum XngCli {
    /// Generates XAND entities the corresponding docker-compose.yaml file and directories corresponding to each entity.
    #[structopt(name = "generate")]
    Generate {
        #[structopt(flatten)]
        generate_config: GenerateCmdArgs,
    },
}

#[derive(StructOpt, Debug)]
pub struct GenerateCmdArgs {
    /// Configuration filepath
    #[structopt(
        long = "config",
        default_value = "./config/config.yaml",
        parse(from_os_str)
    )]
    pub config_file: PathBuf,

    /// Path to a published chain spec zip downloaded from Artifactory
    #[structopt(long = "chainspec-zip", parse(from_os_str))]
    pub chainspec_zip: PathBuf,

    /// Directory for `generate` to write all output to. Once complete, you can run `docker-compose up -d` here
    #[structopt(long = "output-dir", default_value = "./generated", parse(from_os_str))]
    pub output_dir: PathBuf,
}
