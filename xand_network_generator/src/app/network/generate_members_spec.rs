#[derive(Debug, Clone)]
pub struct GenerateMembersSpec {
    pub permissioned_count: usize,
    pub non_permissioned_count: usize,
}

impl GenerateMembersSpec {
    pub fn new(permissioned_count: usize, non_permissioned_count: usize) -> Self {
        GenerateMembersSpec {
            permissioned_count,
            non_permissioned_count,
        }
    }

    pub fn total(&self) -> usize {
        self.permissioned_count + self.non_permissioned_count
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl GenerateMembersSpec {
        pub fn test() -> Self {
            Self {
                permissioned_count: 3,
                non_permissioned_count: 0,
            }
        }
    }

    #[test]
    fn total__returns_sum_of_permissioned_and_non_permissioned() {
        // Given
        let spec = GenerateMembersSpec::new(7, 6);

        // When
        let total = spec.total();

        // Then
        assert_eq!(total, 13);
    }
}
