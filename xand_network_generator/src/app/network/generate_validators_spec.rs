#[derive(Debug, Clone)]
pub struct GenerateValidatorsSpec {
    pub permissioned_count: usize,
    pub non_permissiond_count: usize,
}

impl GenerateValidatorsSpec {
    pub fn new(permissioned_count: usize, non_permissiond_count: usize) -> Self {
        GenerateValidatorsSpec {
            permissioned_count,
            non_permissiond_count,
        }
    }

    pub fn total(&self) -> usize {
        self.permissioned_count + self.non_permissiond_count
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl GenerateValidatorsSpec {
        pub fn test() -> Self {
            Self {
                permissioned_count: 3,
                non_permissiond_count: 0,
            }
        }
    }

    #[test]
    fn total__returns_sum_of_permissioned_and_non_permissioned() {
        // Given
        let spec = GenerateValidatorsSpec::new(7, 6);

        // When
        let total = spec.total();

        // Then
        assert_eq!(total, 13);
    }
}
