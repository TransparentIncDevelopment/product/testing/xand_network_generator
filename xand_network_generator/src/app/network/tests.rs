use strum::IntoEnumIterator;

use super::*;

impl XandEntitySet {
    #[cfg(test)]
    pub fn test() -> Self {
        let member_spec = GenerateMembersSpec::new(3, 2);
        let validator_spec = GenerateValidatorsSpec::new(5, 2);

        Self::generate(
            member_spec,
            validator_spec,
            Some(0),
            Bank::iter().collect(),
            true,
        )
        .unwrap()
    }
}

#[test]
fn generate__produces_consistent_keys_when_seeded() {
    let member_spec = GenerateMembersSpec::new(3, 0);
    let validator_spec = GenerateValidatorsSpec::new(3, 0);

    let set1 = XandEntitySet::generate(
        member_spec.clone(),
        validator_spec.clone(),
        Some(0),
        Bank::iter().collect(),
        false,
    )
    .unwrap();
    let set2 = XandEntitySet::generate(
        member_spec,
        validator_spec,
        Some(0),
        Bank::iter().collect(),
        false,
    )
    .unwrap();

    assert_eq!(set1.trust.kp().get_address(), set2.trust.kp().get_address());
    assert_eq!(
        set1.limited_agent.kp().get_address(),
        set2.limited_agent.kp().get_address()
    );

    for (member_1, member_2) in set1
        .permissioned_members
        .iter()
        .zip(set2.permissioned_members)
    {
        assert_eq!(member_1.kp().get_address(), member_2.kp().get_address());
    }
    for (validator_1, validator_2) in set1
        .permissioned_validators
        .iter()
        .zip(set2.permissioned_validators)
    {
        assert_eq!(
            validator_1.state().borrow().node_key(),
            validator_2.state().borrow().node_key()
        );
    }
}

#[test]
fn generate__produces_consistent_keys_when_adding_nodes() {
    let member_spec = GenerateMembersSpec::new(1, 0);
    let validator_spec = GenerateValidatorsSpec::new(2, 0);
    let set1 = XandEntitySet::generate(
        member_spec.clone(),
        validator_spec.clone(),
        Some(0),
        Bank::iter().collect(),
        false,
    )
    .unwrap();
    let set2 = XandEntitySet::generate(
        member_spec,
        validator_spec,
        Some(0),
        Bank::iter().collect(),
        false,
    )
    .unwrap();

    assert_eq!(set1.trust.kp().get_address(), set2.trust.kp().get_address());
    assert_eq!(
        set1.limited_agent.kp().get_address(),
        set2.limited_agent.kp().get_address()
    );

    assert_eq!(
        set1.permissioned_members[0].kp().get_address(),
        set2.permissioned_members[0].kp().get_address()
    );
    assert_eq!(
        set1.permissioned_validators[0].state().borrow().node_key(),
        set2.permissioned_validators[0].state().borrow().node_key()
    );
}

#[test]
fn generate__produces_expected_number_of_members() {
    // Given
    let member_spec = GenerateMembersSpec::new(2, 4);
    let validator_spec = GenerateValidatorsSpec::test();

    // When
    let entity_set = XandEntitySet::generate(
        member_spec,
        validator_spec,
        Some(0),
        Bank::iter().collect(),
        false,
    )
    .unwrap();

    // Then
    assert_eq!(entity_set.permissioned_members.len(), 2);
    assert_eq!(entity_set.extra_members.len(), 4);
}

#[test]
fn generate__produced_expected_number_validators() {
    // Given
    let member_spec = GenerateMembersSpec::test();
    let validator_spec = GenerateValidatorsSpec::new(2, 4);

    // When
    let entity_set = XandEntitySet::generate(
        member_spec,
        validator_spec,
        Some(0),
        Bank::iter().collect(),
        false,
    )
    .unwrap();

    // Then
    assert_eq!(entity_set.permissioned_validators.len(), 2);
    assert_eq!(entity_set.extra_validators.len(), 4);
}

#[test]
fn generate__entities_have_none_auth_when_jwt_is_disabled() {
    // Given
    let enable_auth = false;
    let member_spec = GenerateMembersSpec::test();
    let validator_spec = GenerateValidatorsSpec::test();

    // When
    let entity_set = XandEntitySet::generate(
        member_spec,
        validator_spec,
        Some(0),
        Bank::iter().collect(),
        enable_auth,
    )
    .unwrap();

    // Then
    assert!(entity_set.limited_agent.xand_api_auth().is_none());
    assert!(entity_set.trust.xand_api_auth().is_none());
    assert!(entity_set.permissioned_validators.iter().all(|v| v
        .state()
        .borrow()
        .xand_api_auth()
        .is_none()));
    assert!(entity_set.permissioned_members.iter().all(|v| v
        .state()
        .borrow()
        .xand_api_auth
        .is_none()));
    // TODO Make `Member` internal data private, add accessors for things like this xand_api_auth - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7400
}

#[test]
fn generate__entities_have_some_auth_when_jwt_is_enabled() {
    // Given
    let enable_auth = true;
    let member_spec = GenerateMembersSpec::new(2, 0);
    let validator_spec = GenerateValidatorsSpec::new(2, 0);

    // When
    let entity_set = XandEntitySet::generate(
        member_spec,
        validator_spec,
        Some(0),
        Bank::iter().collect(),
        enable_auth,
    )
    .unwrap();

    // Then
    assert!(entity_set.limited_agent.xand_api_auth().is_some());
    assert!(entity_set.trust.xand_api_auth().is_some());
    assert!(entity_set.permissioned_validators.iter().all(|v| v
        .state()
        .borrow()
        .xand_api_auth()
        .is_some()));
    assert!(entity_set.permissioned_members.iter().all(|v| v
        .state()
        .borrow()
        .xand_api_auth
        .is_some()));
}
