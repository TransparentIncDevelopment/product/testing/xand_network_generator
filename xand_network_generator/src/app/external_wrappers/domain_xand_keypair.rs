use rand::{CryptoRng, RngCore};
use std::{borrow::Borrow, path::Path, rc::Rc};
use tpfs_krypt::{secrecy::ExposeSecret, AnyXandKeyPair, XandKeyPair, XandKeyType};

#[derive(Clone, Debug)]
pub struct DomainXandKeyPair {
    external_kp: Rc<AnyXandKeyPair>,
}

impl DomainXandKeyPair {
    pub(crate) fn generate_libp2p<R>(rng: &mut R) -> DomainXandKeyPair
    where
        R: RngCore + CryptoRng,
    {
        let external_kp = Rc::new(XandKeyType::ValidatorLibp2p.generate_with(rng).unwrap());
        DomainXandKeyPair { external_kp }
    }

    pub(crate) fn generate_produce_blocks_kp<R>(rng: &mut R) -> DomainXandKeyPair
    where
        R: RngCore + CryptoRng,
    {
        let external_kp = Rc::new(
            XandKeyType::ValidatorSessionProduceBlocks
                .generate_with(rng)
                .unwrap(),
        );
        DomainXandKeyPair { external_kp }
    }

    pub(crate) fn generate_finalize_blocks_kp<R>(rng: &mut R) -> DomainXandKeyPair
    where
        R: RngCore + CryptoRng,
    {
        let external_kp = Rc::new(
            XandKeyType::ValidatorSessionFinalizeBlocks
                .generate_with(rng)
                .unwrap(),
        );
        DomainXandKeyPair { external_kp }
    }

    pub(crate) fn generate_authority_key<R>(rng: &mut R) -> DomainXandKeyPair
    where
        R: RngCore + CryptoRng,
    {
        let external_kp = Rc::new(XandKeyType::ValidatorAuthority.generate_with(rng).unwrap());
        DomainXandKeyPair { external_kp }
    }

    pub(crate) fn generate_trust_key<R>(rng: &mut R) -> DomainXandKeyPair
    where
        R: RngCore + CryptoRng,
    {
        let external_kp = Rc::new(XandKeyType::Trust.generate_with(rng).unwrap());
        DomainXandKeyPair { external_kp }
    }

    pub(crate) fn generate_member_key<R>(rng: &mut R) -> DomainXandKeyPair
    where
        R: RngCore + CryptoRng,
    {
        let external_kp = Rc::new(XandKeyType::Wallet.generate_with(rng).unwrap());
        DomainXandKeyPair { external_kp }
    }

    pub(crate) fn generate_limited_agent_key<R>(rng: &mut R) -> DomainXandKeyPair
    where
        R: RngCore + CryptoRng,
    {
        let external_kp = Rc::new(XandKeyType::LimitedAgent.generate_with(rng).unwrap());
        DomainXandKeyPair { external_kp }
    }

    pub fn write_to(&self, dir: &Path) -> crate::Result<()> {
        self.external_kp
            .write_to_directory(dir)
            .map_err(|e| crate::app::error::Error::Keygen(e.to_string()))?;
        Ok(())
    }
    pub fn get_address(&self) -> String {
        self.external_kp.address().unwrap()
    }

    pub fn secret_key_str(&self) -> String {
        if let AnyXandKeyPair::ValidatorLibp2p(key) = self.external_kp.borrow() {
            key.get_secret_string().expose_secret().clone()
        } else {
            panic!("Shouldn't be needing secrets for these key types")
        }
    }
}
