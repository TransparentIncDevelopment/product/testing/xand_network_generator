use crate::app::{
    entities::{
        limited_agent::{states::deployable_limited_agent::DeployableLimitedAgent, LimitedAgent},
        member::{states::banked_member::DeployableMember, Member},
        trust::Trust,
        validator::{states::deployable_validator::DeployableValidator, Validator},
    },
    port::participant_info::member::MemberPublicInfo,
};
use chain_spec_generator::{
    LimitedAgentIdentity, MemberIdentity, TrustIdentity, ValidatorIdentity, ValidatorKeys,
};

impl From<Trust> for TrustIdentity {
    fn from(t: Trust) -> Self {
        TrustIdentity {
            address: t
                .kp()
                .get_address()
                .parse()
                .expect("`Address -> String -> Address` should not fail"),
            pub_key: t.encryption_key().into(),
            cidr_blocks: t.ips().iter().map(Into::into).collect(),
        }
    }
}

impl From<LimitedAgent<DeployableLimitedAgent>> for LimitedAgentIdentity {
    fn from(la: LimitedAgent<DeployableLimitedAgent>) -> Self {
        LimitedAgentIdentity {
            address: la
                .kp()
                .get_address()
                .parse()
                .expect("`Address -> String -> Address` should not fail"),
            cidr_blocks: la.ips().iter().map(Into::into).collect(),
        }
    }
}

impl From<&Member<DeployableMember>> for MemberIdentity {
    fn from(m: &Member<DeployableMember>) -> Self {
        MemberIdentity {
            address: m
                .kp()
                .get_address()
                .parse()
                .expect("`Address -> String -> Address` should not fail"),
            pub_key: m.encryption_key().into(),
            cidr_blocks: m.ips().iter().map(Into::into).collect(),
        }
    }
}

impl From<&Validator<DeployableValidator>> for ValidatorIdentity {
    fn from(v: &Validator<DeployableValidator>) -> Self {
        let state = v.state();
        Self {
            key_gen_summary: ValidatorKeys {
                val_kp_pub: state.borrow().validator_address().into(),
                produce_blocks_kp_pub: state.borrow().validator_block_production_address().into(),
                finalize_blocks_kp_pub: state
                    .borrow()
                    .validator_block_finalization_address()
                    .into(),
                pub_key: v.encryption_key().into(),
            },
            cidr_blocks: v.ips().iter().map(Into::into).collect(),
        }
    }
}
