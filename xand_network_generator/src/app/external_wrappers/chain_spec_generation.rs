use crate::app::{file_io, network::XandEntitySet};
use chain_spec_generator::{
    ChainSpec as ChainSpecGenerator, EmissionRate, MemberIdentity, ValidatorIdentity,
};
use std::num::NonZeroU64;
use std::path::{Path, PathBuf};

pub struct ChainSpecBuilder {
    template_filepath: PathBuf,
    minor_units_per_validator_emission: u64,
    block_quota: NonZeroU64,
}

impl ChainSpecBuilder {
    pub fn new(
        template_filepath: PathBuf,
        minor_units_per_validator_emission: u64,
        block_quota: NonZeroU64,
    ) -> Self {
        ChainSpecBuilder {
            template_filepath,
            minor_units_per_validator_emission,
            block_quota,
        }
    }
}

pub mod conversions;

impl ChainSpecBuilder {
    pub fn build(&self, entity_set: &XandEntitySet, workdir: PathBuf) -> crate::Result<ChainSpec> {
        let trust = entity_set.trust().into();
        let limited_agent = entity_set.limited_agent().into();
        let members: Vec<MemberIdentity> = entity_set.members().iter().map(Into::into).collect();
        let validators: Vec<ValidatorIdentity> =
            entity_set.validators().iter().map(Into::into).collect();
        let chainspec = ChainSpecGenerator {
            trust,
            limited_agent,
            members,
            validators,
            validator_emission_rate: EmissionRate {
                minor_units_per_validator_emission: self.minor_units_per_validator_emission,
                block_quota: self.block_quota,
            },
        };
        let out_filepath = chainspec
            .generate_into(&self.template_filepath, &workdir)
            .unwrap();
        let chain_spec = ChainSpec::new(out_filepath);
        Ok(chain_spec)
    }
}

#[derive(Clone)]
pub struct ChainSpec {
    filepath: PathBuf,
}

impl ChainSpec {
    pub fn new(filepath: PathBuf) -> Self {
        ChainSpec { filepath }
    }

    #[cfg(test)]
    pub fn test() -> Self {
        Self {
            filepath: "./some/invalid/path".into(),
        }
    }

    pub fn copy_to_dir<P: AsRef<Path>>(&self, to_dir: P) -> crate::Result<()> {
        let to_file = to_dir.as_ref().join(self.filepath.file_name().unwrap());
        file_io::copy_file(&self.filepath, &to_file)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::Value;
    use std::fs;
    use tempfile::TempDir;

    const CHAINSPEC_VERSION: &str = "8.1.1";

    fn build_chainspec(
        version: &str,
        minor_units_per_validator_emission: u64,
        block_quota: NonZeroU64,
    ) -> Value {
        let entity_set = XandEntitySet::test();
        // Note: "tempdir" must not be dropped for the duration of this function
        let tempdir = TempDir::new().unwrap();
        let workdir = tempdir.path().into();

        let template_path = PathBuf::from(format!(
            "{}/.test-assets/chain-spec-template.{}.zip",
            env!("CARGO_MANIFEST_DIR"),
            version
        ));

        // TODO - address intermittent failure

        let sut = ChainSpecBuilder::new(
            template_path,
            minor_units_per_validator_emission,
            block_quota,
        );
        let chainspec = sut.build(&entity_set, workdir).unwrap();

        let output = fs::read(chainspec.filepath).unwrap();
        serde_json::from_str(&String::from_utf8_lossy(&output)).unwrap()
    }

    #[test]
    fn verify_validator_emission_rate_can_be_customzied() {
        // Given
        let validator_emissions_minor_units = 123u64;
        let validator_emissions_block_quota = 2;
        // When
        let json_value = build_chainspec(
            CHAINSPEC_VERSION,
            validator_emissions_minor_units,
            validator_emissions_block_quota.try_into().unwrap(),
        );
        // Then
        assert_eq!(
            json_value["genesis"]["runtime"]["validatorEmissions"]["emissionRateSetting"]
                ["minor_units_per_emission"],
            validator_emissions_minor_units
        );
        assert_eq!(
            json_value["genesis"]["runtime"]["validatorEmissions"]["emissionRateSetting"]
                ["block_quota"],
            validator_emissions_block_quota
        );
    }
}
