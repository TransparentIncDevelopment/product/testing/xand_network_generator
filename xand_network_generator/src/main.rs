use structopt::StructOpt;
use xand_network_generator::app::{cli::XngCli, lib_main};

pub fn main() -> xand_network_generator::Result<()> {
    lib_main(XngCli::from_args())
}
