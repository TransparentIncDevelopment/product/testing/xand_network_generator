#[derive(Clone)]
pub struct BootnodeUrlVec {
    pub urls: Vec<String>,
}

impl BootnodeUrlVec {
    pub fn to_formatted_arg_string(&self) -> String {
        self.urls.join(" ")
    }
}

impl From<Vec<&str>> for BootnodeUrlVec {
    fn from(urls: Vec<&str>) -> Self {
        Self {
            urls: urls.into_iter().map(Into::into).collect(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl BootnodeUrlVec {
        pub fn test() -> Self {
            vec!["/dummy/bootnode/1", "dummy/bootnode/2", "dummy/bootnode/3"].into()
        }
    }
}
