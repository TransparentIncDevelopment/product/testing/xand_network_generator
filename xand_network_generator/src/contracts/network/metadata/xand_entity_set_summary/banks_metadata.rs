use crate::contracts::data::bank_info::Bank;
use std::collections::HashMap;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct BanksMetadata {
    #[serde(serialize_with = "serde_ordered_collections::map::sorted_serialize")]
    pub urls: HashMap<Bank, String>,
}
