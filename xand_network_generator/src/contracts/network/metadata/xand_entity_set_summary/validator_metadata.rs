use super::XandApiDetails;
use crate::contracts::data::bank_info::BankInfo;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct ValidatorMetadata {
    pub entity_name: String,
    pub xand_api_details: XandApiDetails,
    pub address: String,
    pub initially_permissioned: bool,
    pub banks: Vec<BankInfo>,
}
