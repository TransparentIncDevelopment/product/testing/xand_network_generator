use crate::contracts::network::auth::JwtInfo;
use url::Url;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct MemberApiDetails {
    pub member_api_url: Url,
    pub auth: Option<JwtInfo>,
}
