use super::XandApiDetails;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct LimitedAgentMetadata {
    pub address: String,
    pub xand_api_details: XandApiDetails,
}
