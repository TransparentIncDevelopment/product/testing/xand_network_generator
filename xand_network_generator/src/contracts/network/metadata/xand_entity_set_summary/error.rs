use crate::app::error::file_io_error::FileIoError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum DeserializeXandEntitySetSummaryError {
    #[error(transparent)]
    IoError(#[from] FileIoError),
    #[error(transparent)]
    DeserializeError(#[from] serde_yaml::Error),
}
