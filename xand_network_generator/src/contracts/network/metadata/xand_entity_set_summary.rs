use crate::app::{
    contexts::docker_compose::{
        docker_compose_network::docker_xand_entity_set::DockerXandEntitySet,
        entities::{
            docker_compose_member::DockerComposeMember,
            docker_compose_validator::DockerComposeValidator,
        },
    },
    file_io,
    port::participant_info::member::MemberPublicInfo,
};
use error::DeserializeXandEntitySetSummaryError;
use std::{io::BufReader, path::Path};
use tpfs_krypt::KeyPair;
use url::Url;

pub use banks_metadata::BanksMetadata;
pub use limited_agent_metadata::LimitedAgentMetadata;
pub use member_api_details::MemberApiDetails;
pub use member_metadata::MemberMetadata;
pub use trust_metadata::TrustMetadata;
pub use validator_metadata::ValidatorMetadata;
pub use xand_api_details::XandApiDetails;

mod banks_metadata;
pub mod error;
mod limited_agent_metadata;
mod member_api_details;
mod member_metadata;
mod trust_metadata;
mod validator_metadata;
mod xand_api_details;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct XandEntitySetSummary {
    pub banks: BanksMetadata,
    pub trust: TrustMetadata,
    pub limited_agent: LimitedAgentMetadata,
    pub validators: Vec<ValidatorMetadata>,
    pub members: Vec<MemberMetadata>,
}

impl XandEntitySetSummary {
    pub fn load<P: AsRef<Path>>(filepath: P) -> Result<Self, DeserializeXandEntitySetSummaryError> {
        let file = file_io::open_file(filepath)?;
        let reader = BufReader::new(file);
        let entities: XandEntitySetSummary = serde_yaml::from_reader(reader)?;
        Ok(entities)
    }
}
impl From<&DockerXandEntitySet> for XandEntitySetSummary {
    fn from(entities: &DockerXandEntitySet) -> Self {
        let on_chain_validators = entities
            .permissioned_validators
            .iter()
            .map(|val| build_validator_metadata(val, true));
        let off_chain_validators = entities
            .extra_validators
            .iter()
            .map(|val| build_validator_metadata(val, false));

        let on_chain_members = entities
            .permissioned_members
            .iter()
            .map(|mem| build_member_metadata(mem, true));

        let off_chain_members = entities
            .extra_members
            .iter()
            .map(|mem| build_member_metadata(mem, false));

        Self {
            banks: entities.banks.metadata(),
            trust: entities.trust.metadata(),
            limited_agent: entities.limited_agent.metadata(),
            validators: on_chain_validators.chain(off_chain_validators).collect(),
            members: on_chain_members.chain(off_chain_members).collect(),
        }
    }
}

fn build_validator_metadata(
    entity: &DockerComposeValidator,
    initially_permissioned: bool,
) -> ValidatorMetadata {
    let address = entity.validator.state().borrow().validator_address();
    let xand_api_url =
        Url::parse(&format!("http://localhost:{}", entity.xand_api_host_port())).unwrap();
    let entity_name = format!("validator-{}", entity.index);

    ValidatorMetadata {
        entity_name,
        xand_api_details: XandApiDetails {
            xand_api_url,
            auth: entity.validator.state().borrow().xand_api_auth().clone(),
        },
        address,
        initially_permissioned,
        banks: entity.validator.bank_info().clone(),
    }
}

fn build_member_metadata(
    entity: &DockerComposeMember,
    initially_permissioned: bool,
) -> MemberMetadata {
    let member_api_url = Url::parse(&format!(
        "http://localhost:{}",
        entity.member_api_host_port()
    ))
    .unwrap();

    let xand_api_url =
        Url::parse(&format!("http://localhost:{}", entity.xand_api_host_port())).unwrap();
    let address = entity.member.get_address().0.to_string();

    let state_data = entity.member.state().borrow();
    let member_api_auth = state_data.member_api_auth.clone();
    let xand_api_auth = state_data.xand_api_auth.clone();

    MemberMetadata {
        entity_name: format!("member-{}", entity.index),
        address,
        xand_api_details: XandApiDetails {
            xand_api_url,
            auth: xand_api_auth,
        },
        member_api_details: MemberApiDetails {
            member_api_url,
            auth: member_api_auth,
        },
        banks: entity.member.state().borrow().member_bank_info.clone(),
        encryption_public_key: entity.member.encryption_key_pair().identifier().value,
        initially_permissioned,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::contexts::docker_compose::docker_compose_network::docker_xand_entity_set::DockerXandEntitySet;
    use crate::app::contexts::docker_compose::entities::docker_compose_member::DockerComposeMember;
    use crate::app::contexts::docker_compose::entities::docker_compose_validator::DockerComposeValidator;
    use crate::contracts::data::bank_info::{Account, Bank, BankInfo};
    use insta::assert_yaml_snapshot;
    use std::collections::HashMap;

    #[test]
    fn from_docker_xand_entity_set__validators_initial_permissions_are_reflected_in_metadata() {
        // Given
        let mut docker_entities = DockerXandEntitySet::test();
        let mut val_0 = DockerComposeValidator::test();
        let mut val_1 = DockerComposeValidator::test();
        val_0.index = 0;
        val_1.index = 1;

        // Explicitly set on and off-chain validators
        docker_entities.permissioned_validators = vec![val_0];
        docker_entities.extra_validators = vec![val_1];

        // When
        let summary = XandEntitySetSummary::from(&docker_entities);

        // Then
        assert_eq!(summary.validators.len(), 2);
        let val_metadata_0 = summary.validators.get(0).unwrap();
        let val_metadata_1 = summary.validators.get(1).unwrap();
        assert!(val_metadata_0.initially_permissioned);
        assert!(!val_metadata_1.initially_permissioned);
    }

    #[test]
    fn from_docker_xand_entity_set__members_initial_permissions_are_reflected_in_metadata() {
        // Given
        let mut docker_entities = DockerXandEntitySet::test();
        let mut mem_0 = DockerComposeMember::test();
        let mut mem_1 = DockerComposeMember::test();
        mem_0.index = 0;
        mem_1.index = 1;

        // Explicitly set on and off-chain validators
        docker_entities.permissioned_members = vec![mem_0];
        docker_entities.extra_members = vec![mem_1];

        // When
        let summary = XandEntitySetSummary::from(&docker_entities);

        // Then
        assert_eq!(summary.members.len(), 2);
        let mem_metadata_0 = summary.members.get(0).unwrap();
        let mem_metadata_1 = summary.members.get(1).unwrap();
        assert!(mem_metadata_0.initially_permissioned);
        assert!(!mem_metadata_1.initially_permissioned);
    }

    #[test]
    fn build_validator_metadata__index_related_fields_are_indexed() {
        // Given
        let mut dc_validator = DockerComposeValidator::test();
        dc_validator.index = 1;

        // When
        let metadata = build_validator_metadata(&dc_validator, true);

        // Then
        assert_eq!(metadata.entity_name, "validator-1");

        let expected_url = Url::parse("http://localhost:10041").unwrap();
        assert_eq!(metadata.xand_api_details.xand_api_url, expected_url);
    }

    #[test]
    fn build_validator_metadata__when_initially_permissioned_is_true_then_flag_reflected_in_metadata(
    ) {
        // Given
        let dc_validator = DockerComposeValidator::test();
        let initially_permissioned = true;

        // When
        let metadata = build_validator_metadata(&dc_validator, initially_permissioned);

        // Then
        assert!(metadata.initially_permissioned);
    }

    #[test]
    fn build_validator_metadata__when_initially_permissioned_is_false_then_flag_reflected_in_metadata(
    ) {
        // Given
        let dc_validator = DockerComposeValidator::test();
        let initially_permissioned = false;

        // When
        let metadata = build_validator_metadata(&dc_validator, initially_permissioned);

        // Then
        assert!(!metadata.initially_permissioned);
    }

    #[test]
    fn build_member_metadata__index_related_fields_are_indexed() {
        // Given
        let mut dc_member = DockerComposeMember::test();
        dc_member.index = 3;

        // When
        let metadata = build_member_metadata(&dc_member, true);

        // Then
        assert_eq!(metadata.entity_name, "member-3");
        let expected_xand_api_url = Url::parse("http://localhost:10047/").unwrap();
        assert_eq!(
            metadata.xand_api_details.xand_api_url,
            expected_xand_api_url
        );
        let expected_member_api_url = Url::parse("http://localhost:3003/").unwrap();
        assert_eq!(
            metadata.member_api_details.member_api_url,
            expected_member_api_url
        );
    }

    #[test]
    fn validator_metadata_serializes_as_expected() {
        // Given
        let metadata = ValidatorMetadata {
            entity_name: "validator-0".to_string(),
            xand_api_details: XandApiDetails {
                xand_api_url: Url::parse("http://localhost:10042").unwrap(),
                auth: None,
            },
            address: "5E9t7QR2A3QtsYxpcpeCtiMQ1pHJGEVGvq5tJ4vtJg2rdAWH".to_string(),
            initially_permissioned: true,
            banks: vec![BankInfo {
                bank: Bank::TestBankOne,
                account: Account::new(1234, 5678),
            }],
        };

        // Then
        assert_yaml_snapshot!(metadata);
    }

    #[test]
    fn member_metadata_serializes_as_expected() {
        // Given
        let metadata = MemberMetadata {
            entity_name: "member-0".to_string(),
            xand_api_details: XandApiDetails {
                xand_api_url: Url::parse("http://localhost:10044").unwrap(),
                auth: None,
            },
            address: "5FqS4AH26QHs4brZXuafAxnkMkv1Yq4NE5PHLQCfM8PTHYJE".to_string(),
            member_api_details: MemberApiDetails {
                member_api_url: Url::parse("http://localhost:3000").unwrap(),
                auth: None,
            },
            encryption_public_key: "A3EQZLQypQ2keoCE4GAdgXsJGAaU4NS9Bige2SvRhozv".to_string(),
            banks: vec![BankInfo {
                bank: Bank::Mcb,
                account: Account::new(55, 66),
            }],
            initially_permissioned: true,
        };

        // Then
        assert_yaml_snapshot!(metadata);
    }

    #[test]
    fn trust_metadata_serializes_as_expected() {
        // Given
        let xand_api_url = Url::parse("http://localhost:10044").unwrap();
        let trust_api_url = Url::parse("http://localhost:10045").unwrap();
        let metadata = TrustMetadata {
            address: "5FqS4AH26QHs4brZXuafAxnkMkv1Yq4NE5PHLQCfM8PTHYJE".to_string(),
            xand_api_details: XandApiDetails {
                xand_api_url,
                auth: None,
            },
            trust_api_url,
            banks: vec![BankInfo {
                bank: Bank::Mcb,
                account: Account::new(999999, 494949),
            }],
            encryption_public_key: "DaGkwZkj9zbSeMW9wyxZuS7AcPYS2u4FsHshf1dZLLJq".to_string(),
        };

        // Then
        assert_yaml_snapshot!(metadata);
    }

    #[test]
    fn limited_agent_metadata_serializes_as_expected() {
        // Given
        let xand_api_url = Url::parse("http://localhost:11001").unwrap();
        let metadata = LimitedAgentMetadata {
            address: "5FqS4AH26QHs4brZXuafAxnkMkv1Yq4NE5PHLQCfM8PTHYJE".to_string(),
            xand_api_details: XandApiDetails {
                xand_api_url,
                auth: None,
            },
        };

        // Then
        assert_yaml_snapshot!(metadata);
    }

    #[test]
    fn banks_metadata__serializes_deterministically() {
        for _i in 0..100 {
            // Given
            let metadata = BanksMetadata {
                urls: {
                    let mut urls = HashMap::new();
                    urls.insert(Bank::Provident, "http://example.com/foo".into());
                    urls.insert(Bank::Mcb, "http://example.com/bar".into());
                    urls
                },
            };

            // When
            let string = serde_yaml::to_string(&metadata).unwrap();

            // Then
            assert_eq!(
                string,
                r###"---
urls:
  mcb: "http://example.com/bar"
  provident: "http://example.com/foo"
"###
            );
        }
    }
}
