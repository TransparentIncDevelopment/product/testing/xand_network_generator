#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct JwtInfo {
    pub secret: JwtSecret,
    pub token: Jwt,
}

impl JwtInfo {
    pub fn secret(&self) -> &JwtSecret {
        &self.secret
    }

    pub fn token(&self) -> &Jwt {
        &self.token
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct JwtSecret(pub String);

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Jwt(pub String);

impl Jwt {
    pub fn value(&self) -> &String {
        &self.0
    }
}
