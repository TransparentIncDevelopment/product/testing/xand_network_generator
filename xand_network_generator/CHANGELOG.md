# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [7.0.0] 2022-12-14
* Add block quota to config and apply to chain spec

## [6.0.2] 2022-10-24
* Generate encryption keys for validators

## [6.0.1] 2022-10-18
### Changed
* Use fixed chain spec generator to correctly generate only confidential networks (no more non-conf network generation)

## [5.0.0] 2022-08-23
### Changed
* Use the latest validator and chainspec generator versions with new nomenclature

## [3.0.0] 2022-04-05
### Changed
* Use the latest trust version with updated banks config structure

## [2.0.0] 2022-03-21
### Changed
* Use the latest chain-spec-generator and chain spec template version

## [1.1.0] 2022-02-21
### Added
* Validator bank accounts on the network

## [Unreleased]
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->



<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->
