# Xand Network Generator

## Building / linting

There are multiple crates in this repo, with no workspace setup. For this reason, run commands that operate at a crate level (e.g. `cargo clippy`) at the root of each rate.

## Component Versions

The versions of member-api, trust, validator, and bank-mocks are controlled via the config file at [`config/config.yaml`](https://gitlab.com/TransparentIncDevelopment/product/testing/xand_network_generator/-/blob/master/config/config.yaml).
**Please note:** There's no guarantee that the default versions are compatible with each other or
have been tested together. To find tested compatible versions look in the Acceptance test project's version config file
here: https://gitlab.com/TransparentIncDevelopment/product/testing/xand-acceptance-tests/-/blob/master/config/xng.yaml

Some other sources for compatible versions:
1. These are product releases fully tested: [xand-product-release Repo](https://gitlab.com/TransparentIncDevelopment/product/xand-product-release/-/blob/master/software_versions.yaml)
2. Testing status of these is unknown: [Xand-K8s Repo](https://gitlab.com/TransparentIncDevelopment/product/devops/xand-k8s/-/blob/master/.env)

XNG is updated for breaking changes in applications as they are introduced, such that it is only compatible with the most recent versions of applications (xand-trust, member-api, xandstrate) and chain-spec. This means that the latest version of XNG _might not_ produce a functioning network for older versions of applications/chain-spec. 

## Chain Spec

### Background
For a XAND network, a chain spec is a file acting as block 0 (i.e. genesis block) of the underlying blockchain. It includes
starting state like
- runtime rules (wasm blob)
- initial Validator/Member/Trust identities and IPs
- pallet configurations ("epoch durations" for consensus, default expiration values, etc.)

There are two chainspec related components required by Xand Network Generator:
1. **Chainspec Template:** These are blank templates that have the structure of a chainspec but with most fields empty
    or in a default state. 
1. **Chainspec Generator:** This tool hydrates a chainspec template with valid values.

For each version of the runtime, `thermite` publishes a chainpsec zip package
[to Artifactory](https://transparentinc.jfrog.io/ui/repos/tree/General/artifacts-internal%2Fchain-specs%2Ftemplates).

The zip contains a template chain spec with the correct runtime wasm blob.

This tool uses the templates to generate XAND networks of different starting shapes
(varying # of validators/members) for a given version of the runtime under test.

### Test Assets Chainspec

Currently, some tests for Xand Network Generator use a checked-in chainspec zip artifact with the version kept hardcoded in [the chain spec generation external wrapper](./src/app/external_wrappers/chain_spec_generation.rs#L74)

### CI + Config Chainspec 

The CI generates a docker-compose network as an artifact from the config listed in [`./config/config.yaml`](./config/config.yaml) and the chainspec listed in the variable in the CI file [`generate-network.yaml`](../.ci/generate-network.yaml#L3).  When updating, make sure that the version of validator in the config and version of chainspec listed in the ci file match.

## Installation

This tool can be installed without the need for pulling, building, and running from source, via cargo install:
```bash
cargo install --registry tpfs xand_network_generator
```

To download a specific (older or beta) version of XNG, run 
```bash
cargo install --registry tpfs --version <xng_version> xand_network_generator
```

## Usage

These are instructions for the current `master` branch of `xand_network_generator`.

#### Download a chain spec template zip from Artifactory

Download the `CHAINSPEC_VERSION` template you'd like to use into a `./chainspec_templates` directory. You must select
a version compatible with your validator software. A matching version is guaranteed to be compatible with your validator version. Note that thermite uses the tag value to version the validator and chain-spec templates, not the value in the Cargo.toml file.

> In cases where a new version of the chainspec has breaking schema changes, make sure [chain-spec-generator](https://gitlab.com/TransparentIncDevelopment/product/libs/chain-spec-generator/) dependency used here has been updated to take in those breaking changes.

Download a chain spec template with:
```bash
CHAINSPEC_VERSION="<chainspec_template_version>"
ZIP_FILENAME="chain-spec-template.${CHAINSPEC_VERSION}.zip"

wget --directory-prefix=./chainspec_templates/ --user=$ARTIFACTORY_USER  --password=$ARTIFACTORY_PASS \
    https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${ZIP_FILENAME}
```

#### Select application versions

Edit the `./config/config.yaml` file to set:  
- \# of Validators
- \# of Members
- Software Versions

Ensure the `validator` image version in `./config/config.yaml` matches the chain spec version you are using.
Ensure the other pieces of software have compatible versions for this version of xandstrate/validator.

#### Generate

Generate a local `docker-compose` network with:
```
xand_network_generator generate  \
    --config ./xand_network_generator/config/config.yaml \
    --chainspec-zip ./chainspec_templates/chain-spec-template.$CHAINSPEC_VERSION.zip \
    --output-dir ./generated
```

**If running locally from this repo, use `cargo run --` in place of `xand_network_generator`.**

> The tool follows a zero-based indexing scheme to name validators and members (i.e `[validator-0, validator-1, ...]`). It will include the requested number of
> validators and members in the chain spec, and produce an additional 5 validators and 5 members that are not included in the chain spec, but
> are configured to connect to the network.

#### Start network 

Start the network (all entities) with:

```bash
cd generated
docker-compose up -d
```

See the `./generated/entities-metadata.yaml` file to see the indexed entities and metadata to act on behalf of
each entity.

The above command requires the `tpfs` alternate registry to be set in your `$HOME/.cargo/config.toml` file. See
[The Cargo Book: Registries](https://doc.rust-lang.org/cargo/reference/registries.html) for more details if you get a
registry related error.

# TODOs

Below are scratch notes

## Tech Debt

- Clean up `ServiceBuilder` interface so consumption does not mix XAND Entity concepts with
docker-compose concepts

For example
```rust
s.set_image(&self.version)
    .set_public_addr(multi_addr_dns)
    .set_node_key(node_key)
    .set_consensus(true)
    .set_bootnode_url_vec(&self.bootnode_url_vec)
    .set_node_name(&node_name)
    .set_volumes_prefix(validator_volume_prefix);
```

- Expose config types so this crate can use them directly (XandConfig); Currently copied over

- TODO: Write unit tests for how DockerComposeContext et al. should be behaving

- Leftover static methods in dir_structure should be labeled as DockerSpecific

- TODO: Decopule config directories initialization with paths to specific items for mounting.
    - One mount path should be passed into validator for starting.

- TODO: Add "driver_opts" and "ipam" to docker network in resulting docker-compose file

- Do chainspec gen from versioned xandstrate lib?

- Don't let chainspec generator use Entity types directly, but interface only with public data via a Trait

## For working state

- Output config file for all entities

- ServiceBuilder is mixing xand node reqs with raw docker-compose service (like bank-mocks)

- Add sustrate-telemetry to docker-compose
