# Xand Network Generator Workspace

This workspace exists to house crates for generating network definitions of Xand components in a full Xand network.

Currently, this workspace has these crates:

- [xand_network_generator](./xand_network_generator) - The core of Xand Network Generator (CLI and Library)
- [network_maestro](./network_maestro) - Programmatic access to docker-compose networks (Library)

As development proceeds this list of crates will change.

# TODOs

- [Network Maestro TODOs](./network_maestro#TODOs)
- [Xand Network Generator TODOs](./xand_network_generator#TODOs)