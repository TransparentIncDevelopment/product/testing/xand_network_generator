use crate::{ContainerInstanceId, Error, Error::BadPath, NetworkMaestro, Result};
use std::{path::PathBuf, process::Command};

/// Uses `docker-compose` via shelling out to start/stop/manipulate the docker compose containers
#[derive(derive_more::Constructor)]
pub struct ComposeShellMaestro {
    /// Where the docker compose file we're interacting with is located
    compose_file_location: PathBuf,
    /// Where the env faile containing environment variables is located
    env_file_location: PathBuf,
    /// If true, print output as commands finish for debugging
    debug_mode: bool,
}

impl ComposeShellMaestro {
    fn command_preamble(&self) -> Result<Command, Error> {
        let mut cmd = Command::new("docker-compose");
        cmd.args([
            // No color codes in output
            "--ansi",
            "never",
            "-f",
            self.compose_file_location
                .canonicalize()?
                .to_str()
                .ok_or_else(|| BadPath(self.compose_file_location.clone()))?,
            "--env-file",
            self.env_file_location
                .canonicalize()?
                .to_str()
                .ok_or_else(|| BadPath(self.env_file_location.clone()))?,
        ]);
        Ok(cmd)
    }

    /// Runs docker-compose commands
    fn dc(&self, args: &[&str]) -> Result<(), Error> {
        let output = self.command_preamble()?.args(args).output()?;
        if self.debug_mode {
            dbg!(output);
        }
        Ok(())
    }
}

#[async_trait::async_trait]
impl NetworkMaestro for ComposeShellMaestro {
    async fn start_network(&self) -> Result<(), Error> {
        self.dc(&["up", "-d"])
    }

    async fn stop_network(&self) -> Result<(), Error> {
        self.dc(&["down"])
    }

    async fn start_node(&self, instance: ContainerInstanceId) -> Result<(), Error> {
        self.dc(&["start", &format!("validator-{}", instance.0)])
    }

    async fn stop_node(&self, instance: ContainerInstanceId) -> Result<(), Error> {
        self.dc(&["stop", &format!("validator-{}", instance.0)])
    }

    /// Instance currently does nothing - there's only one
    async fn start_member(&self, _instance: ContainerInstanceId) -> Result<(), Error> {
        self.dc(&["start", "member-api"])
    }

    /// Instance currently does nothing - there's only one
    async fn stop_member(&self, _instance: ContainerInstanceId) -> Result<(), Error> {
        self.dc(&["stop", "member-api"])
    }

    async fn start_trust(&self) -> Result<(), Error> {
        self.dc(&["start", "trustee"])
    }

    async fn stop_trust(&self) -> Result<(), Error> {
        self.dc(&["stop", "trustee"])
    }

    async fn start_trust_node(&self) -> Result<(), Error> {
        self.dc(&["start", "trust-non-consensus-node"])
    }

    async fn stop_trust_node(&self) -> Result<(), Error> {
        self.dc(&["stop", "trust-non-consensus-node"])
    }

    async fn start_bank_mock(&self) -> Result<(), Error> {
        self.dc(&["start", "bank-mocks"])
    }

    async fn stop_bank_mock(&self) -> Result<(), Error> {
        self.dc(&["stop", "bank-mocks"])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use futures::executor::block_on;

    #[test]
    #[ignore]
    fn manual_test() {
        let csm = ComposeShellMaestro {
            compose_file_location:
                "/home/sushi/dev/work/xand-trust/xand-docker-compose/docker-compose.yaml".into(),
            env_file_location: "/home/sushi/dev/work/xand-trust/xand-docker-compose/.env".into(),
            debug_mode: true,
        };
        block_on(csm.start_network()).unwrap();
        block_on(csm.stop_node(ContainerInstanceId(0))).unwrap();
        block_on(csm.stop_network()).unwrap();
    }
}
