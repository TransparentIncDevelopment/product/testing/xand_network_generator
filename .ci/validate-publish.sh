#!/usr/bin/env bash

MEMBERS_COUNT=$(toml get Cargo.toml workspace.members | jq '.| length')
ITER=0
trace-cmd.sh cargo-login cargo login --registry tpfs $TPFS_CRATES_TOKEN
while (($ITER < $MEMBERS_COUNT))
do
  MEMBER_CRATE=$(toml get Cargo.toml workspace.members | jq --argjson index $ITER -r '.[$index]' )
  echo $MEMBER_CRATE
  trace-cmd.sh cargo-publish cargo publish --registry tpfs --dry-run --manifest-path $MEMBER_CRATE/Cargo.toml || exit 1
  ITER=$(expr $ITER + 1)
done