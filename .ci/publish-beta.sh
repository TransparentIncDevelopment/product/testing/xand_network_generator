#!/usr/bin/env bash

MEMBERS_COUNT=$(toml get Cargo.toml workspace.members | jq '.| length')
ITER=0
trace-cmd.sh cargo-login cargo login --registry tpfs $TPFS_CRATES_TOKEN
while (($ITER < $MEMBERS_COUNT))
do
  MEMBER_CRATE=$(toml get Cargo.toml workspace.members | jq --argjson index $ITER -r '.[$index]' )
  echo $MEMBER_CRATE
  cd $MEMBER_CRATE
  CURRENT_VERSION=$(toml get Cargo.toml package.version | tr -d '"')
  BETA_VERSION="$CURRENT_VERSION-beta.${CI_PIPELINE_IID}"
  echo "Setting beta version in Cargo.toml -> $BETA_VERSION"
  NEW_CARGO_TOML=$(toml set Cargo.toml package.version $BETA_VERSION)
  echo "$NEW_CARGO_TOML" > Cargo.toml
  cd ..
  trace-cmd.sh cargo-publish cargo publish --registry tpfs --manifest-path $MEMBER_CRATE/Cargo.toml --allow-dirty
  ITER=$(expr $ITER + 1)
done