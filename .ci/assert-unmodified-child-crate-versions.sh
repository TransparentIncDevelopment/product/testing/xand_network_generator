#!/bin/bash

set -e

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

MAESTRO_TOML="$DIR/../network_maestro/Cargo.toml"
MAESTRO_VERSION=$(toml get "$MAESTRO_TOML" package.version | tr -d \")

if [ "$MAESTRO_VERSION" != "0.0.0" ]; then
    echo "network_maestro version has been manually modified: found \"$MAESTRO_VERSION\", expected \"0.0.0\"."
    echo "Please reset the version of network_maestro to \"0.0.0\". It is managed exclusively by CI."
    exit 1
fi
